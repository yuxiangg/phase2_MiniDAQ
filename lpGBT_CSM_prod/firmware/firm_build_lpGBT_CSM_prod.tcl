# /* 
#        _________   ________     ________       
#       / U OF M  \ | LSA    \   / Physics\
#       \__ATLAS__/ |   ___   | |   ______/
#          |   |    |  |   \  | |  |
#          |   |    |  |___/  | |  \______     
#          |   |    |         | |         \
#          \___/    |________/   \________/
# */  

# // Author : Yuxiang Guo  gyuxiang@umich.edu
# // File   : firm_build_lpGBT_CSM_prod.tcl
# // Create : 2024-11-04 16:31:00
# // Revise : 2024-11-04 16:31:00
# // Editor : sublime text4, tab size (4)
# // Description: 
# //
# // -----------------------------------------------------------------------------


#*****************************************************************************************
#################################  README FIRST ##########################################
#*****************************************************************************************

# In order to execute the tcl script and build the project, run Vivado and go to: 
# Tools -> Run Tcl Script...
#
# An alternative way would be to open a terminal, and run this command:
# vivado -mode batch -source <PATH>/build.tcl
#
# For more info on how to make further changes to the script, see: 
# http://xillybus.com/tutorials/vivado-version-control-packaging
#


set_param general.maxThreads 8

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/sources"
set constrain_dir "$origin_dir/constrains"
set firmware_dir "$origin_dir/../../../firm_lpgbtCsmProd"


create_project lpgbtcsm_prod $firmware_dir -part xc7a35tfgg484-2 -force
set_property target_language Verilog [current_project]
set_property simulator_language Mixed [current_project]

#*****************************************************************************************
#################################  Adding Source #########################################
#*****************************************************************************************


add_files -fileset sources_1 $source_dir

#*****************************************************************************************
#################################  Adding Constrain ######################################
#****************************************campu*************************************************
add_files -fileset constrs_1 $constrain_dir


#*****************************************************************************************
#################################  Set Top Module ######################################
#*****************************************************************************************
set obj [get_filesets sources_1]
set_property -name "top" -value "lpgbtcms_fpga_top" -objects $obj

# create_ip -name ila -vendor xilinx.com -library ip -module_name ila_0
# set_property -dict [list \
#   CONFIG.C_NUM_OF_PROBES {4} \
#   CONFIG.C_PROBE0_WIDTH {1} \
# ] [get_ips ila_0]

# create_ip -name vio -vendor xilinx.com -library ip -module_name vio_0
# set_property -dict [list \
#   CONFIG.C_NUM_PROBE_IN {2} \
#   CONFIG.C_NUM_PROBE_OUT {0} \
#   CONFIG.C_PROBE_IN1_WIDTH {20} \
# ] [get_ips vio_0]

