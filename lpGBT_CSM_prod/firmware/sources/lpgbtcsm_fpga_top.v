/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : lpgbtcsm_fpga_top.v
// Create : 2024-11-12 17:07:46
// Revise : 2024-11-12 18:06:00
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------

`timescale 1ns / 1ps

module lpgbtcsm_fpga_top 
    #(parameter NUM_DEV = 20)
    (
//    input                   sys_clk_p,        // 160MHz from LpGBT elink 
//    input                   sys_clk_n,        // 

    input tdi_master_i,
    output tdo_master_o,
    input tms_master_i,
    input tck_master_i,

    input TDC_reset_from_SCA,
    input [NUM_DEV-1:0] daisychain_ctrl_v,

    input elink_TTC_in_p,   //TTC from elink
    input elink_TTC_in_n,

    input     [NUM_DEV-1:0]  tdo,              // JTAG Test Data Out.
    output    [NUM_DEV-1:0]  tdi,              // JTAG Test Data In.
    output    [NUM_DEV-1:0]  tck,              // JTAG Test Clock.
    output    [NUM_DEV-1:0]  tms,              // JTAG Test Mode Select.
    output   rst_AMT_MB, // AMT_RESET TO AMT PIN 1 RESETB

    output    [NUM_DEV-1:0]  mezz_enc_out_p,        // Calibration P.
    output    [NUM_DEV-1:0]  mezz_enc_out_n         // Calibration N.
);

wire [NUM_DEV-1:0] mezz_enc;

assign rst_AMT_MB = TDC_reset_from_SCA;

// Generate daisy-chained JTAG signals based on daisychain_ctrl_v

// tdi_master_i -> TDC0 -> TDC1 -> ... -> TDC19 -> tdo_master_o

wire [NUM_DEV:0] daisychain_tdi;
assign daisychain_tdi[0] = tdi_master_i;
assign tdo_master_o = daisychain_tdi[NUM_DEV];

generate
    genvar j;
    for (j = 0; j < NUM_DEV; j = j + 1) begin: jtag_daisy_chain
        assign daisychain_tdi[j + 1] = (daisychain_ctrl_v[j]) ? tdo[j] : daisychain_tdi[j];
        assign tck[j] = (daisychain_ctrl_v[j]) ? tck_master_i : 1'b0;
        assign tms[j] = (daisychain_ctrl_v[j]) ? tms_master_i : 1'b0;
        assign tdi[j] = (daisychain_ctrl_v[j]) ? daisychain_tdi[j] : 1'b0;
    end
endgenerate


//wire clk_ibuf;
//wire clk;
//// Differential Input Buffer
//IBUFDS ibufds_inst (
//    .I(sys_clk_p),    // Differential positive input
//    .IB(sys_clk_n),   // Differential negative input
//    .O(clk_ibuf)  // Single-ended output of IBUFDS
//);

//// Global Clock Buffer
//BUFG bufg_inst (
//    .I(clk_ibuf), // Input from IBUFDS
//    .O(clk)   // Global clock output
//);

//ila_0 ila_jtag (
//    .clk(clk), // input wire clk

//    .probe0(tdi_master_i), // input wire [0:0]  probe0  
//    .probe1(tdo_master_o), // input wire [0:0]  probe1 
//    .probe2(tms_master_i), // input wire [0:0]  probe2 
//    .probe3(tck_master_i) // input wire [0:0]  probe3
//);

//vio_0 vio_GPIO (
//  .clk(clk),              // input wire clk
//  .probe_in0(TDC_reset_from_SCA),  // input wire [0 : 0] probe_in0
//  .probe_in1(daisychain_ctrl_v)  // input wire [19 : 0] probe_in1
//);

//assign elink_ttc_org = 1'b0;
IBUFDS 
#(.IOSTANDARD("LVDS_25"),
  .DIFF_TERM(1),
  .IBUF_LOW_PWR(1))
    IBUFDS_inst
       (.O  (elink_ttc_org),
        .I  (elink_TTC_in_p),
        .IB (elink_TTC_in_n));

generate
    genvar i;
    for (i = 0; i <= NUM_DEV-1; i = i + 1) begin:enc_obuf
        assign mezz_enc[i] = elink_ttc_org;
        OBUFDS 
        #(.IOSTANDARD("DEFAULT"),
          .SLEW("SLOW"))
            OBUFDS_inst
            (.O  (mezz_enc_out_p[i]),
            .OB  (mezz_enc_out_n[i]),
            .I   (mezz_enc[i]));
    end   
endgenerate

endmodule
