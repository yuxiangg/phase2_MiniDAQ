# phase2_MiniDAQ


git clone --recursive https://gitlab.cern.ch/yuxiangg/phase2_MiniDAQ.git

This repo includes the firmware and software for testing the ATLAS phase2 upgrade MDT front-end electronics (ASD2, TDC and CSM) using the MiniDAQ board.

---
## Hardware connection
Up to 20 mezz (3 ASD + 1 TDC for each mezz) per CSM. Mezz 0-9 uses the master LpGBT and mezz 10-19 uses the slave LpGBT. For ATLAS Muon application, maximum 24 mezz will be used for one chamber and in this case, 2 CSMs will be connected to the MiniDAQ board. The maximum mezz that can be read out by 2 CSMs are 40. A maximum connection is shown below. Fiber #6 is master uplink, fiber #7 is master downlink and fiber #5 is slave uplink. For other connections, refer to MiniDAQ user manual. 

```mermaid
graph TD
ASD0 --> TDC0
ASD1 --> TDC0
ASD2 --> TDC0
TDC0 --> CSM0M[CSM0 Master LpGBT]
TDC[......] --> CSM0M
TDC9 --> CSM0M
CSM0M--fiber #6--> SFP7
SFP7--fiber #7--> CSM0M
SFP7--> MiniDAQ

TDC10 --> CSM0S
TDCasdf[......] --> CSM0S
TDC19 --> CSM0S[CSM0 Slave LpGBT]
CSM0S--fiber #5--> SFP5
SFP5--> MiniDAQ

TDC20 --> CSM1M[CSM1 Master LpGBT]
TDCaa[......] --> CSM1M
TDC29 --> CSM1M
CSM1M--fiber #6--> SFP3
SFP3--fiber #7--> CSM1M
SFP3--> MiniDAQ


TDC30 --> CSM1S
TDCasadf[......] --> CSM1S
TDC39 --> CSM1S[CSM1 Slave LpGBT]
CSM1S--fiber #5--> SFP1
SFP1--> MiniDAQ

MiniDAQ-->PC

PC-->Online(Online Monitor)
PC-->Offline(Offline Reco)
```



## Test Guide (Triggerless 640 Mbps)

1. Program the MiniDAQ bit file
Load MiniDAQ bit file firmware/xcku035_10g24_top_40mezz_SFP1357.bit to the MiniDAQ FPGA xcku035. 

2. Configure LpGBT
There are 2 ways to configure the LpGBTs. One is using the **piGBT** device (obsolete), and the other is to use **MiniDAQ UART** (recommended) to configure through the downlink fiber. PiGBT method will be listed in the end of this README file.
- Connect **USB-UART** port (*J4*) to your host PC. Two new ttyUSB* devices will appear when you run `ls /dev/ttyUSB*`. Change the ownership by running `sudo chown $yourusername /dev/ttyUSB*` so that you have the permission to use this device. Modify GUI/MainWindow_init.py by replacing `/dev/ttyUSB1` with your device name (usually the bigger number of the 2 newly appeared ttyUSB* devices).
- Run ***GUI/MainWindow_init.py*** in python to start the GUI (GUI/requirements.txt lists all modules you need to download. You can use "pip install -r requirements.txt" or let Pycharm (a free python IDE) to automatic handle it). 
  - In tab "LpGBT config", select "**CSM0 Master**". Click the button "Load Master", then click "Uplink Reset". Check in vivado if *uplink[**0**].vio_uplink_ins.lpgbtfpga_uplinkrdy_1[**0:0**]* is 1. If click "Load Master" again the GUI Output Log will print LpGBT117 reply. 
  - If using mezz **10-19**, select **CSM0 Master** and check "**VTRX Slave**" (if it is already checked, uncheck first then re-check). Then select "**CSM0 Slave**". Click the button "Load Slave", then click "Uplink Reset". Check in vivado if *uplink[**1**].vio_uplink_ins.lpgbtfpga_uplinkrdy_1[**1:1**]* is 1. If click "Load Slave" again the GUI Output Log will print LpGBT115 reply. 
  - If using mezz **20-29**, select **CSM1 Master**.  Click the button "Load Master", then click "Uplink Reset". Check in vivado if *uplink[**2**].vio_uplink_ins.lpgbtfpga_uplinkrdy_1[**2:2**]* is 1. If click "Load Slave" again the GUI Output Log will print LpGBT117 reply. 
  - If using mezz **30-39**, select **CSM1 Master** and check "**VTRX Slave**" (if it is already checked, uncheck first then re-check). Then select "**CSM1 Slave**". Click the button "Load Slave", then click "Uplink Reset". Check in vivado if *uplink[**3**].vio_uplink_ins.lpgbtfpga_uplinkrdy_1[**3:3**]* is 1. If click "Load Slave" again the GUI Output Log will print LpGBT115 reply. 

3. Configure mezz JTAG 
- through CSM FPGA
  - Load CSM bit file ***lpGBT_CSM/firmware/lpgbtcsm_fpga_top_20mezz.bit*** to the CSM FPGA xc7a. Load ***config_tcl/b0_xc7a_triggerless_320.tcl*** to xc7a (to disable all TDC inputs). Note: all mezz are enabled by default. If you want to configure certain boards, manually set ***mezz_enable_vio[19:0]*** , then ***rst_global***, then load the tcl file again. Check ***vio_top_inst.active_chains[19:0]*** to see if it corresponds to all connected and enabled mezzanine cards, and check ***vio_top_inst.chain_success[5:0]*** to be all 1. ASD threshold is configured to 114 by default. To change ASD settings, modify ***config_tlc/0_config_asd_20mezz.tcl*** accordingly.
  - If you are using 2 CSMs, do the steps above for each CSM.
- through GBT-SCA
  - Load CSM bit file ***lpGBT_CSM_prod/firmware/lpgbtcsm_fpga_top.bit*** to the CSM FPGA xc7a.
  - Select "**SCA**" in tab "LpGBT config".
  - In tab "Mezz Config", click "**Mezz Scan**". All selected mezz will be scanned if there is a mezz connected. If a selected mezz is connected, its label will turn blue, otherwise it will remain gray and become unselected. Once Mezz Scan completes, mezz can be configured by "***write_TDC_all***" and "***write_ASD_all***". If you want to change settings, click "***TDC_setup***" to set individual TDCs and ASDs before "***write_TDC_all***" and "***write_ASD_all***". Then click "***TDC_input_disable***" to start data line lock process.

4. Lock the data line and synchronize all mezz cards
Load ***config_tcl/b1_xcku035_triggerless_320.tcl*** to xcku035. Check ***tdc_decoder_top_inst/vio_40M_inst.locked_dline0[39:0]*** and ***locked_dline1[39:0]*** to see if they correspond to all connected mezzanine cards. A bc_reset is sent to all mezz cards through TTC during this step.



5. Enable mezz input
- through CSM FPGA
  - Load ***config_tcl/b2_xc7a_triggerless_320.tcl*** to xc7a (to enable all TDC inputs). If you are using 2 CSMs, do this step for each CSM.
- through GBT-SCA
  - In tab "Mezz Config", click "***TDC_input_enable***".

6. Start DAQ 
Start the DAQ GUI (`./DAQManager` in ***ATLAS_Online_Monitor/ROOT_plot/build***). Select Network device. If you cannot see the device, either run DAQManager with sudo privilege or give DAQManager the permission by `source setEcapPermissions.sh` in ***ATLAS_Online_Monitor/ROOT_plot*** . Write the configuration file name (template can be found in ATLAS_Online_Monitor/ROOT_plot/conf/run_20240412.conf). Click "Start Run". Select View->ADC Plots/TDC Plots to see real time ADC and TDC spectrum. Adjust the coarse_count_offset in xcku vivado ***tdc_decoder_top_inst/vio_trigger_inst.coarse_count_offset[11:0]*** if needed. After changing coarse_count_offset, run b0 for xc7a, b1 for xcku, and b2 for xc7a. Then start the DAQ.

# Other Mode

If you are not running the default 2 $\times$ 320 Mbps triggerless mode, the configuration procedure is similar. Follow the general test guide and load different tcl scripts accordingly.
1. 2 $\times$ 320 Mbps triggerless mode: a0->a1->a2
2. 2 $\times$ 320 Mbps triggered mode: d0->d1->d2->xcku_trigger_start
3. 2 $\times$ 160 Mbps triggered mode: c0->c1->c2->xcku_trigger_start

# MiniDAQ Firmware
If you want to compile your own bit file instead of using the given one, below are some useful info:

MiniDAQ firmware directory: firmware

Bit file: ***firmware/xcku035_10g24_top_40mezz_SFP1357.bit***

Debug file: ***firmware/xcku035_10g24_top_40mezz_SFP1357.ltx***

Tcl script to create a vivado project: ***firmware/firm_build_MiniDAQ.tcl.***

Top module: ***firmware/sources/lpgbt-fpga-xcku035/hdl/xcku035_10g24_top.sv.***

According to how many mezz you are connecting, some local parameters need to be manually changed:

parameter UPLINKCOUNT   = 4;
parameter DOWNLINKCOUNT = 2;

And the mgt ip (***xlx_ku_mgt_ip_10g24***) physical resources needs to be checked according to your connection. ***MGTREFCLK0*** of Quad X0Y1 should be used for all SFP inputs.

Note: due to input crosstalk, adjacent SFPs cannot be used at the same time (e.g. SFP0 and SFP1 cannot be used at the same time, but SFP0 and SFP2 can). By default for 40 mezz, SFP1, SFP3, SFP5 and SFP7 are used.

# CSM firmware 

CSM firmware directory: lpGBT_CSM/firmware

Bit file: ***lpGBT_CSM/firmware/lpgbtcsm_fpga_top_20mezz.bit***

Debug file: ***lpGBT_CSM/firmware/lpgbtcsm_fpga_top_20mezz.ltx***

Tcl script to create a vivado project: ***lpGBT_CSM/firmware/firm_build_lpGBT_CSM.tcl***.

Top module: ***lpGBT_CSM/firmware/sources/lpgbtcsm_fpga_top.v***.

Note: This is not the final CSM FPGA firmware. CSM FPGA bit file programming and mezz JTAG configuration will be via the lpGBT downlink in the future version, where the local JTAG connection to the CSM FPGA will be removed.

# piGBT

A customized raspberry pi control toolkit (https://pigbt.web.cern.ch/manual/overview.html) is used for configuring the master and slave LpGBTs on the CSM board, via the web-based piGBT GUI. This module will be removed and LpGBTs will be configured via the downlink.

For mezz 0-9, load piGBT/master.cnf to the master LpGBT. If a production-batch VTRX+ is used, enable the slave LpGBT channel through the I2C master when the module is still connected to the master LpGBT.

For mezz 10-19, load piGBT/slave.cnf to the slave LpGBT. You must first enable the slave channel for data of mezz 10-19 to reach the MiniDAQ board. You do not need to configure the slave LpGBT if you have no mezz connection in slots 10-19.

For mezz 20-39, repeat the steps above on the second CSM.

# Configure lpGBT using PiGBT
1. Connect PiGBT cable to master LpGBT. Open PiGBT web page, set mode TRX, FEC5 and 10 Gbps. Then load piGBT/master.cnf (this is for lpGBTv1) in Register tab. 

2. If using mezz 10-19, go to I2C Master tab, set: Master=I2CM1, Slave address=0x50, Register address width=8, Register address=0x00, Bus speed=400k, Data=0x03. Then click "Write". Click "Read" to make sure Data Read=0x3. This step opens the slave LpGBT path of the VTRx+. Now switch the PiGBT cable to slave LpGBT. Open PiGBT web page, set mode TX, FEC5 and 10 Gbps. Then load piGBT/slave.cnf (this is for lpGBTv1) in Register tab. 
3. If using mezz 20-39, repeat step 1 and 2 on the second CSM.

# ATLAS_Online_Monitor

This is a ROOT based GUI to start/stop the DAQ. Real time ADC/TDC spectra will be plotted, and a binary .dat file will be saved without any filtering/modification. For details refer to the submodule readme file.

# smdt-reco

This is a ROOT based offline analysis program. A detailed decoding, spectra fitting, autocalibration, residual, resolution and efficiency calculation will be performed.  For details refer to the submodule readme file.

