Use MAXPOWERTOOL002# (USB to powerbus) to write power up/down sequence configuration to the device. Once loaded the info will be stored to the devices.
Use MaximDigitalPower (software) to load the configuration. When all 8 devices show up in the software, load "powerup_sequence.xml". When completed, "StoreUserAll" to load the current configuration to the on-device memory.
You do not need to load individual configurations for each device as long as you have all 8 devices shown up in the software.
MaximDigitalPowerTool - V2.32.00.exe.zip can be used for windows10. Unzip and install it. The latest version may have some bugs but this version tested fine.
