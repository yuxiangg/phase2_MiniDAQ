/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : uart_debug_top.v
// Create : 2024-03-12 15:20:51
// Revise : 2024-03-12 17:15:17
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------
module uart_debug_top (

    input           USER_CLOCK_P,
    input           USER_CLOCK_N,
    input           uart_rxd,
    output          uart_txd
   
);

wire clk40;
clk_40 clk_40_inst
(
    // Clock out ports
    .clk_out1(clk40),     // output clk_out1
    // Clock in ports
    .clk_in1_p(USER_CLOCK_P),    // input clk_in1_p
    .clk_in1_n(USER_CLOCK_N));    // input clk_in1_n

wire reset, reset_sync, command_fifo_empty;
wire [159:0] data_back,command_o;
reg [159:0] command_r;
wire data_back_fifo_wr_en,data_back_fifo_wr_en_sync,command_fifo_rd, command_fifo_rd_sync;

reg reset_r,data_back_fifo_wr_en_r, command_fifo_rd_r;
always @(posedge clk40) begin 
    reset_r <= reset;
    data_back_fifo_wr_en_r <= data_back_fifo_wr_en;
    command_fifo_rd_r <= command_fifo_rd;
    if (~command_fifo_empty) begin
        command_r <= command_o;
    end
end
assign reset_sync = (~reset_r) & reset;
assign command_fifo_rd_sync = (~command_fifo_rd_r) & command_fifo_rd;
assign data_back_fifo_wr_en_sync = (~data_back_fifo_wr_en_r) & data_back_fifo_wr_en;

UART_interface_standalone UART_interface_standalone_inst
(
    .clk40                (clk40),
    .reset                (reset_sync),
    .rxd_i                (uart_rxd),
    .txd_o                (uart_txd),
    .command_fifo_rd_en_i (command_fifo_rd_sync),
    .command_o            (command_o),
    .command_fifo_empty_o (command_fifo_empty),
    .data_back            (data_back),
    .data_back_fifo_wr_en (data_back_fifo_wr_en_sync)
);


vio_uart vio_uart_inst (
  .clk        (clk40),                // input wire clk
  .probe_in0  (command_fifo_empty),    // input wire [0 : 0] probe_in0
  .probe_in1  (command_r),    // input wire [159 : 0] probe_in1
  .probe_out0 (command_fifo_rd),  // output wire [0 : 0] probe_out0
  .probe_out1 (data_back_fifo_wr_en),  // output wire [0 : 0] probe_out1
  .probe_out2 (data_back),  // output wire [159 : 0] probe_out2
  .probe_out3 (reset)  // output wire [0 : 0] probe_out1
);



endmodule : uart_debug_top
