/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : UART_interface_standalone.v
// Create : 2024-03-12 15:28:22
// Revise : 2024-03-12 18:07:37
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------

module UART_interface_standalone(
    input           clk40,
    input           reset,
    //UART interface
    input           rxd_i,
    output          txd_o,
    //command_interface
    input           command_fifo_rd_en_i,
    output  [159:0] command_o,
    output          command_fifo_empty_o,
    //data interface
    input [159:0] data_back,
    input data_back_fifo_wr_en
);


wire [7:0] rx_data;
wire rx_ready;
wire rx_parity_error;

wire frame_ready;
wire [159:0] frame_data;

wire [159:0] data_back_out;
wire data_back_fifo_full,data_back_fifo_empty,data_back_fifo_rd_en;

localparam parity_mode = 2'b00;
localparam baud_rate   = 2'b11;

wire [7:0] tx_data_i;
wire tx_start_i, tx_ready_o;


uart_top uart_inst(
    .clk             (clk40),
    .reset_n         (~reset),
    .parity_mode     (parity_mode),
    .baud_rate       (baud_rate),
    .rxd             (rxd_i),
    .rx_data         (rx_data),
    .rx_ready        (rx_ready),
    .rx_parity_error (rx_parity_error),

    .tx_data         (tx_data_i),
    .tx_start        ((tx_start_i)&(~tx_ready_o)),
    .txd             (txd_o), 
    //tx_start only recognizes the rising edge, so need a force down
    .tx_ready        (tx_ready_o)
);
 

frame_build  frame_build_inst(
    .clk             (clk40),
    .reset_n         (~reset),
    .rx_data         (rx_data),
    .rx_ready        (rx_ready),
    .rx_parity_error (rx_parity_error),
    .frame_ready     (frame_ready),
    .frame_data      (frame_data)
);
                
                    
fifo_160bit command_fifo_inst (
    .clk         (clk40),                   // input wire clk
    .srst        (reset),                   // input wire srst
    .din         (frame_data),              // input wire [159 : 0] din
    .wr_en       (frame_ready),             // input wire wr_en
    .rd_en       (command_fifo_rd_en_i),    // input wire rd_en
    .dout        (command_o),               // output wire [159 : 0] dout
    .full        (),                        // output wire full
    .empty       (command_fifo_empty_o),    // output wire empty
    .wr_rst_busy (),                        // output wire wr_rst_busy
    .rd_rst_busy ()                         // output wire rd_rst_busy
);
              

fifo_160bit data_back_fifo (
    .clk         (clk40),                  // input wire clk
    .srst        (reset),                // input wire srst
    .din         (data_back),                  // input wire [159 : 0] din
    .wr_en       (data_back_fifo_wr_en),              // input wire wr_en
    .rd_en       (data_back_fifo_rd_en),              // input wire rd_en
    .dout        (data_back_out),                // output wire [159 : 0] dout
    .full        (data_back_fifo_full),                // output wire full
    .empty       (data_back_fifo_empty),              // output wire empty
    .wr_rst_busy (),  // output wire wr_rst_busy
    .rd_rst_busy ()  // output wire rd_rst_busy
);

send_back_data send_back_data_inst(
    .clk(clk40),
    .reset(reset),

    .data_back_fifo_empty(data_back_fifo_empty),
    .data_back_fifo_rd(data_back_fifo_rd_en),
    .data_back(data_back_out),

    .send_ready(tx_ready_o),
    .start_send(tx_start_i),
    .data_send(tx_data_i)
);

endmodule
