##===================================================================================================##
##========================================  I/O PINS  ===============================================##
##===================================================================================================##

###==============##
### SYSTEM CLOCK 200MHz ##
###==============##
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_P]
set_property IOSTANDARD LVDS [get_ports USER_CLOCK_N]
set_property PACKAGE_PIN T22 [get_ports USER_CLOCK_P]
set_property PACKAGE_PIN U22 [get_ports USER_CLOCK_N]
set_property DIFF_TERM_ADV TERM_100 [get_ports USER_CLOCK_P]
set_property DIFF_TERM_ADV TERM_100 [get_ports USER_CLOCK_N]




###==============##
### SYSTEM CLOCK 200MHz ##
###==============##
#set_property IOSTANDARD LVDS [get_ports SYS_CLOCK_200_P]
#set_property PACKAGE_PIN T22 [get_ports SYS_CLOCK_200_P]
#set_property IOSTANDARD LVDS [get_ports SYS_CLOCK_200_N]
#set_property PACKAGE_PIN U22 [get_ports SYS_CLOCK_200_N]
# set_property DIFF_TERM TRUE [get_ports SYS_CLOCK_200_P]
# set_property DIFF_TERM TRUE [get_ports SYS_CLOCK_200_N]





# UART INTERFACE
##-------------
set_property PACKAGE_PIN AC18 [get_ports uart_rxd]
set_property IOSTANDARD LVCMOS25 [get_ports uart_rxd]

set_property PACKAGE_PIN AB17 [get_ports uart_txd]
set_property IOSTANDARD LVCMOS25 [get_ports uart_txd]

#set_property PACKAGE_PIN AC19 [get_ports uart_cts]
#set_property IOSTANDARD LVCMOS25 [get_ports uart_cts]

#set_property PACKAGE_PIN AA18 [get_ports uart_rts]
#set_property IOSTANDARD LVCMOS25 [get_ports uart_rts]


