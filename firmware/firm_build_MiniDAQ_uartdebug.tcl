
set_param general.maxThreads 8

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/uart_debug/sources"
set constrain_dir "$origin_dir/uart_debug/constrains"
set firmware_dir "$origin_dir/../../firm_MiniDAQ_uartdebug"


create_project firm_MiniDAQ_uartdebug $firmware_dir -part xcku035-fbva676-1-c -force
set_property target_language Verilog [current_project]
set_property simulator_language Mixed [current_project]


#*****************************************************************************************
#################################  Adding Source #########################################
#*****************************************************************************************


add_files -fileset sources_1 $source_dir
add_files -fileset sources_1 $source_dir/../../sources/uart

#*****************************************************************************************
#################################  Adding Constrain ######################################
#****************************************campu*************************************************
add_files -fileset constrs_1 $constrain_dir

set_property top uart_debug_top [current_fileset]


create_ip -name clk_wiz -vendor xilinx.com -library ip -module_name clk_40
set_property -dict [list CONFIG.Component_Name {clk_40} CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} CONFIG.PRIM_IN_FREQ {200.000} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40.000} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLKIN1_JITTER_PS {50.0} CONFIG.MMCM_CLKFBOUT_MULT_F {5.000} CONFIG.MMCM_CLKIN1_PERIOD {5.000} CONFIG.MMCM_CLKIN2_PERIOD {10.0} CONFIG.MMCM_CLKOUT0_DIVIDE_F {25.000} CONFIG.CLKOUT1_JITTER {135.255} CONFIG.CLKOUT1_PHASE_ERROR {89.971}] [get_ips clk_40]

create_ip -name vio -vendor xilinx.com -library ip -module_name vio_uart
set_property -dict [list CONFIG.C_PROBE_OUT2_WIDTH {160} CONFIG.C_PROBE_IN1_WIDTH {160} CONFIG.C_NUM_PROBE_OUT {3} CONFIG.C_NUM_PROBE_IN {2} CONFIG.Component_Name {vio_uart}] [get_ips vio_uart]

create_ip -name fifo_generator -vendor xilinx.com -library ip -module_name fifo_160bit
set_property -dict [list CONFIG.Component_Name {fifo_160bit} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {160} CONFIG.Input_Depth {512} CONFIG.Output_Data_Width {160} CONFIG.Output_Depth {512} CONFIG.Use_Embedded_Registers {false} CONFIG.Data_Count_Width {9} CONFIG.Write_Data_Count_Width {9} CONFIG.Read_Data_Count_Width {9} CONFIG.Full_Threshold_Assert_Value {511} CONFIG.Full_Threshold_Negate_Value {510} CONFIG.Empty_Threshold_Assert_Value {4} CONFIG.Empty_Threshold_Negate_Value {5}] [get_ips fifo_160bit]


