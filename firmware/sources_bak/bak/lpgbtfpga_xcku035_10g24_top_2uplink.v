/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : lpgbtfpga_xcku035_10g24_top_2uplink.v
// Create : 2023-09-28 17:24:17
// Revise : 2023-10-02 16:55:10
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------


module lpgbtfpga_xcku035_10g24_top_2uplink(
    // --===============--
    //   -- Clocks scheme --
    //   --===============--       
    //   -- MGT(GTX) reference clock:
    //   ----------------------------
      
    //   -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
    //   --
    //   --          * The MGT reference clock frequency must be 320MHz.
    input SMA_MGT_REFCLK_P,
    input SMA_MGT_REFCLK_N,
        
      // -- Fabric clock: 200MHz
    input USER_CLOCK_P,
    input USER_CLOCK_N,

    input TRIGGER0_P,
    input TRIGGER0_N,

    // --hit to mezz
    output [23:0] hit,
    output mezz_TRST,

    // -- MGT(GTX) --
    output [0:0] SFP_TX_P,
    output [0:0] SFP_TX_N,
    input [0:0] SFP_RX_P,
    input [0:0] SFP_RX_N,
       
    // -- SFP control
    output [7:0] SFP_TX_DISABLE,

    // --====================--
    // -- Signals forwarding --
    // --====================--

    // -- SMA output:
    // --------------
    output USER_SMA_GPIO_P,
    output USER_SMA_GPIO_N,


    // -- 125 MHz clock from MMCM
    // --      gtx_clk_bufg_out:       OUT STD_LOGIC;
    output phy_resetn,

    // -- RGMII Interface
    // ------------------
    output [3:0] rgmii_txd,
    output rgmii_tx_ctl,
    output rgmii_txc,
    input [3:0] rgmii_rxd,
    input rgmii_rx_ctl,
    input rgmii_rxc,

    // -- MDIO Interface
    //     -----------------
    inout mdio,
    output mdc,

    // -- UART Interface
    input uart_rxd,
    output uart_txd
);

    localparam UPLINKCOUNT = 2;
    localparam DOWNLINKCOUNT = 1;
    
    localparam FEC5           =1;
    localparam FEC12          =2;
    localparam DATARATE_5G12  =1;
    localparam DATARATE_10G24 =2;
    localparam PCS            =0;
    localparam PMA            =1;

    // -- Clocks:
    wire mgtRefClk_from_smaMgtRefClkbuf;

    wire mgt_freedrpclk;

    wire lpgbtfpga_mgttxclk;
    wire lpgbtfpga_mgtrxclk;


    // -- User CDC for lpGBT-FPGA      
    wire lpgbtfpga_clk40;

    wire [2:0] uplinkPhase[UPLINKCOUNT-1:0];
    wire [2:0] uplinkPhaseCalib[UPLINKCOUNT-1:0];
    wire [UPLINKCOUNT-1:0] uplinkPhaseForce;
                   
                                                                     
    wire [9:0] downlinkPhase[DOWNLINKCOUNT-1:0];
    wire [9:0] downlinkPhaseCalib[DOWNLINKCOUNT-1:0];
    
    wire [DOWNLINKCOUNT-1:0] downlinkPhaseForce;

    // -- LpGBT-FPGA
    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrst;
    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrdy;
                                 

    wire lpgbtfpga_uplinkrst; 
    wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkrdy;
    

    wire [31:0] lpgbtfpga_downlinkUserData[DOWNLINKCOUNT-1:0];
    wire [31:0] lpgbtfpga_downlinkUserData_tmp[DOWNLINKCOUNT-1:0];
    

    wire [1:0] lpgbtfpga_downlinkEcData[DOWNLINKCOUNT-1:0];
    wire [1:0] lpgbtfpga_downlinkIcData[DOWNLINKCOUNT-1:0];

    wire [229:0] lpgbtfpga_uplinkUserData[UPLINKCOUNT-1:0];
    wire [1:0] lpgbtfpga_uplinkEcData[UPLINKCOUNT-1:0];
    wire [1:0] lpgbtfpga_uplinkIcData[UPLINKCOUNT-1:0];
    wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkclk;


    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txpolarity_m;

    wire [UPLINKCOUNT-1:0] lpgbtfpga_mgt_rxpolarity;


    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txaligned;
    wire [6:0]lpgbtfpga_mgt_txpiphase_m[DOWNLINKCOUNT-1:0];
    wire [6:0]lpgbtfpga_mgt_txpicalib_m[DOWNLINKCOUNT-1:0];
    wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txcaliben;
                    
    wire downLinkBypassInterleaver = 1'b0;
    wire downLinkBypassFECEncoder = 1'b0;
    wire downLinkBypassScrambler = 1'b0;

    wire upLinkScramblerBypass = 1'b0;
    wire upLinkFecBypass = 1'b0;
    wire upLinkInterleaverBypass = 1'b0;

    wire upLinkFECCorrectedClear = 1'b0;
    wire [UPLINKCOUNT-1:0] upLinkFECCorrectedLatched;



    // -- Config
    wire uplinkSelectDataRate = 1'b0;

    wire [DOWNLINKCOUNT-1:0] generator_rst;
    wire [1:0] downconfig_g0[DOWNLINKCOUNT-1:0];
    wire [1:0] downconfig_g1[DOWNLINKCOUNT-1:0];
    wire [1:0] downconfig_g2[DOWNLINKCOUNT-1:0];
    wire [1:0] downconfig_g3[DOWNLINKCOUNT-1:0];
    wire [15:0] downlink_gen_rdy[DOWNLINKCOUNT-1:0];

    wire [55:0] upelink_config[UPLINKCOUNT-1:0];
    wire [27:0] uperror_detected[UPLINKCOUNT-1:0];
    wire [UPLINKCOUNT-1:0] reset_upchecker;
    // ------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------

    // --SCA debug
    // -----------
    wire [2:0] sca_enable_array[DOWNLINKCOUNT-1:0];
    wire [DOWNLINKCOUNT-1:0]reset_sca;
    wire [DOWNLINKCOUNT-1:0]sca_ready;
    wire [DOWNLINKCOUNT-1:0]start_sca;
    wire [DOWNLINKCOUNT-1:0]start_reset;
    wire [DOWNLINKCOUNT-1:0]start_connect;

    wire [159:0] sca_rx_parr[DOWNLINKCOUNT-1:0];
    // --wire sca_rx_done: std_logic;

    // --wire ec_line_tx  : std_logic_vector(1 downto 0);
    // --wire ec_line_rx  : std_logic_vector(1 downto 0);

    wire tx_ready;
    wire [7:0] tx_addr[DOWNLINKCOUNT-1:0];
    wire [7:0] tx_ctrl[DOWNLINKCOUNT-1:0];
    wire [7:0] tx_trid[DOWNLINKCOUNT-1:0];
    wire [7:0] tx_ch[DOWNLINKCOUNT-1:0];
    wire [7:0] tx_len[DOWNLINKCOUNT-1:0];
    wire [7:0] tx_cmd[DOWNLINKCOUNT-1:0];
    wire [31:0] tx_data[DOWNLINKCOUNT-1:0];

    // --wire rx_addr: std_logic_vector(7 downto 0);
    // --wire rx_ctrl: std_logic_vector(7 downto 0);
    // --wire rx_trid: std_logic_vector(7 downto 0);
    // --wire rx_ch: std_logic_vector(7 downto 0);
    // --wire rx_len: std_logic_vector(7 downto 0);
    // --wire rx_err: std_logic_vector(7 downto 0);
    // --wire rx_data: std_logic_vector(31 downto 0);


    wire [2:0] sca_rx_done[DOWNLINKCOUNT-1:0][2:0];  //  Reply received flag (pulse)
    wire [7:0] rx_addr[DOWNLINKCOUNT-1:0][2:0];  //  --! Reply: address field (According to the SCA manual)
    wire [7:0] rx_ctrl[DOWNLINKCOUNT-1:0][2:0];  //  --! Reply: control field (According to the SCA manual)
    wire [7:0] rx_trid[DOWNLINKCOUNT-1:0][2:0];  //  --! Reply: transaction ID field (According to the SCA manual)
    wire [7:0] rx_ch[DOWNLINKCOUNT-1:0][2:0];  //    --! Reply: channel field (According to the SCA manual)
    wire [7:0] rx_len[DOWNLINKCOUNT-1:0][2:0];  //   --! Reply: len field (According to the SCA manual)
    wire [7:0] rx_err[DOWNLINKCOUNT-1:0][2:0];  //   --! Reply: error field (According to the SCA manual)
    wire [31:0] rx_data[DOWNLINKCOUNT-1:0][2:0];  // --!Reply: data field (According to the SCA manual)

    wire [23:0] rx_addr_24[DOWNLINKCOUNT-1:0];
    wire [23:0] rx_ctrl_24[DOWNLINKCOUNT-1:0];
    wire [23:0] rx_trid_24[DOWNLINKCOUNT-1:0];
    wire [23:0] rx_ch_24[DOWNLINKCOUNT-1:0];
    wire [23:0] rx_len_24[DOWNLINKCOUNT-1:0];
    wire [23:0] rx_err_24[DOWNLINKCOUNT-1:0];
    wire [95:0] rx_data_96[DOWNLINKCOUNT-1:0];


    // -- EC line
    wire [1:0]ec_line_tx[DOWNLINKCOUNT-1:0][2:0];       // --! (TX) Array of bits to be mapped to the TX GBT-Frame
    wire [1:0]ec_line_rx[DOWNLINKCOUNT-1:0][2:0];       // --! (RX) Array of bits to be mapped to the RX GBT-Frame


    // --wire m_axi_awaddr         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    // --wire m_axi_awprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    // --wire m_axi_awvalid         :  STD_LOGIC;
    // --wire m_axi_awready         :  STD_LOGIC;
    // --wire m_axi_wdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    // --wire m_axi_wstrb         :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    // --wire m_axi_wvalid         :  STD_LOGIC;
    // --wire m_axi_wready         :  STD_LOGIC;
    // --wire m_axi_bresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    // --wire m_axi_bvalid         :  STD_LOGIC;
    // --wire m_axi_bready         :  STD_LOGIC;
    // --wire m_axi_araddr         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    // --wire m_axi_arprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    // --wire m_axi_arvalid         :  STD_LOGIC;
    // --wire m_axi_arready         :  STD_LOGIC;
    // --wire m_axi_rdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    // --wire m_axi_rresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    // --wire m_axi_rvalid         :  STD_LOGIC;
    // --wire m_axi_rready         :  STD_LOGIC;


    // -- IC Debug
    // ------------
    wire [DOWNLINKCOUNT-1:0] ic_ready;
    wire [DOWNLINKCOUNT-1:0] ic_empty;

    


    wire [7:0] GBTx_address[DOWNLINKCOUNT-1:0];
    wire [15:0] Register_ptr[DOWNLINKCOUNT-1:0];
    wire [15:0] ic_nb_to_be_read_rd[DOWNLINKCOUNT-1:0];

    wire [DOWNLINKCOUNT-1:0] ic_start_wr;
    wire [DOWNLINKCOUNT-1:0] ic_wr;
    wire [7:0] ic_data_wr[DOWNLINKCOUNT-1:0];  

    wire [DOWNLINKCOUNT-1:0] ic_start_rd;    
    wire [DOWNLINKCOUNT-1:0] ic_rd;
    wire [7:0] ic_data_rd[DOWNLINKCOUNT-1:0];

    wire [7:0] GBTx_rd_addr[DOWNLINKCOUNT-1:0];
    wire [15:0] GBTx_rd_mem_ptr[DOWNLINKCOUNT-1:0];
    wire [15:0] GBTx_rd_mem_size[DOWNLINKCOUNT-1:0];
    wire ic_empty_rd;

    //to gbtic
    wire [7:0] GBTx_address_to_gbtic[DOWNLINKCOUNT-1:0];
    wire [15:0] Register_addr_to_gbtic[DOWNLINKCOUNT-1:0];
    wire [15:0] nb_to_be_read_to_gbtic[DOWNLINKCOUNT-1:0];

    wire [DOWNLINKCOUNT-1:0] start_write_to_gbtic;
    wire [DOWNLINKCOUNT-1:0] wr_to_gbtic;
    wire [7:0] data_to_gbtic[DOWNLINKCOUNT-1:0];  

    wire [DOWNLINKCOUNT-1:0] start_read_to_gbtic; 
    wire [DOWNLINKCOUNT-1:0] rd_to_gbtic;
    wire [7:0] data_from_gbtic[DOWNLINKCOUNT-1:0];

    wire [7:0] ic_rd_gbtx_addr[DOWNLINKCOUNT-1:0];
    wire [15:0] ic_rd_mem_ptr[DOWNLINKCOUNT-1:0];
    wire [15:0] ic_rd_nb_of_words[DOWNLINKCOUNT-1:0];

    //elink sca data
    wire [DOWNLINKCOUNT-1:0]txFrameClkPllLocked_from_gbtExmplDsgn;
    wire [7:0] elink_320m_sca0_pri[DOWNLINKCOUNT-1:0];
    wire [7:0] elink_320m_sca0_aux[DOWNLINKCOUNT-1:0];
    wire [7:0] elink_320m_sca1_pri[DOWNLINKCOUNT-1:0];
    wire [7:0] elink_320m_sca1_aux[DOWNLINKCOUNT-1:0];
    wire [7:0] elink_320m_sca2_pri[DOWNLINKCOUNT-1:0];
    wire [7:0] elink_320m_sca2_aux[DOWNLINKCOUNT-1:0];
    wire [1:0] up_elink_80m_sca0_pri[DOWNLINKCOUNT-1:0];
    wire [1:0] up_elink_80m_sca0_aux[DOWNLINKCOUNT-1:0];
    wire [1:0] up_elink_80m_sca1_pri[DOWNLINKCOUNT-1:0];
    wire [1:0] up_elink_80m_sca1_aux[DOWNLINKCOUNT-1:0];
    wire [1:0] up_elink_80m_sca2_pri[DOWNLINKCOUNT-1:0];
    wire [1:0] up_elink_80m_sca2_aux[DOWNLINKCOUNT-1:0];

    wire [1:0] down_elink_80m_sca0_pri[DOWNLINKCOUNT-1:0];
    wire [1:0] down_elink_80m_sca0_aux[DOWNLINKCOUNT-1:0];
    wire [1:0] down_elink_80m_sca1_pri[DOWNLINKCOUNT-1:0];
    wire [1:0] down_elink_80m_sca1_aux[DOWNLINKCOUNT-1:0];
    wire [1:0] down_elink_80m_sca2_pri[DOWNLINKCOUNT-1:0];
    wire [1:0] down_elink_80m_sca2_aux[DOWNLINKCOUNT-1:0];


    // -- Frame clock:
    // ---------------
    wire txFrameClk_from_txPll;

    wire fast_clock_for_meas;
    wire [31:0] cmd_delay;

    wire [1:0] ttc_ctrl;
    wire trigger_SMA;

    wire LOCK_TOP;


    //Ethernet
    wire sys_clk_160;
    wire clk_200;
    wire tx_axis_clk;
    wire [7:0] tx_axis_fifo_tdata;
    wire tx_axis_fifo_tvalid;
    wire tx_axis_fifo_tready;
    wire tx_axis_fifo_tlast;




    // -- Reset controll    
    assign SFP_TX_DISABLE = 'b0;

//    rx_addr_24  <= rx_addr(2) & rx_addr(1) & rx_addr(0);
//    rx_ctrl_24  <= rx_ctrl(2) & rx_ctrl(1) & rx_ctrl(0);
//    rx_trid_24  <= rx_trid(2) & rx_trid(1) & rx_trid(0);
//    rx_ch_24    <= rx_ch  (2) & rx_ch  (1) & rx_ch  (0);
//    rx_len_24   <= rx_len (2) & rx_len (1) & rx_len (0);
//    rx_err_24   <= rx_err (2) & rx_err (1) & rx_err (0);
//    rx_data_96  <= rx_data(2) & rx_data(1) & rx_data(0);

genvar i;
for (i = 0; i < DOWNLINKCOUNT; i = i + 1) begin
    assign down_elink_80m_sca0_aux[i] = ec_line_tx[i][0];
    assign down_elink_80m_sca1_pri[i] = ec_line_tx[i][1];
    assign down_elink_80m_sca2_pri[i] = ec_line_tx[i][2];
    assign ec_line_rx[i][2] = up_elink_80m_sca2_pri[i];
    assign ec_line_rx[i][1] = up_elink_80m_sca1_pri[i];
    assign ec_line_rx[i][0] = up_elink_80m_sca0_aux[i];

        // -- Data pattern generator / checker (PRBS7)
    lpgbtfpga_patterngen lpgbtfpga_patterngen_inst(
        // --clk40Mhz_Tx_i      : in  std_logic;
        .clk320DnLink_i(lpgbtfpga_clk40),
        .clkEnDnLink_i(1'b1),
        .generator_rst_i(generator_rst[i]),

        // -- Group configurations:
        // --    "11": 320Mbps
        // --    "10": 160Mbps
        // --    "01": 80Mbps
        // --    "00": Fixed pattern
        .config_group0_i           (downconfig_g0[i]),
        .config_group1_i           (downconfig_g1[i]),
        .config_group2_i           (downconfig_g2[i]),
        .config_group3_i           (downconfig_g3[i]),
        .downlink_o                (lpgbtfpga_downlinkUserData[i]),
        .fixed_pattern_i           ('h12345678),
        .eport_gen_rdy_o           (downlink_gen_rdy[i])
    );

    vio_downlink vio_downlink_inst (
        .clk(mgt_freedrpclk),                  // input wire clk
        .probe_in0(lpgbtfpga_downlinkrdy[i]),      // input wire [0 : 0] probe_in0
        .probe_in1(downlink_gen_rdy[i]),      // input wire [15 : 0] probe_in1
        .probe_in2(lpgbtfpga_mgt_txaligned[i]),      // input wire [0 : 0] probe_in2
        .probe_in3(lpgbtfpga_mgt_txpiphase[i]),      // input wire [6 : 0] probe_in3
        .probe_in4(downlinkPhase[i]),      // input wire [9 : 0] probe_in4

        .probe_out0(lpgbtfpga_downlinkrst[i]),    // output wire [0 : 0] probe_out0
        .probe_out1(downLinkBypassInterleaver[i]),    // output wire [0 : 0] probe_out1
        .probe_out2(downLinkBypassFECEncoder[i]),    // output wire [0 : 0] probe_out2
        .probe_out3(downLinkBypassScrambler[i]),    // output wire [0 : 0] probe_out3
        .probe_out4(lpgbtfpga_mgt_txpicalib[i]),    // output wire [6 : 0] probe_out4
        .probe_out5(lpgbtfpga_mgt_txcaliben[i]),    // output wire [0 : 0] probe_out5
        .probe_out6(generator_rst[i]),    // output wire [0 : 0] probe_out6
        .probe_out7(downconfig_g0[i]),    // output wire [1 : 0] probe_out7
        .probe_out8(downconfig_g1[i]),    // output wire [1 : 0] probe_out8
        .probe_out9(downconfig_g2[i]),    // output wire [1 : 0] probe_out9
        .probe_out10(downconfig_g3[i]),  // output wire [1 : 0] probe_out10
        .probe_out11(lpgbtfpga_mgt_txpolarity[i]),  // output wire [0 : 0] probe_out11
        .probe_out12(downlinkPhaseCalib[i]),  // output wire [9 : 0] probe_out12
        .probe_out13(downlinkPhaseForce[i])  // output wire [0 : 0] probe_out13
    );

end

genvar j;
for (j = 0; j < UPLINKCOUNT; j = j + 1) begin
    vio_uplink vio_uplink_inst (
        .clk(mgt_freedrpclk),                // input wire clk
        .probe_in0(lpgbtfpga_uplinkrdy[j]),    // input wire [0 : 0] probe_in0
        .probe_in1(uperror_detected[j]),    // input wire [27 : 0] probe_in1
        .probe_in2(upLinkFECCorrectedLatched[j]),    // input wire [0 : 0] probe_in2
        .probe_in3(uplinkPhase[j]),    // input wire [2 : 0] probe_in3

        .probe_out0(lpgbtfpga_uplinkrst[j]),  // output wire [0 : 0] probe_out0
        .probe_out1(upLinkInterleaverBypass[j]),  // output wire [0 : 0] probe_out1
        .probe_out2(upLinkFecBypass[j]),  // output wire [0 : 0] probe_out2
        .probe_out3(upLinkScramblerBypass[j]),  // output wire [0 : 0] probe_out3
        .probe_out4(reset_upchecker[j]),  // output wire [0 : 0] probe_out4
        .probe_out5(upelink_config[j]),  // output wire [53 : 0] probe_out5
        .probe_out6(lpgbtfpga_mgt_rxpolarity[j]),  // output wire [0 : 0] probe_out6
        .probe_out7(upLinkFECCorrectedClear[j]),  // output wire [0 : 0] probe_out7
        .probe_out8(uplinkPhaseCalib[j]),  // output wire [2 : 0] probe_out8
        .probe_out9(uplinkPhaseForce[j])  // output wire [0 : 0] probe_out9
);
end



// -- Clocks
    
// -- MGT(GTX) reference clock:
// ----------------------------   
// -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
// --          * The MGT reference clock frequency must be 320MHz for the latency-optimized GBT Bank. 

// IBUFDS_GTE3: Gigabit Transceiver Buffer
//              UltraScale
// Xilinx HDL Language Template, version 2021.1

IBUFDS_GTE3 #(
   .REFCLK_EN_TX_PATH(1'b0),   // Refer to Transceiver User Guide.
   .REFCLK_HROW_CK_SEL(2'b00), // Refer to Transceiver User Guide.
   .REFCLK_ICNTL_RX(2'b00)     // Refer to Transceiver User Guide.
)
smaMgtRefClkIbufdsGtxe2 (
   .O(mgtRefClk_from_smaMgtRefClkbuf),         // 1-bit output: Refer to Transceiver User Guide.
   .ODIV2(), // 1-bit output: Refer to Transceiver User Guide.
   .CEB(1'b0),     // 1-bit input: Refer to Transceiver User Guide.
   .I(SMA_MGT_REFCLK_P),         // 1-bit input: Refer to Transceiver User Guide.
   .IB(SMA_MGT_REFCLK_N)        // 1-bit input: Refer to Transceiver User Guide.
);

// -- Frame clock

xlx_k7v7_tx_pll txPll
(
    .txFrameClk  (txFrameClk_from_txPll),
    .sys_clk_160 (sys_clk_160),
    .reset       (1'b0),
    .locked      (txFrameClkPllLocked_from_gbtExmplDsgn),
    .clk_in1     (lpgbtfpga_clk40)
);
assign LOCK_TOP = txFrameClkPllLocked_from_gbtExmplDsgn;

IBUFDS 
IBUFDS_inst (
   .O(trigger_SMA),   // 1-bit output: Buffer output
   .I(TRIGGER0_P),   // 1-bit input: Diff_p buffer input (connect directly to top-level port)
   .IB(TRIGGER0_N)  // 1-bit input: Diff_n buffer input (connect directly to top-level port)
);
    
sys_clk sys_clk_inst
(
    .mgt_freedrpclk (mgt_freedrpclk),
    .clk_200        (clk_200),
    .locked         (locked),
    .clk_in1_p      (USER_CLOCK_P),
    .clk_in1_n      (USER_CLOCK_N)
);

            
  // -- In this example design, the 40MHz clock used for the user logic is derived from a division of the Tx user clock of the MGT
  // -- It should be noted, that in realistic cases, this clock typically comes from an external PLL (sync. to the MGT Tx reference clock)
  
BUFGCE_DIV #(
   .BUFGCE_DIVIDE(8),              // 1-8
   // Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
   .IS_CE_INVERTED(1'b0),          // Optional inversion for CE
   .IS_CLR_INVERTED(1'b0),         // Optional inversion for CLR
   .IS_I_INVERTED(1'b0),           // Optional inversion for I
   .SIM_DEVICE("ULTRASCALE"))  // ULTRASCALE, ULTRASCALE_PLUS
BUFGCE_DIV_inst (
   .O(lpgbtfpga_clk40),     // 1-bit output: Buffer
   .CE(1'b1),   // 1-bit input: Buffer enable
   .CLR(1'b0), // 1-bit input: Asynchronous clear
   .I(lpgbtfpga_mgtrxclk)      // 1-bit input: Buffer 320M clk_mgtRxClk_o
);

lpgbtFpga_10g24_multiuplink #(
    .FEC(FEC5),
    .UPLINKCOUNT(UPLINKCOUNT),
    .DOWNLINKCOUNT(DOWNLINKCOUNT))
inst_lpgbtFpga_10g24_multiuplink (
//Clocks
    .downlinkClk_i               (lpgbtfpga_clk40),
    .uplinkClk_i                 (lpgbtfpga_clk40),
    .downlinkRst_i               (lpgbtfpga_downlinkrst),
    .uplinkRst_i                 (lpgbtfpga_uplinkrst),
//Downlink
    .downlinkUserData_i          (lpgbtfpga_downlinkUserData_tmp),
    .downlinkEcData_i            (lpgbtfpga_downlinkEcData),
    .downlinkIcData_i            (lpgbtfpga_downlinkIcData),
    .downLinkBypassInterleaver_i (downLinkBypassInterleaver),
    .downLinkBypassFECEncoder_i  (downLinkBypassFECEncoder),
    .downLinkBypassScrambler_i   (downLinkBypassScrambler),
    .downlinkReady_o             (lpgbtfpga_downlinkrdy),
    .downlinkPhase_o             (downlinkPhase),
    .downlinkPhaseCalib_i        (downlinkPhaseCalib),
    .downlinkPhaseForce_i        (downlinkPhaseForce),
//uplink
    .uplinkUserData_o            (lpgbtfpga_uplinkUserData),
    .uplinkEcData_o              (lpgbtfpga_uplinkEcData),
    .uplinkIcData_o              (lpgbtfpga_uplinkIcData),
    .uplinkBypassInterleaver_i   (upLinkInterleaverBypass),
    .uplinkBypassFECEncoder_i    (upLinkFecBypass),
    .uplinkBypassScrambler_i     (upLinkScramblerBypass),
    .uplinkFECCorrectedClear_i   (upLinkFECCorrectedClear),
    .uplinkFECCorrectedLatched_o (upLinkFECCorrectedLatched),
    .uplinkReady_o               (lpgbtfpga_uplinkrdy),
    .uplinkPhase_o               (uplinkPhase),
    .uplinkPhaseCalib_i          (uplinkPhaseCalib),
    .uplinkPhaseForce_i          (uplinkPhaseForce),
//MGT
    .mgt_rxn_i                   (SFP_RX_N),
    .mgt_rxp_i                   (SFP_RX_P),
    .mgt_txn_o                   (SFP_TX_N),
    .mgt_txp_o                   (SFP_TX_P),
    .clk_mgtrefclk_i             (mgtRefClk_from_smaMgtRefClkbuf),
    .clk_mgtfreedrpclk_i         (mgt_freedrpclk),
    .clk_mgtRxClk_o              (lpgbtfpga_mgtrxclk),
    .clk_mgtTxClk_o              (lpgbtfpga_mgttxclk),
    .mgt_txpolarity_i            (lpgbtfpga_mgt_txpolarity),
    .mgt_rxpolarity_i            (lpgbtfpga_mgt_rxpolarity),
    .mgt_txcaliben_i             (lpgbtfpga_mgt_txcaliben),
    .mgt_txcalib_i               (lpgbtfpga_mgt_txpicalib),
    .mgt_txaligned_o             (lpgbtfpga_mgt_txaligned),
    .mgt_txphase_o               (lpgbtfpga_mgt_txpiphase)
);



    // -- Data stimulis
    // lpgbtfpga_downlinkEcData     <= (others => '1');
    // --lpgbtfpga_downlinkIcData_m     <= (others => '1');
    
    

// --    lpgbtfpga_patternchecker_inst: lpgbtfpga_patternchecker
// --        port map(
// --            reset_checker_i  => reset_upchecker_s,
// --            ser320_clk_i     => lpgbtfpga_clk40,
// --            ser320_clkEn_i   => '1',
    
// --            data_rate_i      => uplinkSelectDataRate_s,
    
// --            elink_config_i   => upelink_config_s,
    
// --            error_detected_o => uperror_detected_s,
    
// --            userDataUpLink_i => lpgbtfpga_uplinkUserData_s
// --        );


tdc_decoder_top tdc_decoder_top_inst
(
    .clk_40              (lpgbtfpga_clk40),
    .rst_in              (reset_upchecker_s),
    .userDataUpLink      (lpgbtfpga_uplinkUserData),
    .trigger_in          (trigger_SMA),
    // .error_detected_o    (error_detected_o),
    .encode_ttc          (ttc_ctrl),
    .hit                 (hit),
    .mezz_TRST           (mezz_TRST),
    .SCA0_PRI            (elink_320m_sca0_pri),
    .SCA0_AUX            (elink_320m_sca0_aux),
    .SCA1_PRI            (elink_320m_sca1_pri),
    .SCA1_AUX            (elink_320m_sca1_aux),
    .SCA2_PRI            (elink_320m_sca2_pri),
    .SCA2_AUX            (elink_320m_sca2_aux),
    .sys_clk_160         (sys_clk_160),
    .tx_axis_clk         (tx_axis_clk),
    .tx_axis_fifo_tdata  (tx_axis_fifo_tdata),
    .tx_axis_fifo_tvalid (tx_axis_fifo_tvalid),
    .tx_axis_fifo_tready (tx_axis_fifo_tready),
    .tx_axis_fifo_tlast  (tx_axis_fifo_tlast)
);


// --      USER_SMA_GPIO_P <= lpgbtfpga_clk40;
//       --USER_SMA_GPIO_N <= lpgbtfpga_mgtrxclk;
      
      
   // --jtag_master_inst : debug_jtag_master
   // --  PORT MAP (
   // --    aclk          =>    txFrameClk_from_txPll,
   // --    aresetn       => txFrameClkPllLocked_from_gbtExmplDsgn,
   // --    m_axi_awaddr  => m_axi_awaddr,
   // --    m_axi_awprot  => m_axi_awprot,
   // --    m_axi_awvalid => m_axi_awvalid,
   // --    m_axi_awready => m_axi_awready,
   // --    m_axi_wdata   => m_axi_wdata,
   // --    m_axi_wstrb   => m_axi_wstrb,
   // --    m_axi_wvalid  => m_axi_wvalid,
   // --    m_axi_wready  => m_axi_wready,
   // --    m_axi_bresp   => m_axi_bresp,
   // --    m_axi_bvalid  => m_axi_bvalid,
   // --    m_axi_bready  => m_axi_bready,
   // --    m_axi_araddr  => m_axi_araddr,
   // --    m_axi_arprot  => m_axi_arprot,
   // --    m_axi_arvalid => m_axi_arvalid,
   // --    m_axi_arready => m_axi_arready,
   // --    m_axi_rdata   => m_axi_rdata,
   // --    m_axi_rresp   => m_axi_rresp,
   // --    m_axi_rvalid  => m_axi_rvalid,
   // --    m_axi_rready  => m_axi_rready
   // --  );
         
// -- lpgbtfpga_uplinkUserData_s 320Mbps
// -- LpGBT_CSM V2 : sca0_aux -- elink_320m_s(17)
// -- elink_320m_s(17)     <= userDataUpLink_i(143 downto 136)

// -- elink_320m_sca0_aux (7 downto 0)           <=  lpgbtfpga_uplinkUserData_s(143 downto 136);    
// -- up_elink_80m_sca0_aux (1 downto 0)         <=  elink_320m_sca0_aux (5) & elink_320m_sca0_aux (1);    

// -- lpgbtfpga_downlinkUserData_tmp (31 downto 0)  <= x"12" & down_elink_80m_sca0_aux (1 downto 0) & "11" & x"45678";

// --  LpGBT_CSM V2 : sca1_pri -- elink_320m_s(12)
// --  elink_320m_s(12)     <= userDataUpLink_i(103 downto 96)

//  --elink_320m_sca1_pri(7 downto 0)           <=  lpgbtfpga_uplinkUserData_s(103 downto 96);
//  --elink_320m_sca0_aux(7 downto 0)           <=  lpgbtfpga_uplinkUserData_s(143 downto 136);
//  --up_elink_80m_sca1_pri (1 downto 0)         <=  elink_320m_sca1_pri (4) & elink_320m_sca1_pri (0);
//  --up_elink_80m_sca1_pri (1 downto 0)         <=  elink_320m_sca1_pri (4) & elink_320m_sca1_pri (0);

//    up_elink_80m_sca0_pri (1 downto 0)         <=  elink_320m_sca0_pri (4) & elink_320m_sca0_pri (0);
//    up_elink_80m_sca0_aux (1 downto 0)         <=  elink_320m_sca0_aux (4) & elink_320m_sca0_aux (0);
//    up_elink_80m_sca1_pri (1 downto 0)         <=  elink_320m_sca1_pri (4) & elink_320m_sca1_pri (0);
//    up_elink_80m_sca1_aux (1 downto 0)         <=  elink_320m_sca1_aux (4) & elink_320m_sca1_aux (0);
//    up_elink_80m_sca2_pri (1 downto 0)         <=  elink_320m_sca2_pri (4) & elink_320m_sca2_pri (0);
//    up_elink_80m_sca2_aux (1 downto 0)         <=  elink_320m_sca2_aux (4) & elink_320m_sca2_aux (0);


//    lpgbtfpga_downlinkUserData_tmp (31 downto 0)  <= x"00" & down_elink_80m_sca0_aux & "00" & down_elink_80m_sca0_pri & down_elink_80m_sca2_pri &
//                                                down_elink_80m_sca2_aux & down_elink_80m_sca1_aux & down_elink_80m_sca1_pri &
//                                                x"00" & ttc_ctrl;

 

gbtsc_top #(
    // -- IC configuration
    .g_IC_FIFO_DEPTH(453), //  --! Defines the depth of the FIFO used to handle the Internal control (Max. number of words/bytes can be read/write from/to a GBTx)
    .g_ToLpGBT(1),    //      --! 1 to use LpGBT. Otherwise, it should be 0

    // -- EC configuration
    .g_SCA_COUNT(3)//              --! Defines the maximum number of SCA that can be connected to this module
)
gbtsc_inst (
    // -- Clock & reset
    .tx_clk_i           (txFrameClk_from_txPll),       //                            --! Tx clock (Tx_frameclk_o from GBT-FPGA IP): must be a multiple of the LHC frequency
    .tx_clk_en          (1'b1),                        //    --! Tx clock enable signal must be used in case of multi-cycle path(tx_clk_i > LHC frequency). By default: always enabled

    .rx_clk_i           (txFrameClk_from_txPll),       //                             --! Rx clock (Rx_frameclk_o from GBT-FPGA IP): must be a multiple of the LHC frequency
    .rx_clk_en          (1'b1),                        //    --! Rx clock enable signal must be used in case of multi-cycle path(rx_clk_i > LHC frequency). By default: always enabled

    .rx_reset_i         (reset_sca),   //                                   --! Reset RX datapath
    .tx_reset_i         (reset_sca),   //                                  --! Reset TX datapath

    // -- IC control
    .tx_start_write_i    (start_write_to_gbtic),        //                            --! Request a write config. to the GBTx (IC)
    .tx_start_read_i     (start_read_to_gbtic),         //                          --! Request a read config. to the GBTx (IC)

    // -- IC configuration
    .tx_GBTx_address_i   (GBTx_address_to_gbtic),       //          --! I2C address of the GBTx
    .tx_register_addr_i  (Register_addr_to_gbtic),      //          --! Address of the first register to be accessed
    .tx_nb_to_be_read_i  (nb_to_be_read_to_gbtic),      //         --! Number of words/bytes to be read (only for read transactions)

    // -- IC FIFO control
    .wr_clk_i            (txFrameClk_from_txPll),  //             --! Fifo's writing clock
    .tx_wr_i             (wr_to_gbtic),            //                      --! Request a write operation into the internal FIFO (Data to GBTx)
    .tx_data_to_gbtx_i   (data_to_gbtic),           //      --! Data to be written into the internal FIFO

    .rd_clk_i            (txFrameClk_from_txPll),    
    .rx_rd_i             (rd_to_gbtic),              //                     --! Request a read operation of the internal FIFO (GBTx reply)
    .rx_data_from_gbtx_o (data_from_gbtic),          //     --! Data from the FIFO

    // -- IC Status
    .tx_ready_o          (ic_ready),                 //                  --! IC core ready for a transaction
    .rx_empty_o          (ic_empty),                  //                --! Rx FIFO is empty (no reply from GBTx)

    .rx_gbtx_addr_o      (ic_rd_gbtx_addr),         //        --! I2C address of the GBTx (read from a reply)
    .rx_mem_ptr_o        (ic_rd_mem_ptr),           //    --! I2C address of the first register read/written
    .rx_nb_of_words_o    (ic_rd_nb_of_words),       //        --! Number of words/bytes read/written

    // -- SCA control
    .sca_enable_i        (sca_enable_array),   //--! Enable flag to select SCAs
    .start_reset_cmd_i   (start_reset),        //                           --! Send a reset command to the enabled SCAs
    .start_connect_cmd_i (start_connect),      //                            --! Send a connect command to the enabled SCAs
    .start_command_i     (start_sca),          //                         --! Send the command set in input to the enabled SCAs
    .inject_crc_error    (1'b0),               //                   --! Emulate a CRC error

    // -- SCA command
    .tx_address_i        (tx_addr),           //     --! Command: address field (According to the SCA manual)
    .tx_transID_i        (tx_trid),           //    --! Command: transaction ID field (According to the SCA manual)
    .tx_channel_i        (tx_ch),             //   --! Command: channel field (According to the SCA manual)
    .tx_command_i        (tx_cmd),            //    --! Command: command field (According to the SCA manual)
    .tx_data_i           (tx_data),           //    --! Command: data field (According to the SCA manual)

    .rx_received_o    (sca_rx_done),   //--! Reply received flag (pulse)
    .rx_address_o     (rx_addr),       //   --! Reply: address field (According to the SCA manual)
    .rx_control_o     (open),          //--! Reply: control field (According to the SCA manual)
    .rx_transID_o     (rx_trid),       //   --! Reply: transaction ID field (According to the SCA manual)
    .rx_channel_o     (rx_ch),         // --! Reply: channel field (According to the SCA manual)
    .rx_len_o         (rx_len),        // --! Reply: len field (According to the SCA manual)
    .rx_error_o       (rx_err),        //   --! Reply: error field (According to the SCA manual)
    .rx_data_o        (rx_data),       //   --! Reply: data field (According to the SCA manual)

    // -- EC line
    .ec_data_o        (ec_line_tx), // -- downlink -- txData_to_gbtExmplDsgn(81 downto 80),        --! (TX) Array of bits to be mapped to the TX GBT-Frame
    .ec_data_i        (ec_line_rx), //-- uplink  -- rxData_from_gbtExmplDsgn(81 downto 80),        --! (RX) Array of bits to be mapped to the RX GBT-Frame

    // -- IC lines
    .ic_data_o           (lpgbtfpga_downlinkIcData_m),   //             --! (TX) Array of bits to be mapped to the TX GBT-Frame (bits 83/84)
    .ic_data_i           (lpgbtfpga_uplinkIcData_m)     //            --! (RX) Array of bits to be mapped to the RX GBT-Frame (bits 83/84)

);

BUFG delaymeasclk_inst (
   .O(fast_clock_for_meas), // 1-bit output: Clock output.
   .I(mgtRefClk_from_smaMgtRefClkbuf)  // 1-bit input: Clock input.
);


gbtic_controller inst_gbtic_controller
(
    .clk                 (txFrameClk_from_txPll),
    .rst                 (reset_upchecker_s),

    //-- Data to GBT-IC State machine 
    .ic_ready            (ic_ready),
    .ic_empty            (ic_empty),

    //-- Configuration
    .GBTx_address        (GBTx_address_to_gbtic),
    .Register_addr       (Register_addr_to_gbtic),
    .nb_to_be_read       (nb_to_be_read_to_gbtic),

    //-- Control
    .ic_wr_start         (start_write_to_gbtic),
    .ic_rd_start         (start_read_to_gbtic),

    //-- WRITE register(s)
    .ic_wfifo_data       (data_to_gbtic),
    .ic_fifo_wr          (wr_to_gbtic),

    //-- READ register(s)    
    .ic_rfifo_data       (data_from_gbtic),
    .ic_fifo_rd          (rd_to_gbtic),

    //-- UART interface
    .rxd                 (uart_rxd),
    .txd                 (uart_txd),

    //-- To GBT-SC
    .reset_gbtsc         (reset_sca),
    .start_reset         (start_reset),
    .start_connect       (start_connect),
    .start_command       (start_sca),
    .sca_enable_array    (sca_enable_array),

    .tx_address          (tx_addr),
    .tx_transID          (tx_trid),
    .tx_channel          (tx_ch),
    .tx_len              (tx_len),
    .tx_command          (tx_cmd),
    .tx_data             (tx_data),

    .rx_reply_received_i (sca_rx_done),
    .rx_address          (rx_addr_24),
    .rx_transID          (rx_trid_24),
    .rx_channel          (rx_ch_24),
    .rx_len              (rx_len_24),
    .rx_error            (rx_err_24),
    .rx_data             (rx_data_96)
);



tri_mode_ethernet_mac_0_example_design ethernet_mac
(
    .glbl_rst            (reset_upchecker_s),
    .clk_200             (clk_200),

    //-- 125 MHz clock from MMCM
    .gtx_clk_bufg_out    (tx_axis_clk),
    .phy_resetn          (phy_resetn),

    //-- RGMII terface
    .rgmii_txd           (rgmii_txd),
    .rgmii_tx_ctl        (rgmii_tx_ctl),
    .rgmii_txc           (rgmii_txc),
    .rgmii_rxd           (rgmii_rxd),
    .rgmii_rx_ctl        (rgmii_rx_ctl),
    .rgmii_rxc           (rgmii_rxc),

    //-- MDIO terface
    .mdio                (mdio),
    .mdc                 (mdc),

    //-- USER side TX AXI-S terface       
    .tx_axis_fifo_tdata  (tx_axis_fifo_tdata),
    .tx_axis_fifo_tvalid (tx_axis_fifo_tvalid),
    .tx_axis_fifo_tready (tx_axis_fifo_tready),
    .tx_axis_fifo_tlast  (tx_axis_fifo_tlast)
);


endmodule
