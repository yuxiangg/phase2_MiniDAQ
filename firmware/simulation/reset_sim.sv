/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : reset_sim.sv
// Create : 2024-02-01 21:37:04
// Revise : 2024-02-01 22:37:43
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------  





module reset_sim();

reg reset_in;
reg clk_40;
wire reset_40;
wire reset_50;
wire reset_160;
always begin
  clk_40 <= 1'b1;
  #12.5;
  clk_40 <= 1'b0;
  #12.5;
end

reg clk_50;
always begin
  clk_50 <= 1'b1;
  #10;
  clk_50 <= 1'b0;
  #10;
end

reg clk_160;
always begin
  clk_160 <= 1'b1;
  #3.125;
  clk_160 <= 1'b0;
  #3.125;
end


xlx_ku_mgt_ip_reset_synchronizer rst_sync1(
 .clk_in(clk_50),
 .rst_in(reset_in),
 .rst_out(reset_50)
);

xlx_ku_mgt_ip_reset_synchronizer rst_sync2(
 .clk_in(clk_40),
 .rst_in(reset_in),
 .rst_out(reset_40)
);

xlx_ku_mgt_ip_reset_synchronizer rst_sync3(
 .clk_in(clk_160),
 .rst_in(reset_in),
 .rst_out(reset_160)
);


initial begin
  reset_in = 1'b0;

  # 10;
  reset_in = 1'b1;
  # 100;
  reset_in = 1'b0;
  # 200;


  $stop;
end


endmodule
