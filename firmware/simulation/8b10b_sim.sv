/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : 8b10b_sim.sv
// Create : 2024-03-20 17:29:20
// Revise : 2024-03-20 19:46:36
// Editor : sublime text4, tab size (2)
// Description: 
//
// -----------------------------------------------------------------------------  





module a8b10b_sim();

reg reset_in;
reg clk_40;
wire [7:0] Dout;
reg [9:0] Din;
wire DErr, Kout, KErr, DpErr;
reg en;
wire DErr_and_KErr, DErr_and_Kout, KErr_and_Kout;
assign DErr_and_KErr = DErr & KErr;
assign DErr_and_Kout = DErr & Kout;
assign KErr_and_Kout = KErr & Kout;

always begin
  clk_40 <= 1'b1;
  #12.5;
  clk_40 <= 1'b0;
  #12.5;
end

mDec8b10bMem_tb 
    inst_mDec8b10bMem_tb (
      .o8_Dout          (Dout),
      .o_Kout           (Kout),
      .o_DErr           (DErr),
      .o_KErr           (KErr),
      .o_DpErr          (DpErr),
      .i_ForceDisparity (1'b0),
      .i_Disparity      (1'b0),
      .i10_Din          (Din),
      .o_Rd             (),
      .i_Clk            (clk_40),
      .i_ARst_L         (~reset_in),
      .soft_reset_i     (),
      .i_enable         (en)
    );


// initial begin
//   reset_in = 1'b1;
//   en = 1'b0;

//   # 30;
//   reset_in = 1'b0;
//   # 30;
//   Din = 10'b0011111010;
//   en = 1'b1;
//   @(posedge clk_40);
//   # 1;
//   @(posedge clk_40);
//   # 1;
//   en = 1'b0;
//   Din = 10'b0011110011;

//   #70;
//   en = 1'b1;



//   # 200;


//   $stop;
// end
always_ff @(posedge clk_40 or negedge reset_in) begin
  if(reset_in) begin
    Din <= 'b0;
  end else begin
    Din <= Din +1'b1;
  end
end


initial begin
  reset_in = 1'b1;
  en = 1'b1;
  # 1;
  reset_in = 1'b0;

  # 25700;


  $stop;
end


endmodule
