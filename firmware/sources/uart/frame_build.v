`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2015/10/14 11:28:17
// Design Name: 
// Module Name: frame_build
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module frame_build(
    input clk,
    input reset_n,
    input [7:0] rx_data,
    input  rx_ready,
    input rx_parity_error,
    output reg frame_ready,
    output reg [159:0]  frame_data
  );

localparam IDLE = 2'b00;
localparam GET_DATA = 2'b01;
localparam BITFILE_WRITE = 2'b10;

reg [1:0] state = 2'b00; 
reg [3:0] data_counter = 4'b0001;
always @(posedge clk) begin
  if (~reset_n) begin
    // reset
    state <= IDLE;
    data_counter <=  4'b0001;
    frame_data  <= 160'b0;
    frame_ready <= 1'b0;
  end
  else begin
    case(state)
      IDLE: begin
        frame_ready <= 1'b0;
        if (rx_ready&(~rx_parity_error)) begin
          if(rx_data[7:4]==4'b1111) begin
            state <= BITFILE_WRITE;
            data_counter <=  4'b0001;
            frame_data  <= {156'b0,rx_data[3:0]};
          end else if (rx_data[7:4]==4'b1110) begin
            state <= IDLE;
            data_counter <=  4'b0001;
            frame_data   <= {156'b0,rx_data[3:0]};
            frame_ready  <= 1'b1;  //JTAG_COMMAND has fixed 4 bit header + 4 bit command
          end else if(rx_data==8'b0000_0000)begin
            state <= GET_DATA;
            data_counter <=  4'b0001;
            frame_data  <= 160'b0;
          end
        end
      end
      GET_DATA: begin
        frame_ready <= 1'b0;
        if (rx_ready&(~rx_parity_error)) begin
          if(rx_data==8'b0000_0000)begin
            state <= GET_DATA;
            data_counter <=  4'b0001;
            frame_data  <= 160'b0;           
          end else if(rx_data==8'b0000_1111)begin
            frame_ready <= 1'b1;
            state <= IDLE;
          end else if(rx_data[7:4]==data_counter)begin
            state <= GET_DATA;
            frame_data <= {frame_data[155:0],rx_data[3:0]};
            data_counter <= (data_counter==4'b1101) ? 4'b0001:data_counter+4'b0001;
          end else begin
            state <= IDLE;
            frame_data  <= 160'b0;
          end
        end
      end
      BITFILE_WRITE: begin
        frame_ready <= 1'b0;
        if (rx_ready&(~rx_parity_error)) begin
          data_counter     <=  data_counter + 4'b1;
          frame_data       <= {frame_data[151:0],rx_data[7:0]};
          if(data_counter==4) begin   //BIT_WRITE has fixed 1byte header + 4byte bitfile content
            frame_ready  <= 1'b1;
            state <= IDLE;
          end
        end
      end
      default:begin end
    endcase
  end
end

// module frame_build(
//     input clk,
//     input reset_n,
//     input [7:0] rx_data,
//     input  rx_ready,
//     input rx_parity_error,
//     output reg frame_ready,
//     output reg [159:0]  frame_data
//   );

// localparam COMMAND_LEN          = 3;
// localparam IDLE                 = 1;
// localparam GET_DATA             = 2;
// localparam BITFILE_WRITE        = 4;
// // localparam JTAG_COMMAND      = 16;
// // localparam FPGA_CONFIG       = 32;
// // localparam READBACK          = 64;
// // localparam BITFILE_WRITE     = 128;
// // localparam JTAG_COMMAND      = 256;



// localparam IDLE_BIT             =0;
// localparam GET_DATA_BIT         =1;
// localparam BITFILE_WRITE_BIT    =2;
// // localparam JTAG_COMMAND_BIT  =4;
// // localparam FPGA_CONFIG_BIT   =5;
// // localparam READBACK_BIT      =6;
// // localparam BITFILE_WRITE_BIT =7;
// // localparam JTAG_COMMAND_BIT  =8;


// reg     [COMMAND_LEN-1:0]   state;
// reg     [COMMAND_LEN-1:0]   nextstate;

// reg [3:0] data_counter;

// always @(posedge clk  ) begin
//     if (~reset_n) begin
//         // reset
//         state <= IDLE;
//     end else if (rx_ready)begin
//         state <= nextstate;
//     end
// end

// always @(*) begin
//     case(state)
//         IDLE: 
//             nextstate = (~rx_ready)?IDLE:( //only update when rx_ready=1
//                         ((rx_data==8'b0000_0000)&(~rx_parity_error)) ? GET_DATA : 
//                         ((rx_data[7:4]==4'b1111)&(~rx_parity_error)) ? BITFILE_WRITE:
//                         ((rx_data[7:4]==4'b1110)&(~rx_parity_error)) ? IDLE:
//                         IDLE);
//         GET_DATA:
//             nextstate = (~rx_ready)?GET_DATA:( //only update when rx_ready=1
//                         ((rx_data==8'b0000_0000)&(~rx_parity_error)) ? GET_DATA :
//                         ((rx_data==8'b0000_1111)&(~rx_parity_error)) ? IDLE :
//                         ((rx_data[7:4]==data_counter)&(~rx_parity_error)) ? GET_DATA :
//                         IDLE);
//         BITFILE_WRITE:
//             nextstate = (~rx_ready)?BITFILE_WRITE:( //only update when rx_ready=1
//                         (data_counter<4) ? BITFILE_WRITE : IDLE);
//         default: nextstate = IDLE;
//     endcase
// end

// always @(posedge clk) begin
//     if (~reset_n) begin
//         // reset
//         data_counter <= 4'b0001;
//         frame_data   <= 160'b0;
//         frame_ready  <= 1'b0;
//     end else if (rx_ready&(~rx_parity_error)) begin
//         if (state[IDLE_BIT]) begin
//             data_counter <= 4'b0001;
//             frame_data   <= 160'b0;
//             frame_ready  <= 1'b0;
//             if(nextstate[BITFILE_WRITE_BIT]) begin
//                 frame_data   <= {156'b0,rx_data[3:0]};
//                 frame_ready  <= 1'b0;
//             end else if(rx_ready&(rx_data[7:4]==4'b1110)&(~rx_parity_error)) begin
//                 frame_data   <= {156'b0,rx_data[3:0]};
//                 frame_ready  <= 1'b1;  //JTAG_COMMAND has fixed 4 bit header + 4 bit command
//             end
//         end else if (state[GET_DATA_BIT]) begin
//             if (rx_data==8'b0000_0000) begin  //reset while state=GET_DATA
//                 data_counter <=  4'b0001;
//                 frame_data   <= 160'b0;
//                 frame_ready  <= 1'b0;
//             end else if (rx_data[7:4]==data_counter) begin
//                 frame_data   <= {frame_data[155:0],rx_data[3:0]};
//                 data_counter <= (data_counter==4'b1101) ? 4'b0001:data_counter+4'b0001; //data_counter = 15 is for bit write
//             end else if (rx_data==8'b0000_1111) begin
//                 frame_ready  <= 1'b1;
//             end else begin  //other invalid uart byte
//                 data_counter <=  4'b0001;
//                 frame_data   <= 160'b0;
//                 frame_ready  <= 1'b0;
//             end
//         end else if (state[BITFILE_WRITE_BIT]) begin
//             data_counter     <=  data_counter + 4'b1;
//             frame_data       <= {frame_data[151:0],rx_data[7:0]};
//             frame_ready      <= 1'b0;
//             if(data_counter==4) begin   //BIT_WRITE has fixed 1byte header + 4byte bitfile content
//                 frame_ready  <= 1'b1;
//             end
//         end else begin
//             data_counter <=  4'b0001;
//             frame_data  <= 160'b0;
//             frame_ready <= 1'b0;
//         end
//     end else begin  //frame_ready must only last 1 clock
//         frame_ready <= 1'b0;
//     end
// end

// ila_uart_frame ila_uart_frame_inst (
//     .clk(clk), // input wire clk


//     .probe0(state), // input wire [2:0]  probe0  
//     .probe1(nextstate), // input wire [2:0]  probe1 
//     .probe2(data_counter), // input wire [3:0]  probe2 
//     .probe3(frame_data), // input wire [159:0]  probe3 
//     .probe4(frame_ready), // input wire [0:0]  probe4
//     .probe5(reset_n), // input wire [0:0]  probe5 
//     .probe6(rx_data), // input wire [7:0]  probe6 
//     .probe7(rx_ready), // input wire [0:0]  probe7 
//     .probe8(rx_parity_error) // input wire [0:0]  probe8
// );



endmodule
