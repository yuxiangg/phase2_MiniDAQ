/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : tdc_decoder_top.sv
// Create : 2023-11-05 22:35:53
// Revise : 2025-01-09 18:34:54
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------



module tdc_decoder_top
    #(parameter UPLINKCOUNT = 1,
      parameter TDC_COUNT_SINGLE = 10)
    (
    input           clk_40,
    input           clk_asyn,
    input           rst_in,
    input   [229:0] userDataUpLink[UPLINKCOUNT-1:0],
    // input [27:0] elink_config_i[1:0],
    input           trigger_in,
    // output  [27:0]  error_detected_o,
    output  [1:0]   encode_ttc,
    // output [23:0] hit,
    // output mezz_TRST,
    output [UPLINKCOUNT*TDC_COUNT_SINGLE-1:0] locked_dline1,
    output [UPLINKCOUNT*TDC_COUNT_SINGLE-1:0] locked_dline0,

    input           bc_rst,
    input           event_rst,
    input           master_rst,

    input           en_int_trigger,
    input           enable_hit,
    input           hit_inv,
    input           hit_single,
    input   [11:0]  hit_width,
    input   [23:0]  hit_interval,

    input   [19:0]  correct_counter_th,
    input           enable_K28_1,
    input           enable_320M,
    input   [7:0]   trigger_deadtime,
    input   [11:0]  match_window,
    input   [11:0]  search_margin,
    input   [11:0]  bc_offset,
    input   [11:0]  reject_offset,
    input   [11:0]  rollover,
    input   [11:0]  coarse_count_offset,
    input           en_relative_trig_data,
    input           enable_matching,
    input           trigger_redge,
    input   [7:0]   trigger_latency,


  
    // input sys_clk_160,

    //  ethernet interface
    input  tx_axis_clk,
    output [7:0] tx_axis_fifo_tdata,
    output tx_axis_fifo_tvalid,
    input  tx_axis_fifo_tready,
    output tx_axis_fifo_tlast
);



localparam TDC_COUNT = TDC_COUNT_SINGLE*UPLINKCOUNT;
localparam TDC_DATA_WIDTH = 40;

reg [11:0] trigger_coarse;

//clk_40 aligned
// wire enable_K28_1;
// wire enable_320M;

(* dont_touch = "true" *) wire [TDC_COUNT-1:0] locked_dline1;
(* dont_touch = "true" *) wire [TDC_COUNT-1:0] locked_dline0;



assign locked_dline = locked_dline1&locked_dline0;

wire [TDC_COUNT-1:0] matching_busy;


wire [TDC_COUNT-1:0] tdc_fifo_read;
wire [TDC_COUNT-1:0] tdc_fifo_empty;
wire [TDC_COUNT*TDC_DATA_WIDTH-1:0] tdc_fifo_data;

wire tdc_fifo_empty_mux;
wire matching_busy_mux;
wire tdc_fifo_read_mux;
wire [TDC_DATA_WIDTH-1:0] tdc_fifo_data_mux;

wire [29:0] trigger_fifo_data;
wire trigger_fifo_empty;
wire building_busy;
wire trigger_fifo_read;

// wire [11:0] match_window;
// wire [11:0] search_margin;
// wire [11:0] bc_offset;
// wire [11:0] reject_offset;
// wire [11:0] rollover;


// wire enable_relative_trigger_data;

wire [TDC_DATA_WIDTH-1:0] event_data;
wire [TDC_DATA_WIDTH-1:0] readout_fifo_data;
wire readout_fifo_empty;
wire readout_fifo_full;
wire readout_fifo_read;
//vio for ethernet enable
wire packet_rst;
wire packet_enable;

//vio for trigger matching
// wire enable_matching;
// wire bc_reset;
// wire trigger_redge;
// wire [11:0] coarse_count_offset;
// wire [7:0] trigger_latency; 




// wire global_rst;
wire fpga_bcr;
wire rst_40;


// wire ttc_bc_reset_vio;
// wire ttc_event_reset_vio;
// wire ttc_master_reset_vio;

//vio for hit generator
// wire enable_hit;
// wire [11:0] width_hit;  //hit width = width_hit*6.25ns
// wire hit_single;
// wire hit_inv;
// wire [23:0] hit_mask;
// wire [23:0] delay_mask;
// wire [11:0] hit_delay;
// wire [23:0] hit_interval;
// wire [11:0] hit_trigger_ratio;
wire trigger_internal;
// wire enable_internal_trigger;


wire fpga_bcr_40;
// reg srst_160;
// wire rst_40_vio;
wire trigger_detect;
wire trigger_delayed;
// wire [7:0] trigger_deadtime;




wire trigger_fifo_read_triggermode;
wire trigger_fifo_read_triggerless;
wire [TDC_COUNT-1:0] tdc_fifo_read_triggermode;
wire [TDC_COUNT-1:0] tdc_fifo_read_triggerless;
wire [TDC_DATA_WIDTH-1:0] event_data_triggermode;
wire [TDC_DATA_WIDTH-1:0] event_data_triggerless;
wire event_data_ready_triggermode;
wire event_data_ready_triggerless;

assign trigger_detect = en_int_trigger?trigger_internal:trigger_in;
assign packet_enable = 1'b1;


assign trigger_fifo_read = enable_matching?trigger_fifo_read_triggerless:trigger_fifo_read_triggermode;
assign tdc_fifo_read = enable_matching?tdc_fifo_read_triggerless:tdc_fifo_read_triggermode;
assign event_data = enable_matching?event_data_triggerless:event_data_triggermode;
assign event_data_ready = enable_matching?event_data_ready_triggerless:event_data_ready_triggermode;




(* dont_touch = "true" *) wire [43:0] total_received_40bit_dline1[UPLINKCOUNT-1:0][9:0];
(* dont_touch = "true" *) wire [43:0] total_received_40bit_dline0[UPLINKCOUNT-1:0][9:0];
(* dont_touch = "true" *) wire [35:0] total_error_bit_dline1[UPLINKCOUNT-1:0][9:0];
(* dont_touch = "true" *) wire [35:0] total_error_bit_dline0[UPLINKCOUNT-1:0][9:0];
(* dont_touch = "true" *) wire [19:0] phase_sel_160M_20b[UPLINKCOUNT-1:0];



genvar n;
for (n = 0; n < UPLINKCOUNT; n++) begin: uplink
    vio_BER vio_BER_inst (
      .clk(clk_40),                // input wire clk
      .probe_in0 (total_received_40bit_dline1[n][0]),    // input wire [43 : 0] probe_in0
      .probe_in1 (total_received_40bit_dline0[n][0]),    // input wire [43 : 0] probe_in1
      .probe_in2 (total_error_bit_dline1[n][0]),      // input wire [35 : 0] probe_in2
      .probe_in3 (total_error_bit_dline0[n][0]),      // input wire [35 : 0] probe_in3
      .probe_in4 (total_received_40bit_dline1[n][1]),     // input wire [43 : 0] probe_in4
      .probe_in5 (total_received_40bit_dline0[n][1]),     // input wire [43 : 0] probe_in5
      .probe_in6 (total_error_bit_dline1[n][1]),          // input wire [35 : 0] probe_in6
      .probe_in7 (total_error_bit_dline0[n][1]),          // input wire [35 : 0] probe_in7
      .probe_in8 (total_received_40bit_dline1[n][2]),    // input wire [43 : 0] probe_in8
      .probe_in9 (total_received_40bit_dline0[n][2]),    // input wire [43 : 0] probe_in9
      .probe_in10(total_error_bit_dline1[n][2]),        // input wire [35 : 0] probe_in10
      .probe_in11(total_error_bit_dline0[n][2]),        // input wire [35 : 0] probe_in11
      .probe_in12(total_received_40bit_dline1[n][3]),   // input wire [43 : 0] probe_in12
      .probe_in13(total_received_40bit_dline0[n][3]),   // input wire [43 : 0] probe_in13
      .probe_in14(total_error_bit_dline1[n][3]),        // input wire [35 : 0] probe_in14
      .probe_in15(total_error_bit_dline0[n][3]),        // input wire [35 : 0] probe_in15
      .probe_in16(total_received_40bit_dline1[n][4]),   // input wire [43 : 0] probe_in16
      .probe_in17(total_received_40bit_dline0[n][4]),   // input wire [43 : 0] probe_in17
      .probe_in18(total_error_bit_dline1[n][4]),        // input wire [35 : 0] probe_in18
      .probe_in19(total_error_bit_dline0[n][4]),        // input wire [35 : 0] probe_in19
      .probe_in20(total_received_40bit_dline1[n][5]),   // input wire [43 : 0] probe_in20
      .probe_in21(total_received_40bit_dline0[n][5]),   // input wire [43 : 0] probe_in21
      .probe_in22(total_error_bit_dline1[n][5]),        // input wire [35 : 0] probe_in22
      .probe_in23(total_error_bit_dline0[n][5]),        // input wire [35 : 0] probe_in23
      .probe_in24(total_received_40bit_dline1[n][6]),   // input wire [43 : 0] probe_in24
      .probe_in25(total_received_40bit_dline0[n][6]),   // input wire [43 : 0] probe_in25
      .probe_in26(total_error_bit_dline1[n][6]),        // input wire [35 : 0] probe_in26
      .probe_in27(total_error_bit_dline0[n][6]),        // input wire [35 : 0] probe_in27
      .probe_in28(total_received_40bit_dline1[n][7]),   // input wire [43 : 0] probe_in28
      .probe_in29(total_received_40bit_dline0[n][7]),   // input wire [43 : 0] probe_in29
      .probe_in30(total_error_bit_dline1[n][7]),        // input wire [35 : 0] probe_in30
      .probe_in31(total_error_bit_dline0[n][7]),        // input wire [35 : 0] probe_in31
      .probe_in32(total_received_40bit_dline1[n][8]),   // input wire [43 : 0] probe_in32
      .probe_in33(total_received_40bit_dline0[n][8]),   // input wire [43 : 0] probe_in33
      .probe_in34(total_error_bit_dline1[n][8]),        // input wire [35 : 0] probe_in34
      .probe_in35(total_error_bit_dline0[n][8]),        // input wire [35 : 0] probe_in35
      .probe_in36(total_received_40bit_dline1[n][9]),   // input wire [43 : 0] probe_in36
      .probe_in37(total_received_40bit_dline0[n][9]),   // input wire [43 : 0] probe_in37
      .probe_in38(total_error_bit_dline1[n][9]),        // input wire [35 : 0] probe_in38
      .probe_in39(total_error_bit_dline0[n][9]),        // input wire [35 : 0] probe_in39
      .probe_out0(phase_sel_160M_20b[n])  // output wire [19 : 0] probe_out0
    );
end




xlx_ku_mgt_ip_reset_synchronizer fpga_bcr_rst_sync(
 .clk_in(clk_40),
 .rst_in(fpga_bcr),
 .rst_out(fpga_bcr_40)
);

xlx_ku_mgt_ip_reset_synchronizer r40_rst_sync(
 .clk_in(clk_40),
 .rst_in(rst_in),
 .rst_out(rst_40)
);
xlx_ku_mgt_ip_reset_synchronizer pck_rst_sync(
 .clk_in(tx_axis_clk),
 .rst_in(rst_40),
 .rst_out(packet_rst)
);


always @(posedge clk_40 ) begin
    if (rst_40|fpga_bcr_40) begin
        // reset
        trigger_coarse <= coarse_count_offset;
    end else begin
        trigger_coarse <= trigger_coarse == rollover ? 'b0 : trigger_coarse + 1'b1;
    end 
end


trigger_interface inst_trigger_interface
(
    .clk_40              (clk_40),
    .rst_40              (rst_40),
    .bc_reset            (fpga_bcr_40),
    .event_reset         (event_rst),
    .trigger_in          (trigger_detect),
    .trigger_ready_out   (trigger_delayed),
    .trigger_read        (trigger_fifo_read),
    .trigger_fifo_data   (trigger_fifo_data),
    .trigger_fifo_empty  (trigger_fifo_empty),
    // .trigger_coarse      (trigger_coarse),
    .trigger_redge       (trigger_redge),
    .rollover            (rollover),
    .coarse_count_offset (coarse_count_offset),
    .trigger_latency     (trigger_latency),
    .trigger_deadtime    (trigger_deadtime)
);

// assign trigger_fifo_data = 'b0;
// assign trigger_fifo_empty = 'b1;
// assign trigger_coarse = 'b0;



genvar i;
for (i = 0; i < UPLINKCOUNT; i++) begin:tdc_inst

tdcv2_decoder_wrapper #(
        .TDC_COUNT(TDC_COUNT_SINGLE),
        .IS_MASTER((i+1)%2),
        .CSM_ID(i/2),
        .TDC_DATA_WIDTH(TDC_DATA_WIDTH)
) tdcv2_decoder_wrapper (
    .clk_40                       (clk_40),
    .rst_40                       (rst_40),
    .userDataUpLink_i             (userDataUpLink[i]),
    .correct_counter_th           (correct_counter_th),
    .enable_K28_1                 (enable_K28_1),
    .enable_320M                  (enable_320M),
    .phase_sel_160M_20b           (phase_sel_160M_20b[i]),
    .locked_dline1                (locked_dline1[TDC_COUNT_SINGLE*(i+1)-1:TDC_COUNT_SINGLE*i]),
    .locked_dline0                (locked_dline0[TDC_COUNT_SINGLE*(i+1)-1:TDC_COUNT_SINGLE*i]),

    // .sys_clk_160                  (sys_clk_160),
    // .rst_160                      (rst_160),
    .tdc_fifo_read                (tdc_fifo_read[TDC_COUNT_SINGLE*(i+1)-1:TDC_COUNT_SINGLE*i]),
    .tdc_fifo_empty               (tdc_fifo_empty[TDC_COUNT_SINGLE*(i+1)-1:TDC_COUNT_SINGLE*i]),
    .tdc_fifo_data                (tdc_fifo_data[TDC_COUNT_SINGLE*TDC_DATA_WIDTH*(i+1)-1:TDC_COUNT_SINGLE*TDC_DATA_WIDTH*i]),
    .matching_busy                (matching_busy[TDC_COUNT_SINGLE*(i+1)-1:TDC_COUNT_SINGLE*i]),
    .enable_matching              (enable_matching),
    .enable_matching_timeout      (1'b1),
    .trigger_fifo_data            (trigger_fifo_data[16:0]),
    .trigger_fifo_empty           (trigger_fifo_empty),
    .building_busy                (building_busy),
    .trigger_coarse               (trigger_coarse),
    .match_window                 (match_window),
    .search_margin                (search_margin),
    .bc_offset                    (bc_offset),
    .reject_offset                (reject_offset),
    .rollover                     (rollover),
    .enable_relative_trigger_data (en_relative_trig_data),

    .total_received_40bit_dline1 (total_received_40bit_dline1[i]),
    .total_received_40bit_dline0 (total_received_40bit_dline0[i]),
    .total_error_bit_dline1      (total_error_bit_dline1[i]),
    .total_error_bit_dline0      (total_error_bit_dline0[i])
);
end


tdc_fifo_data_mux #(
    .TDC_COUNT(TDC_COUNT),
    .TDC_DATA_WIDTH(TDC_DATA_WIDTH)
) inst_tdc_fifo_data_mux (
    .locked             (locked_dline),
    .tdc_fifo_empty     (tdc_fifo_empty),
    .tdc_fifo_data      (tdc_fifo_data),
    .matching_busy      (matching_busy),
    .tdc_fifo_read      (tdc_fifo_read_triggerless),

    .tdc_fifo_empty_mux (tdc_fifo_empty_mux),
    .tdc_fifo_data_mux  (tdc_fifo_data_mux),
    .matching_busy_mux  (matching_busy_mux),
    .tdc_fifo_read_mux  (tdc_fifo_read_mux)
);

// // ila_tdc_fifo_mux ila_tdc_fifo_mux_inst (
// //     .clk(sys_clk_160), // input wire clk


// //     .probe0(tdc_fifo_empty), // input wire [9:0]  probe0  
// //     .probe1(matching_busy), // input wire [9:0]  probe1 
// //     .probe2(tdc_fifo_read), // input wire [9:0]  probe2 
// //     .probe3(tdc_fifo_empty_mux), // input wire [0:0]  probe3 
// //     .probe4(matching_busy_mux), // input wire [0:0]  probe4 
// //     .probe5(tdc_fifo_read_mux), // input wire [0:0]  probe5 
// //     .probe6(hit) // input wire [23:0]  probe5 
// // );

event_builder inst_event_builder_triggerless
(
    .clk                (clk_40),
    .rst                (rst_40),
    .enable             (enable_matching),
    .trigger_fifo_empty (trigger_fifo_empty),
    .trigger_fifo_data  (trigger_fifo_data),
    .trigger_fifo_read  (trigger_fifo_read_triggerless),
    .tdc_fifo_empty     (tdc_fifo_empty_mux),
    .tdc_fifo_data      (tdc_fifo_data_mux),
    .matching_busy      (matching_busy_mux),
    .tdc_fifo_read      (tdc_fifo_read_mux),
    .readout_fifo_full  (readout_fifo_full),
    .event_data         (event_data_triggerless),
    .event_data_ready   (event_data_ready_triggerless),
    .building_busy      (building_busy)
);



event_builder_triggermode #(
        .TDC_COUNT(TDC_COUNT),
        .TDC_DATA_WIDTH(TDC_DATA_WIDTH)
) inst_event_builder_triggermode (
    .clk                  (clk_40),
    .rst                  (rst_40),
    .enable               (~enable_matching),

    .trigger_fifo_empty   (trigger_fifo_empty),
    .trigger_fifo_data    (trigger_fifo_data),
    .trigger_fifo_read    (trigger_fifo_read_triggermode),

    .locked_array         (locked_dline),
    .tdc_fifo_empty_array (tdc_fifo_empty),
    .tdc_fifo_data_array  (tdc_fifo_data),
    .tdc_fifo_read_array  (tdc_fifo_read_triggermode),
    
    .readout_fifo_full    (readout_fifo_full),
    .event_data           (event_data_triggermode),
    .event_data_ready     (event_data_ready_triggermode)
);


readout_fifo readout_fifo_inst (
    .srst(rst_40),                //The synchronous reset (srst) 
                                 //should be synchronous to wr_clk.
    .wr_clk(clk_40),            // input wire wr_clk
    .rd_clk(tx_axis_clk), 
    .din(event_data),                  // input wire [39 : 0] din
    .wr_en(event_data_ready),              // input wire wr_en
    .rd_en(readout_fifo_read),              // input wire rd_en
    .dout(readout_fifo_data),                // output wire [39 : 0] dout
    .full(readout_fifo_full),                // output wire full
    .empty(readout_fifo_empty),              // output wire empty
    .wr_rst_busy(),  // output wire wr_rst_busy
    .rd_rst_busy()  // output wire rd_rst_busy
);


single_ttc_generator inst_single_ttc_generator
(
    .clk_40            (clk_40),
    .rst_40            (rst_40),
    .encode_ttc        (encode_ttc),
    .fpga_bcr          (fpga_bcr),
    .trigger           (trigger_delayed&(~enable_matching)),
    .bc_reset          (bc_rst),
    .event_reset       (event_rst),
    .master_reset      (master_rst)
);


data_packet data_packet_inst (
    .clk(tx_axis_clk),
    .rst(packet_rst),
    .enable(packet_enable),
    .TDC_fifo_data(readout_fifo_data),
    .TDC_data_ready(~readout_fifo_empty),
    .TDC_data_rd(readout_fifo_read),
    
    .data_to_eth(tx_axis_fifo_tdata),
    .packet_valid(tx_axis_fifo_tvalid),
    .tx_axis_fifo_tready(tx_axis_fifo_tready),
    .last(tx_axis_fifo_tlast)
);



trigger_generator inst_trigger_generator
(
    .clk          (clk_40),
    .rst          (rst_40),
    .enable       (en_int_trigger),
    .trigger_out  (trigger_internal),
    .width        (hit_width),
    .start        (enable_hit),
    .inv          (hit_inv),
    .interval     (hit_interval),
    .start_single (hit_single)
);


// hit_generator inst_hit_generator
// (
//     .clk               (clk_40),
//     .rst               (rst_40),
//     .hit               (hit),
//     .trigger_out       (trigger_internal),
//     .hit_delay         (hit_delay),
//     .width             (width_hit),
//     .start             (enable_hit),
//     .inv               (hit_inv),
//     .hit_mask          (hit_mask),
//     .delay_mask        (delay_mask),
//     .interval          (hit_interval),
//     .hit_trigger_ratio (hit_trigger_ratio),
//     .start_single      (hit_single)
// );


// vio_trigger vio_trigger_inst (
//     .clk(clk_asyn),                // input wire clk
//     .probe_out0(rst_40_vio),  // output wire [0 : 0] probe_out0
//     .probe_out1(trigger_deadtime),  // output wire [7 : 0] probe_out1
//     .probe_out2(match_window),  // output wire [11 : 0] probe_out2
//     .probe_out3(search_margin),  // output wire [11 : 0] probe_out3
//     .probe_out4(bc_offset),  // output wire [11 : 0] probe_out4
//     .probe_out5(reject_offset),  // output wire [11 : 0] probe_out5
//     .probe_out6(rollover),  // output wire [11 : 0] probe_out6
//     .probe_out7(coarse_count_offset), // output wire [11 : 0] probe_out7
//     .probe_out8(enable_relative_trigger_data),  // output wire [0 : 0] probe_out8
//     .probe_out9(enable_matching),  // output wire [0 : 0] probe_out9
//     .probe_out10(trigger_redge),  // output wire [0 : 0]
//     .probe_out11(trigger_latency) // output wire [7 : 0]
// );

// vio_hit vio_hit_inst (
//     .clk(clk_asyn),                // input wire clk
//     .probe_out0(enable_hit),  // output wire [0 : 0] probe_out0
//     .probe_out1(enable_internal_trigger),  // output wire [0 : 0] probe_out1
//     .probe_out2(hit_inv),  // output wire [0 : 0] probe_out2
//     .probe_out3(width_hit),  // output wire [11 : 0] probe_out3
//     .probe_out4(hit_interval),  // output wire [11 : 0] probe_out4
//     .probe_out5(hit_mask),  // output wire [23 : 0] probe_out5
//     .probe_out6(hit_single),  // output wire [0 : 0] probe_out6
//     .probe_out7(hit_delay),  // output wire [11 : 0] probe_out7
//     .probe_out8(delay_mask), // output wire [23 : 0] probe_out8
//     .probe_out9(hit_trigger_ratio) // output wire [11 : 0] probe_out9
// );


// vio_hit vio_hit_inst (
//   .clk(clk_asyn),                // input wire clk
//   .probe_in0(enable_hit),    // input wire [0 : 0] probe_in0
//   .probe_in1(en_int_trigger),    // input wire [0 : 0] probe_in1
//   .probe_in2(hit_inv),    // input wire [0 : 0] probe_in2
//   .probe_in3(hit_width),    // input wire [11 : 0] probe_in3
//   .probe_in4(hit_interval),    // input wire [23 : 0] probe_in4
//   .probe_in5(bc_rst),    // input wire [0 : 0] probe_in5
//   .probe_in6(hit_single),    // input wire [0 : 0] probe_in6
//   .probe_in7(event_rst),    // input wire [0 : 0] probe_in7
//   .probe_in8(master_rst),    // input wire [0 : 0] probe_in8
//   .probe_in9(correct_counter_th),    // input wire [19 : 0] probe_in9
//   .probe_in10(enable_K28_1),  // input wire [0 : 0] probe_in10
//   .probe_in11(enable_320M),  // input wire [0 : 0] probe_in11
//   .probe_in12(trigger_deadtime),  // input wire [7 : 0] probe_in12
//   .probe_in13(match_window),  // input wire [11 : 0] probe_in13
//   .probe_in14(search_margin),  // input wire [11 : 0] probe_in14
//   .probe_in15(bc_offset),  // input wire [11 : 0] probe_in15
//   .probe_in16(reject_offset),  // input wire [11 : 0] probe_in16
//   .probe_in17(rollover),  // input wire [11 : 0] probe_in17
//   .probe_in18(coarse_count_offset),  // input wire [11 : 0] probe_in18
//   .probe_in19(en_relative_trig_data),  // input wire [0 : 0] probe_in19
//   .probe_in20(enable_matching),  // input wire [0 : 0] probe_in20
//   .probe_in21(trigger_redge),  // input wire [0 : 0] probe_in21
//   .probe_in22(trigger_latency)  // input wire [7 : 0] probe_in22
// );



vio_40M vio_40M_inst (
    .clk(clk_asyn),                // input wire clk
    .probe_in0({{(40-TDC_COUNT){1'b0}},locked_dline1}),    // input wire [9 : 0] probe_in0
    .probe_in1({{(40-TDC_COUNT){1'b0}},locked_dline0})    // input wire [9 : 0] probe_in1

    // .probe_out0(global_rst),  // output wire [0 : 0] probe_out0
    // .probe_out1(correct_counter_th),  // output wire [19 : 0] probe_out1
    // .probe_out2(enable_K28_1),  // output wire [0 : 0] probe_out2
    // .probe_out3(ttc_bc_reset_vio),  // output wire [0 : 0] probe_out3
    // .probe_out4(ttc_event_reset_vio),  // output wire [0 : 0] probe_out4
    // .probe_out5(ttc_master_reset_vio),  // output wire [0 : 0] probe_out5
    // .probe_out6(enable_320M),  // output wire [0 : 0] probe_out6
    // .probe_out7(mezz_TRST) // output wire [0 : 0] probe_out7
);



endmodule
