/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : command_resolve_multi.sv
// Create : 2024-07-24 02:57:56
// Revise : 2025-02-21 17:28:23
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------

//[3:0] uplink_to_uart_mapping:
// 0000 uplink0_master/downlink0
// 0001 uplink0_slave /downlink0
// 0010 uplink1_master/downlink1
// 0011 uplink1_slave /downlink1
// 0100 sca



// lpGBT multi-byte write
// [7:0] GBTx_address,[15:0] Register_addr,[7:0] value,[7:0] nb_to_be_read(value_length)

// SCA write command format
// command[74:72]: 
// reset_gbtsc, start_reset, start_connect
// command[71:0]:
// which_CSM,[6:0] tx_address,[7:0] tx_transID,[7:0] tx_channel,[7:0] tx_len,[7:0] tx_command,[31:0] tx_data
// 
//
  
module command_resolve_multi
    #(parameter DOWNLINKCOUNT = 1,
      parameter SCA_PER_DOWNLINK = 4)
    (
    input           clk,
    input           rst,

    input   [159:0] command_i,  
    input           command_fifo_empty_i,
    output          command_fifo_read_o,

    input   [3:0]   uplink_to_uart_mapping_i,
    
    output  [159:0] command_r_o,
    output          MiniDAQ_reg_write_o,
    output          MiniDAQ_reg_read_o,
    output          MiniDAQ_reg_rst_o,

//  For lpGBT config
    output  [7:0]                   ic_GBTx_address_o [DOWNLINKCOUNT-1:0],
    output  [15:0]                  ic_Register_addr_o[DOWNLINKCOUNT-1:0],
    output  [7:0]                   ic_nb_to_be_read_o[DOWNLINKCOUNT-1:0],
    
    output  [DOWNLINKCOUNT-1:0]     ic_wr_start_o  , //ic FIFO flush out to lpGBT
    output  [DOWNLINKCOUNT-1:0]     ic_rd_start_o  , //lpGBT read back to ic FIFO

    //write to internal ic FIFO
    output  [7:0]                   ic_wfifo_data_o[DOWNLINKCOUNT-1:0],
    output  [DOWNLINKCOUNT-1:0]     ic_fifo_wr_o,

    //All enabled SCAs per downlink are configured by the same commands
    
    output  [DOWNLINKCOUNT-1:0]     sca_start_reset_o,
    output  [DOWNLINKCOUNT-1:0]     sca_start_connect_o,
    output  [DOWNLINKCOUNT-1:0]     sca_start_command_o,
    output  [SCA_PER_DOWNLINK-1:0]  sca_enable_array_o[DOWNLINKCOUNT-1:0],
    
    output  [7:0]                   sca_tx_address_o[DOWNLINKCOUNT-1:0],
    output  [7:0]                   sca_tx_transID_o[DOWNLINKCOUNT-1:0],
    output  [7:0]                   sca_tx_channel_o[DOWNLINKCOUNT-1:0],
    output  [7:0]                   sca_tx_len_o    [DOWNLINKCOUNT-1:0],
    output  [7:0]                   sca_tx_command_o[DOWNLINKCOUNT-1:0],
    output  [31:0]                  sca_tx_data_o   [DOWNLINKCOUNT-1:0]
);
    





wire [7:0]   ic_GBTx_address;
wire [15:0]  ic_Register_addr;
// wire [1:0]   ic_which_CSM;
wire [7:0]   ic_nb_to_be_read;
wire         ic_wr_start;
wire         ic_rd_start;
wire [7:0]   ic_wfifo_data;
wire         ic_fifo_wr;

wire         sca_start_reset;
wire         sca_start_connect;
wire         sca_start_command;
wire [3:0]   sca_enable_array;
wire         sca_which_CSM;
wire [7:0]   sca_tx_address;
wire [7:0]   sca_tx_transID;
wire [7:0]   sca_tx_channel;
wire [7:0]   sca_tx_len;
wire [7:0]   sca_tx_command;
wire [31:0]  sca_tx_data;

genvar i;
generate
    for (i=0;i<DOWNLINKCOUNT;i=i+1) begin 
        assign ic_GBTx_address_o  [i] = (i==uplink_to_uart_mapping_i[1])?ic_GBTx_address :'b0;
        assign ic_Register_addr_o [i] = (i==uplink_to_uart_mapping_i[1])?ic_Register_addr:'b0;
        assign ic_nb_to_be_read_o [i] = (i==uplink_to_uart_mapping_i[1])?ic_nb_to_be_read:'b0;
        assign ic_fifo_wr_o       [i] = (i==uplink_to_uart_mapping_i[1])?ic_fifo_wr      :'b0;
        assign ic_wfifo_data_o    [i] = (i==uplink_to_uart_mapping_i[1])?ic_wfifo_data   :'b0;
        assign ic_wr_start_o      [i] = (i==uplink_to_uart_mapping_i[1])?ic_wr_start     :'b0;
        assign ic_rd_start_o      [i] = (i==uplink_to_uart_mapping_i[1])?ic_rd_start     :'b0;

        assign sca_enable_array_o [i] = (i==sca_which_CSM)?sca_enable_array :'b0;
        assign sca_start_reset_o  [i] = (i==sca_which_CSM)?sca_start_reset  :'b0;
        assign sca_start_connect_o[i] = (i==sca_which_CSM)?sca_start_connect:'b0;
        assign sca_start_command_o[i] = (i==sca_which_CSM)?sca_start_command:'b0;
        assign sca_tx_address_o   [i] = (i==sca_which_CSM)?sca_tx_address   :'b0;
        assign sca_tx_transID_o   [i] = (i==sca_which_CSM)?sca_tx_transID   :'b0;
        assign sca_tx_channel_o   [i] = (i==sca_which_CSM)?sca_tx_channel   :'b0;
        assign sca_tx_len_o       [i] = (i==sca_which_CSM)?sca_tx_len       :'b0;
        assign sca_tx_command_o   [i] = (i==sca_which_CSM)?sca_tx_command   :'b0;
        assign sca_tx_data_o      [i] = (i==sca_which_CSM)?sca_tx_data      :'b0;
    end
endgenerate

command_resolve inst_command_resolve
(
    .clk                      (clk),
    .rst                      (rst),
    .command_i                (command_i),
    .command_fifo_empty_i     (command_fifo_empty_i),
    .command_fifo_read_o      (command_fifo_read_o),

    .command_r                (command_r_o),
    .MiniDAQ_reg_write_o      (MiniDAQ_reg_write_o),
    .MiniDAQ_reg_read_o       (MiniDAQ_reg_read_o),
    .MiniDAQ_reg_rst_o        (MiniDAQ_reg_rst_o),

    .ic_GBTx_address_o        (ic_GBTx_address),
    .ic_Register_addr_o       (ic_Register_addr),
    // .ic_which_CSM_o       (ic_which_CSM),
    .ic_nb_to_be_read_o       (ic_nb_to_be_read),
    .ic_wr_start_o            (ic_wr_start),
    .ic_rd_start_o            (ic_rd_start),
    .ic_wfifo_data_o          (ic_wfifo_data),
    .ic_fifo_wr_o             (ic_fifo_wr),

    .sca_start_reset_o        (sca_start_reset),
    .sca_start_connect_o      (sca_start_connect),
    .sca_start_command_o      (sca_start_command),
    .sca_enable_array_o       (sca_enable_array),
    .sca_which_CSM_o          (sca_which_CSM),
    .sca_tx_address_o         (sca_tx_address),
    .sca_tx_transID_o         (sca_tx_transID),
    .sca_tx_channel_o         (sca_tx_channel),
    .sca_tx_len_o             (sca_tx_len),
    .sca_tx_command_o         (sca_tx_command),
    .sca_tx_data_o            (sca_tx_data)
);


endmodule
