/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : trigger_interface.v
// Create : 2021-03-10 01:47:27
// Revise : 2024-09-20 02:14:57
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------
module trigger_interface
(
    input clk_40,
    input rst_40,
    input bc_reset,
    input event_reset,
    input trigger_in,    
    input trigger_read,

    output trigger_ready_out,
    
    output [29:0] trigger_fifo_data,
    output trigger_fifo_empty,
    //configuration
    input trigger_redge,
    input [11:0] rollover,
    input [11:0] coarse_count_offset,
    input [7:0] trigger_latency,
    input [7:0] trigger_deadtime

);

wire trigger_ready_delayed;
wire trigger_write;
wire [29:0] trigger_data_in;
wire [16:0] trigger_time;
wire [16:0] trigger_time_delayed;
wire trigger_fifo_full_tmp;
wire trigger_fifo_full;
wire trigger_fifo_empty_tmp;
reg [11:0] event_count;
reg [13:0] trigger_coarse_reg;
wire trigger_ready;
reg [1:0] tick_reg;
wire delayed_write;
wire [28:0] delayed_trigger_data;
reg trigger_fifo_overflow;
wire t0_clk;
wire t0_clk_rst;

reg [7:0] trigger_ready_block;  // driven by 160 MHz clock
wire trigger_allow;
wire gated_trigger_ready_delayed;
assign trigger_data_in = {trigger_fifo_overflow, event_count, trigger_time_delayed};

assign trigger_allow = ~|trigger_ready_block;
assign gated_trigger_ready_delayed = trigger_ready_delayed & trigger_allow;

xlx_ku_mgt_ip_reset_synchronizer t0_clk_rst_sync(
 .clk_in(t0_clk),
 .rst_in(rst_40|event_reset),
 .rst_out(t0_clk_rst)
);

always @(posedge t0_clk) begin
    if(t0_clk_rst) begin
        event_count <= 'b0;
        trigger_fifo_overflow <= 1'b0;
    end else if(gated_trigger_ready_delayed) begin
        event_count <= event_count + 1'b1;  //
        if(trigger_fifo_full) begin
            trigger_fifo_overflow <= 1'b1;
        end else begin
            trigger_fifo_overflow <= 1'b0;
        end
    end
end


reg [1:0] tick_count;
always @(posedge t0_clk) begin
    tick_count <= tick_count + 1'b1;
end


always @(posedge t0_clk) begin
    if(t0_clk_rst) begin
        trigger_ready_block <= 'b0;
    end else if (gated_trigger_ready_delayed) begin
        trigger_ready_block <= trigger_deadtime;
    end else if (~trigger_allow) begin
        if (&tick_count) begin
            trigger_ready_block <= trigger_ready_block - 1'b1;
        end
    end
end

t0_timestamp t0_timestamp_inst(
    .clk_40              (clk_40),
    .reset_in            (rst_40|bc_reset),

    .trigger_ext         (trigger_in),
    .t0_timestamp        (trigger_time),
    .t0_ready            (trigger_ready),
    .clk0                (t0_clk),  //160 MHz clock
    
    .is_rising_edge      (trigger_redge),    
    .coarse_count_offset (coarse_count_offset),
    .rollover            (rollover)  
);



shift_ram_trigger shift_ram_trigger_inst (
  .A({trigger_latency,2'b00}),        // input wire [9 : 0] A
  .D({trigger_time,trigger_ready}),        // input wire [17 : 0] D
  .CLK(t0_clk),    // input wire CLK
  .SCLR(t0_clk_rst),  // input wire SCLR, only reset output register
  .Q({trigger_time_delayed,trigger_ready_delayed})        // output wire [17 : 0] Q
);

xlx_ku_mgt_ip_reset_synchronizer trigger_ready_rst_sync(
 .clk_in(clk_40),
 .rst_in(gated_trigger_ready_delayed),
 .rst_out(trigger_ready_out)
);


// trigger_FIFO trigger_FIFO_delayed_inst (
//   .clk(clk_40),                  // input wire clk
//   .srst(rst_40),                // input wire srst
//   .din(trigger_data_in),                  // input wire [28 : 0] din
//   .wr_en(trigger_ready_delayed),              // input wire wr_en
//   .rd_en(trigger_read),              // input wire rd_en
//   .dout(trigger_fifo_data),                // output wire [28 : 0] dout
//   .full(trigger_fifo_full),                // output wire full
//   .empty(trigger_fifo_empty),              // output wire empty
//   .wr_rst_busy(),  // output wire wr_rst_busy
//   .rd_rst_busy()  // output wire rd_rst_busy
// );



//sync FPGA tdc data from 160 MHz to 40 MHz
trigger_FIFO trigger_FIFO_delayed_inst (
  .srst        (rst_40),                // input wire srst
  .wr_clk      (t0_clk),            // input wire wr_clk
  .rd_clk      (clk_40),            // input wire rd_clk
  .din         (trigger_data_in),                  // input wire [29 : 0] din
  .wr_en       (gated_trigger_ready_delayed),              // input wire wr_en
  .rd_en       (trigger_read),              // input wire rd_en
  .dout        (trigger_fifo_data),                // output wire [29 : 0] dout
  .full        (trigger_fifo_full),                // output wire full
  .empty       (trigger_fifo_empty),              // output wire empty
  .wr_rst_busy (),  // output wire wr_rst_busy
  .rd_rst_busy ()  // output wire rd_rst_busy
);


ila_trigger_interface ila_trigger_interface_inst (
    .clk(clk_40), // input wire clk


    .probe0(rst_40), // input wire [0:0]  probe0  
    .probe1(trigger_fifo_full), // input wire [0:0]  probe1 
    .probe2(trigger_read), // input wire [0:0]  probe2 
    .probe3(trigger_fifo_data), // input wire [29:0]  probe3 
    .probe4(trigger_fifo_empty), // input wire [0:0]  probe4 
    .probe5(trigger_allow),  // input wire [0:0]  probe5 
    .probe6(trigger_ready_block), // input wire [7:0]  probe6 
    .probe7(trigger_in) // input wire [0:0]  probe7 
);



endmodule
