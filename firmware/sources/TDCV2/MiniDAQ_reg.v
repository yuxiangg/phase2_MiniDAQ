/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : MiniDAQ_reg.v
// Create : 2025-01-27 09:55:52
// Revise : 2025-03-03 15:13:08
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------
// fpga config reg map
/*
reg_address     reg                     value_bit   desc
1               uplink_to_uart_mapping  4           0:CSM0_master, 1:CSM0_slave, 2:CSM1_master, 
                                                    3:CSM1_slave 4:SCA 
2               uplink_reset            1
3               downlink_reset          1
4               sc_reset                1           Reset ic tx, ic rx and sca
5               global_rst              1           FPGA_TDC reset/bcr, send TTC bcr and TTC event reset, 
reset FPGA trigger FIFO, reset FPGA event count, reset FPGA trigger_coarse (for trigger matching), reset event builder, reset FPGA readout FIFO, reset decoder dline lock, reset decoder channel FIFO and RAM.
6               bc_rst                  1           FPGA_TDC reset/bcr, send TTC bcr
7               event_rst               1           FPGA event reset, send TTC event reset
8               master_rst              1           send TTC master reset
9               TRST                    1           mezz JTAG TRST and rst_in (tied together)
10              en_int_trigger          1           switch between internal and external trigger, 1=internal
11              enable_hit              1           start internal periodical pulse
12              hit_inv                 1           inverse the internal trigger. 1 = negative pulse
13              hit_single              1           send a single pulse as internal trigger
14              hit_width               12          internal pulse width = hit_width*25 ns
15              hit_interval            24          rate = 40M/(1+hit_interval), default to 1KHz
16              correct_counter_th      20          comma code repeating time before data line lock
17              enable_K28_1            1           1=k28.1, 0=k28.5
18              enable_320M             1           1=320 Mbps, 0=160 Mbps
19              trigger_deadtime        8           trigger dead time, LSB=25 ns
20              match_window            12          MiniDAQ trigger match window, LSB=25 ns
21              search_margin           12          MiniDAQ trigger match search margin, LSB=25 ns
22              bc_offset               12          MiniDAQ trigger match bc_offset, LSB=25 ns
23              reject_offset           12          MiniDAQ trigger match reject_offset, LSB=25 ns
24              rollover                12          MiniDAQ trigger match rollover, LSB=25 ns
25              coarse_count_offset     12          MiniDAQ trigger match coarse_count_offset, LSB=25 ns
26              en_relative_trig_data   1           MiniDAQ sends out timing relative to trigger instead of raw when set to 1
27              enable_matching         1           1 = matching in MiniDAQ. 0 = sends all data out.
28              trigger_redge           1           1 = rising edge, 0 = falling edge
29              trigger_latency         8           Trigger matching starts trigger_latency*25 ns after a trigger is detected                           
*/
  
module MiniDAQ_reg
    #(parameter UPLINKCOUNT = 1,
      parameter DOWNLINKCOUNT = 1)
    (
    input               clk,
    input               rst,
    input MiniDAQ_reg_rst,


    input [159:0]  command_r,
    input          MiniDAQ_reg_write_i,
    input          MiniDAQ_reg_read_i,

    input [UPLINKCOUNT*10-1:0] locked_dline0,
    input [UPLINKCOUNT*10-1:0] locked_dline1,

    input [UPLINKCOUNT-1:0] lpgbtfpga_uplinkrdy,
    input [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrdy,


    output reg          data_back_write_o,
    output reg [159:0]  data_back_o,

    output reg          sc_reset_o,
    output reg          global_rst_uart_o,
    output reg          bc_rst_uart_o,
    output reg          event_rst_uart_o,
    output reg          master_rst_uart_o,
    output reg          mezz_TRST_o,

    output reg  [3:0]   uplink_to_uart_mapping_o,
    output reg          uplink_reset_uart_o,
    output reg          downlink_reset_uart_o,

    output reg          en_int_trigger_o,
    output reg          enable_hit_o,
    output reg          hit_inv_o,
    output reg          hit_single_o,
    output reg  [11:0]  hit_width_o,
    output reg  [23:0]  hit_interval_o,

    output reg  [19:0]  correct_counter_th_o,
    output reg          enable_K28_1_o,
    output reg          enable_320M_o,

    output reg  [7:0]   trigger_deadtime_o,
    output reg  [11:0]  match_window_o,
    output reg  [11:0]  search_margin_o,
    output reg  [11:0]  bc_offset_o,
    output reg  [11:0]  reject_offset_o,
    output reg  [11:0]  rollover_o,
    output reg  [11:0]  coarse_count_offset_o,
    output reg          en_relative_trig_data_o,
    output reg          enable_matching_o,
    output reg          trigger_redge_o,
    output reg  [7:0]   trigger_latency_o

);
    
reg [7:0] parity_byte;
reg readback_reg_write;
reg [159:0] readback_reg;

localparam ZERO_PADDING_BITS = 128 - (UPLINKCOUNT * 20);
localparam UPLINK_PADDING = 8 - UPLINKCOUNT;
localparam DOWNLINK_PADDING = 8 - DOWNLINKCOUNT;

// output control
always @(posedge clk  ) begin
    if (rst|MiniDAQ_reg_rst) begin
        readback_reg_write <= 1'b0;
        data_back_write_o  <= 1'b0;
        data_back_o <= 'b0;
    end else begin
        readback_reg_write <= MiniDAQ_reg_read_i;
        data_back_write_o  <= readback_reg_write;//data_back_write_o is 2
                        //clock behind MiniDAQ_reg_read_i so it is aligned with data_back_o
        data_back_o <= {readback_reg[159:8],parity_byte};
    end
end


integer i;
always @* begin
    parity_byte = 8'b0;
    for (i = 0; i < 20; i = i + 1) begin
        parity_byte = parity_byte ^ readback_reg[i*8 +: 8]; // XOR each 8-bit segment
    end
end


always @(posedge clk  ) begin
    if (rst|MiniDAQ_reg_rst) begin
        // default values
        uplink_to_uart_mapping_o <= 4'b0; //CSM0
        uplink_reset_uart_o   <= 1'b0;
        downlink_reset_uart_o <= 1'b0;
        sc_reset_o            <= 1'b0;
        global_rst_uart_o     <= 1'b0;
        bc_rst_uart_o         <= 1'b0;
        event_rst_uart_o      <= 1'b0;
        master_rst_uart_o     <= 1'b0;
        mezz_TRST_o           <= 1'b1;

        en_int_trigger_o      <= 1'b0;
        enable_hit_o          <= 1'b0;
        hit_inv_o             <= 1'b1;
        hit_single_o          <= 1'b0;
        hit_width_o           <= 12'h4;
        hit_interval_o        <= 24'd40000;  //rate = 40M/(1+interval), default to 1KHz

        correct_counter_th_o    <=  20'd1000000;
        enable_K28_1_o          <=  1'b1;
        enable_320M_o           <=  1'b1;

        trigger_deadtime_o      <=  8'd10;
        match_window_o          <=  12'd60;
        search_margin_o         <=  12'd100;
        bc_offset_o             <=  12'd0;
        reject_offset_o         <=  12'd300;
        rollover_o              <=  12'd4095;
        coarse_count_offset_o   <=  12'd4070;
        en_relative_trig_data_o <=  1'b0;
        enable_matching_o       <=  1'b1;
        trigger_redge_o         <=  1'b0;
        trigger_latency_o       <=  12'd100;

    end else if(MiniDAQ_reg_write_i) begin
        case(command_r[151:144])
            8'd1:uplink_to_uart_mapping_o <= command_r[3:0];
            8'd2:uplink_reset_uart_o      <= command_r[0];
            8'd3:downlink_reset_uart_o    <= command_r[0];
            8'd4:sc_reset_o               <= command_r[0];
            8'd5:global_rst_uart_o        <= command_r[0];
            8'd6:bc_rst_uart_o            <= command_r[0];
            8'd7:event_rst_uart_o         <= command_r[0];
            8'd8:master_rst_uart_o        <= command_r[0];
            8'd9:mezz_TRST_o              <= command_r[0];

            8'd10:en_int_trigger_o        <= command_r[0];
            8'd11:enable_hit_o            <= command_r[0];
            8'd12:hit_inv_o               <= command_r[0];
            8'd13:hit_single_o            <= command_r[0];
            8'd14:hit_width_o             <= command_r[11:0];
            8'd15:hit_interval_o          <= command_r[23:0];  //rate = 40M/(1+interval), default to 1KHz


            8'd16:correct_counter_th_o    <= command_r[19:0];
            8'd17:enable_K28_1_o          <= command_r[0];
            8'd18:enable_320M_o           <= command_r[0];

            8'd19:trigger_deadtime_o      <= command_r[7:0];
            8'd20:match_window_o          <= command_r[11:0];
            8'd21:search_margin_o         <= command_r[11:0];
            8'd22:bc_offset_o             <= command_r[11:0];
            8'd23:reject_offset_o         <= command_r[11:0];
            8'd24:rollover_o              <= command_r[11:0];
            8'd25:coarse_count_offset_o   <= command_r[11:0];
            8'd26:en_relative_trig_data_o <= command_r[0];
            8'd27:enable_matching_o       <= command_r[0];
            8'd28:trigger_redge_o         <= command_r[0];
            8'd29:trigger_latency_o       <= command_r[7:0];
            default: begin end
        endcase
    end
end

wire [UPLINKCOUNT*20-1:0] dline_status;
genvar j;
for (j = 0; j < UPLINKCOUNT; j=j+1) begin : uplink
    
    assign dline_status[j*20+10-1:j*20]    = locked_dline0[j*10+9:j*10];
    assign dline_status[j*20+20-1:j*20+10] = locked_dline1[j*10+9:j*10];
end

always @(posedge clk  ) begin
    if (rst|MiniDAQ_reg_rst) begin
        readback_reg <= 160'b0;
    end else if(MiniDAQ_reg_read_i) begin
        case(command_r[151:144])
            8'd1:readback_reg <= {command_r[151:144],112'b0,            
                7'b0,en_int_trigger_o,
                enable_hit_o,hit_inv_o,hit_single_o,enable_K28_1_o,
                enable_320M_o,en_relative_trig_data_o,enable_matching_o,trigger_redge_o,
                4'b0,uplink_to_uart_mapping_o,
                uplink_reset_uart_o,downlink_reset_uart_o,sc_reset_o,global_rst_uart_o,
                bc_rst_uart_o,event_rst_uart_o,master_rst_uart_o,mezz_TRST_o,
                8'b0};
            8'd2:readback_reg <= {command_r[151:144],
                hit_interval_o,  //3 bytes
                hit_width_o,correct_counter_th_o, //4 bytes
                trigger_deadtime_o,
                match_window_o,search_margin_o,bc_offset_o,reject_offset_o,rollover_o,coarse_count_offset_o, //9 bytes
                trigger_latency_o,8'b0};
            8'd3:
                readback_reg <= {command_r[151:144], {ZERO_PADDING_BITS{1'b0}},   
                dline_status,
                {UPLINK_PADDING{1'b0}},lpgbtfpga_uplinkrdy,
                {DOWNLINK_PADDING{1'b0}},lpgbtfpga_downlinkrdy,
                8'b0};
            default: begin end
        endcase
    end
end


vio_hit vio_hit_inst (
  .clk(clk),                // input wire clk
  .probe_in0(enable_hit_o),    // input wire [0 : 0] probe_in0
  .probe_in1(en_int_trigger_o),    // input wire [0 : 0] probe_in1
  .probe_in2(hit_inv_o),    // input wire [0 : 0] probe_in2
  .probe_in3(hit_width_o),    // input wire [11 : 0] probe_in3
  .probe_in4(hit_interval_o),    // input wire [23 : 0] probe_in4
  .probe_in5(bc_rst_uart_o),    // input wire [0 : 0] probe_in5
  .probe_in6(hit_single_o),    // input wire [0 : 0] probe_in6
  .probe_in7(event_rst_uart_o),    // input wire [0 : 0] probe_in7
  .probe_in8(master_rst_uart_o),    // input wire [0 : 0] probe_in8
  .probe_in9(correct_counter_th_o),    // input wire [19 : 0] probe_in9
  .probe_in10(enable_K28_1_o),  // input wire [0 : 0] probe_in10
  .probe_in11(enable_320M_o),  // input wire [0 : 0] probe_in11
  .probe_in12(trigger_deadtime_o),  // input wire [7 : 0] probe_in12
  .probe_in13(match_window_o),  // input wire [11 : 0] probe_in13
  .probe_in14(search_margin_o),  // input wire [11 : 0] probe_in14
  .probe_in15(bc_offset_o),  // input wire [11 : 0] probe_in15
  .probe_in16(reject_offset_o),  // input wire [11 : 0] probe_in16
  .probe_in17(rollover_o),  // input wire [11 : 0] probe_in17
  .probe_in18(coarse_count_offset_o),  // input wire [11 : 0] probe_in18
  .probe_in19(en_relative_trig_data_o),  // input wire [0 : 0] probe_in19
  .probe_in20(enable_matching_o),  // input wire [0 : 0] probe_in20
  .probe_in21(trigger_redge_o),  // input wire [0 : 0] probe_in21
  .probe_in22(trigger_latency_o),  // input wire [7 : 0] probe_in22
  .probe_in23(mezz_TRST_o),  // input wire [0 : 0] probe_in23
  .probe_in24(uplink_reset_uart_o),  // input wire [0 : 0] probe_in24
  .probe_in25(downlink_reset_uart_o),  // input wire [0 : 0] probe_in25
  .probe_in26(sc_reset_o),  // input wire [0 : 0] probe_in26
  .probe_in27(global_rst_uart_o)  // input wire [0 : 0] probe_in27
);


endmodule
