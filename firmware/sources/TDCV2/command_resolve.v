/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : command_resolve.v
// Create : 2021-06-24 00:57:05
// Revise : 2025-03-06 10:36:12
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------
// command[159]=1: lpGBT multi-byte write
// command[158]=1: fpga config
// command[158]=1 && command[156]=1: fpga reg reset
// command[157]=1: fpga readback
// command[79]=1: SCA write
// command[41]=1: lpGBT single-byte write
// command[40]=1: lpGBT read

// lpGBT multi-byte write
// [7:0] GBTx_address,[15:0] Register_addr,[7:0] value,[1:0] which_CSM, [5:0] nb_to_be_read(value_length)
// [1:0] which_CSM: 0 = CSM0, 1 = CSM1, 2 = CSM2, 3 = CSM3

// SCA write command format
// command[78:75]: 
// [3:0] sca_enable_array: {sca3,sca2,sca1,sca0}
// command[74:72]: 
// reset_gbtsc, start_reset, start_connect
// command[71:0]:
// which_CSM,[6:0] tx_address,[7:0] tx_transID,[7:0] tx_channel,[7:0] tx_len,[7:0] tx_command,[31:0] tx_data
// 
//


  
module command_resolve(
    input               clk,
    input               rst,

    input   [159:0]     command_i,
    input               command_fifo_empty_i,
    output reg          command_fifo_read_o,
    output reg [159:0]  command_r,

    output reg          MiniDAQ_reg_write_o,
    output reg          MiniDAQ_reg_read_o,
    output reg          MiniDAQ_reg_rst_o,


    //  For lpGBT config
    output reg  [7:0]   ic_GBTx_address_o,
    output reg  [15:0]  ic_Register_addr_o,
    // output reg  [1:0]   ic_which_CSM_o,
    output reg  [7:0]   ic_nb_to_be_read_o,

    output reg          ic_wr_start_o, //ic FIFO flush out to lpGBT
    output reg          ic_rd_start_o, //lpGBT read back to ic FIFO
    //write to internal ic FIFO
    output reg  [7:0]   ic_wfifo_data_o,
    output reg          ic_fifo_wr_o,

    //To GBT-SC
    
    output reg          sca_start_reset_o,
    output reg          sca_start_connect_o,
    output reg          sca_start_command_o,
    output reg  [3:0]   sca_enable_array_o,  
    output reg          sca_which_CSM_o,  
    output reg  [7:0]   sca_tx_address_o,
    output reg  [7:0]   sca_tx_transID_o,
    output reg  [7:0]   sca_tx_channel_o,
    output reg  [7:0]   sca_tx_len_o,
    output reg  [7:0]   sca_tx_command_o,
    output reg  [31:0]  sca_tx_data_o
);
    
localparam COMMAND_LEN        = 9;
localparam IDLE               = 1;
localparam RESOLVE_COMMAND    = 2;
localparam LPGBT_MULTI_WRITE  = 4;
localparam LPGBT_SEND         = 8;
localparam GBTSCA_COMMAND     = 16;
localparam FPGA_CONFIG        = 32;
localparam READBACK           = 64;
localparam BITFILE_WRITE      = 128;
localparam JTAG_COMMAND       = 256;


localparam IDLE_BIT               =0;
localparam RESOLVE_COMMAND_BIT    =1;
localparam LPGBT_MULTI_WRITE_BIT  =2;
localparam LPGBT_SEND_BIT         =3;
localparam GBTSCA_COMMAND_BIT     =4;
localparam FPGA_CONFIG_BIT        =5;
localparam READBACK_BIT           =6;
localparam BITFILE_WRITE_BIT      =7;
localparam JTAG_COMMAND_BIT       =8;


reg     [COMMAND_LEN-1:0]   state;
reg     [COMMAND_LEN-1:0]   nextstate;

reg     [7:0]   value_length;
reg     [7:0]   multi_counter;
wire    [7:0]   fifo_write_data;
wire    [7:0]   GBTx_address_data;
wire    [15:0]  Register_addr_data;



always @(posedge clk  ) begin
    if (rst) begin
        // reset
        state <= IDLE;
    end else begin
        state <= nextstate;
    end
end

always @(posedge clk  ) begin
    if (rst) begin
        // reset
        multi_counter <= 'b0;
    end else if(state == LPGBT_MULTI_WRITE) begin
        multi_counter <= (~|multi_counter) ? 8'b0000_0000:( multi_counter -8'b0000_0001);
        
    end else if(nextstate==LPGBT_MULTI_WRITE) begin
        multi_counter <= value_length-1'b1; 
    end else begin
    
        multi_counter <= 'b0;
    end
end

always @(*) begin
    case(state)
        IDLE: 
            nextstate = (~command_fifo_empty_i) ? RESOLVE_COMMAND : IDLE;
        RESOLVE_COMMAND:
            nextstate = command_r[159] ? LPGBT_MULTI_WRITE :
                        command_r[158] ? FPGA_CONFIG :
                        command_r[157] ? READBACK :
                        command_r[79]  ? GBTSCA_COMMAND :
                        command_r[41]  ? LPGBT_MULTI_WRITE :
                        command_r[40]  ? LPGBT_SEND : //LPGBT_MULTI bytes read config
                        command_r[35]  ? BITFILE_WRITE : //write bit file content to SCA TDO
                        command_r[3]   ? JTAG_COMMAND : //write bit file content to SCA TDO
                        IDLE;
        LPGBT_MULTI_WRITE:
            nextstate = (~|multi_counter) ? LPGBT_SEND : LPGBT_MULTI_WRITE;
        LPGBT_SEND:
            nextstate = IDLE;
        GBTSCA_COMMAND:
            nextstate = IDLE;
        FPGA_CONFIG:
            nextstate = IDLE;
        READBACK:
            nextstate = IDLE;
        BITFILE_WRITE:
            nextstate = IDLE;
        JTAG_COMMAND:
            nextstate = IDLE;
        default: nextstate = IDLE;
    endcase
end

always @(posedge clk) begin
    if (rst) begin
        // reset
        command_fifo_read_o <= 1'b0;
        command_r           <= 'b0;
    end else if (nextstate[RESOLVE_COMMAND_BIT]) begin
        command_fifo_read_o <= 1'b1;
        command_r           <= command_i;
        value_length        <= command_i[7:0];//get length
    end else if (nextstate[READBACK_BIT]) begin
        command_fifo_read_o <= 1'b0;

    end else begin
        command_fifo_read_o <= 1'b0;
    end
end

genvar i;
for (i=0;i<8;i=i+1) begin
    assign fifo_write_data[i] = command_r[8+{multi_counter,3'b000}+i];   
end

genvar j,k;
for (j=0;j<8;j=j+1) begin
    assign GBTx_address_data[j] = command_r[8+16+{value_length,3'b000}+j];   
end
for (k=0;k<16;k=k+1) begin
    assign Register_addr_data[k] = command_r[8+{value_length,3'b000}+k];   
end


// state machine for lpgbt config (read, single-byte write and multi-byte write)
always @(posedge clk) begin
    ic_wfifo_data_o       <= 'b0;
    ic_fifo_wr_o          <= 'b0;           

    // ic_GBTx_address_o  <= 'b0;
    // ic_Register_addr_o <= 'b0;        
    // ic_nb_to_be_read_o <= 'b0;
    // ic_which_CSM_o     <= 'b0;     //commented to be compatible with ic_tx.vhd 
    ic_wr_start_o         <= 'b0;
    ic_rd_start_o         <= 'b0;

    MiniDAQ_reg_write_o   <= 'b0;
    MiniDAQ_reg_rst_o     <= 'b0;
    MiniDAQ_reg_read_o    <= 'b0;

    sca_start_command_o   <= 'b0;
    sca_start_reset_o     <= 'b0;
    sca_start_connect_o   <= 'b0;

    if(rst) begin
        
    end else if(state[LPGBT_MULTI_WRITE_BIT]) begin 
        ic_wfifo_data_o     <= fifo_write_data;              
        ic_fifo_wr_o        <= 1'b1;             
    end else if (state[LPGBT_SEND_BIT]) begin
        ic_GBTx_address_o   <= GBTx_address_data;
        ic_Register_addr_o  <= Register_addr_data;
        ic_nb_to_be_read_o  <= command_r[7:0];//truncated
        // ic_which_CSM_o    <= command_r[7:6];
        ic_wr_start_o       <= command_r[159]|((~command_r[159])&&command_r[41]);
        ic_rd_start_o       <= (~command_r[159])&&command_r[40];       
    end else if (state[GBTSCA_COMMAND_BIT]) begin
        sca_enable_array_o  <= command_r[78:75];
        sca_which_CSM_o     <= command_r[71];
        if (command_r[73]) begin
            sca_start_reset_o   <= 'b1;
        end else if (command_r[72]) begin
            sca_start_connect_o <= 'b1;
        end else begin            
            sca_tx_address_o    <= {1'b0,command_r[70:64]};
            sca_tx_transID_o    <= command_r[63:56];
            sca_tx_channel_o    <= command_r[55:48];
            sca_tx_len_o        <= command_r[47:40];
            sca_tx_command_o    <= command_r[39:32];
            sca_tx_data_o       <= command_r[31:0];
            sca_start_command_o <= 'b1;
        end
    end else if (state[FPGA_CONFIG_BIT]) begin
        MiniDAQ_reg_write_o <= 1'b1;
        if (command_r[156]) begin
            MiniDAQ_reg_rst_o <= 1'b1;
        end
    end else if (state[READBACK_BIT]) begin
        MiniDAQ_reg_read_o <= 1'b1;
    end else if (state[BITFILE_WRITE_BIT]) begin
        sca_tx_data_o       <= command_r[31:0];
        sca_tx_channel_o    <= 8'h13;    //SCA JTAG_CHANNEL = 0x13
        sca_tx_len_o        <= 8'h4;  //may not be useful
        sca_tx_command_o    <= {1'b0,command_r[34:32],4'b0};        
        sca_start_command_o <= 'b1;   
    end else if (state[JTAG_COMMAND_BIT]) begin
        sca_tx_data_o       <= 32'b0;
        sca_tx_channel_o    <= 8'h13;    //SCA JTAG_CHANNEL = 0x13
        sca_tx_len_o        <= 8'h4;  //may not be useful
        if(command_r[0]) begin
            sca_tx_command_o    <= 8'hA2;  //SCA JTAG_GO
        end else begin
            sca_tx_command_o    <= 8'h81;  //SCA JTAG_R_CTRL
        end        
        sca_start_command_o <= 'b1;   
    end
end

ila_uart ila_uart_inst (
    .clk(clk), // input wire clk


    .probe0(state), // input wire [5:0]  probe0  
    .probe1(nextstate), // input wire [5:0]  probe1 
    .probe2(command_r), // input wire [159:0]  probe2 
    .probe3(rst), // input wire [0:0]  probe3 
    .probe4(command_fifo_empty_i), // input wire [0:0]  probe4 
    .probe5(data_back_write_o), // input wire [0:0]  probe5 
    .probe6(MiniDAQ_reg_write_o), // input wire [0:0]  probe6 
    .probe7(MiniDAQ_reg_read_o), // input wire [0:0]  probe7 
    .probe8(MiniDAQ_reg_rst_o), // input wire [0:0]  probe8 
    .probe9(command_fifo_read_o) // input wire [0:0]  probe9
);



// ila_command_resolve ila_command_resolve_inst (
//     .clk(clk), // input wire clk

//     .probe0(sc_reset_o), // input wire [0:0]  probe0  
//     .probe1(sca_start_reset_o), // input wire [0:0]  probe1 
//     .probe2(sca_start_connect_o), // input wire [0:0]  probe2 
//     .probe3(sca_start_command_o), // input wire [0:0]  probe3 
//     .probe4(state), // input wire [5:0]  probe4 
//     .probe5(nextstate), // input wire [5:0]  probe5 
//     .probe6(command_r), // input wire [159:0]  probe6 
//     .probe7(rst), // input wire [0:0]  probe7 
//     .probe8(sca_tx_address_o), // input wire [7:0]  probe8 
//     .probe9(sca_tx_transID_o), // input wire [7:0]  probe9 
//     .probe10(sca_tx_channel_o), // input wire [7:0]  probe10 
//     .probe11(sca_tx_len_o), // input wire [7:0]  probe11 
//     .probe12(sca_tx_command_o), // input wire [7:0]  probe12 
//     .probe13(sca_start_command_o), // input wire [31:0]  probe13
//     .probe14(command_fifo_empty_i), // input wire [0:0]  probe14 
//     .probe15(command_fifo_read_o), // input wire [0:0]  probe15 
//     .probe16(ic_GBTx_address_o), // input wire [7:0]  probe16 
//     .probe17(ic_Register_addr_o), // input wire [15:0]  probe17 
//     .probe18(ic_which_CSM_o), // input wire [2:0]  probe18 
//     .probe19(ic_nb_to_be_read_o), // input wire [7:0]  probe19 
//     .probe20(ic_wr_start_o), // input wire [0:0]  probe20 
//     .probe21(ic_rd_start_o), // input wire [0:0]  probe21 
//     .probe22(ic_wfifo_data_o), // input wire [7:0]  probe22 
//     .probe23(ic_fifo_wr_o), // input wire [0:0]  probe23
//     .probe24(uplink_to_uart_mapping_o) // input wire [3:0]  probe24
// );
endmodule
