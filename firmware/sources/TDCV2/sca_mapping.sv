/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : sca_mapping.sv
// Create : 2024-07-24 20:39:17
// Revise : 2024-11-01 14:21:00
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------
  
module sca_mapping
    #(parameter SCA_PER_DOWNLINK = 4)
    (
    input   [1:0]   sca_line_tx[SCA_PER_DOWNLINK-1:0],
    input   [1:0]   ttc_ctrl,
    input   [229:0] uplinkUserData,

    output  [1:0]   sca_line_rx[SCA_PER_DOWNLINK-1:0],    
    output  [31:0]  downlinkUserData

);

wire    [1:0]   down_elink_80m_sca_pri[SCA_PER_DOWNLINK-1:0];
wire    [1:0]   down_elink_80m_sca_aux[SCA_PER_DOWNLINK-1:0];
wire    [7:0]   uplink_320m [27:0];
wire    [7:0]   uplink_320m_sca_pri[SCA_PER_DOWNLINK-1:0];

genvar m,k;
for (m = 0; m < 28; m=m+1) begin
    assign uplink_320m[m] = uplinkUserData[m*8+7:m*8]; 
end

for (k = 0; k < SCA_PER_DOWNLINK; k=k+1) begin
    //2-bit downlink sca tx mapping
    assign down_elink_80m_sca_pri[k] = sca_line_tx[k];
    assign down_elink_80m_sca_aux[k] = 2'b0;
     
    assign uplink_320m_sca_pri[k] = k==0? uplink_320m[16] :
                                    k==1? uplink_320m[11] :
                                    k==2? uplink_320m[13] : uplink_320m[15];

    //2-bit uplink sca rx mapping
    //sca bits are quadruple (80Mbps sampled by 320Mbps)
    // assign sca_line_rx[k] = {uplink_320m_sca_pri[k][4],uplink_320m_sca_pri[k][0]}; 
    //here used voting to select the best phase
    majority_voter_one_bit
    inst_majority_voter_one_bit_LSB (
        .inA(uplink_320m_sca_pri[k][0]), 
        .inB(uplink_320m_sca_pri[k][1]), 
        .inC(uplink_320m_sca_pri[k][2]), 
        .out(sca_line_rx[k][0]), 
        .err());

    majority_voter_one_bit 
    inst_majority_voter_one_bit_MSB (
        .inA(uplink_320m_sca_pri[k][4]), 
        .inB(uplink_320m_sca_pri[k][5]), 
        .inC(uplink_320m_sca_pri[k][6]), 
        .out(sca_line_rx[k][1]), 
        .err());
end

// for 4 SCA version 
assign downlinkUserData = {4'b0,down_elink_80m_sca_pri[0],down_elink_80m_sca_aux[0],
                                             down_elink_80m_sca_pri[3],2'b00,down_elink_80m_sca_aux[3],
                                             down_elink_80m_sca_pri[2],down_elink_80m_sca_aux[2],
                                             down_elink_80m_sca_aux[1],down_elink_80m_sca_pri[1],
                                             4'b0000,ttc_ctrl,2'b00,ttc_ctrl};   
// for 3 SCA version
// assign downlinkUserData = {8'b0,down_elink_80m_sca_aux[0],2'b00,down_elink_80m_sca_pri[0],
//                                             down_elink_80m_sca_pri[2],2'b00,down_elink_80m_sca_aux[2],
//                                             down_elink_80m_sca_aux[1],down_elink_80m_sca_pri[1],8'b0,ttc_ctrl};
      


endmodule

