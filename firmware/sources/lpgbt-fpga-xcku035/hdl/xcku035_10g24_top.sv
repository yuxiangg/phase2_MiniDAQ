/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : xcku035_10g24_top.sv
// Create : 2023-10-12 23:04:23
// Revise : 2025-03-06 17:04:03
// Editor : sublime text4, tab size (3)
// Description: 
//
// -----------------------------------------------------------------------------


module xcku035_10g24_top #(
    parameter UPLINKCOUNT = 4,
    parameter DOWNLINKCOUNT = 2)
    (
    // --===============--
    //   -- Clocks scheme --
    //   --===============--       
    //   -- MGT(GTX) reference clock:
    //   ----------------------------
      
    //   -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
    //   --
    //   --          * The MGT reference clock frequency must be 320MHz.
    input           SMA_MGT_REFCLK_P,
    input           SMA_MGT_REFCLK_N,
        
      // -- Fabric clock: 200MHz
    input           USER_CLOCK_P,
    input           USER_CLOCK_N,

    input           TRIGGER0_P,
    input           TRIGGER0_N,

    input           TRIGGER1_P,
    input           TRIGGER1_N,



    // --hit to mezz
    // output  [23:0]  hit,
    output          mezz_TRST,

    // -- MGT(GTX) --
    output  [UPLINKCOUNT-1:0]   SFP_TX_P,
    output  [UPLINKCOUNT-1:0]   SFP_TX_N,
    input   [UPLINKCOUNT-1:0]   SFP_RX_P,
    input   [UPLINKCOUNT-1:0]   SFP_RX_N,
       
    // -- SFP control
    output  [7:0]   SFP_TX_DISABLE,

    // --====================--
    // -- Signals forwarding --
    // --====================--

    // -- SMA output:
    // --------------
    output          USER_SMA_GPIO_P,
    output          USER_SMA_GPIO_N,


    // -- 125 MHz clock from MMCM
    // --      gtx_clk_bufg_out:       OUT STD_LOGIC;


    //  320 MHz clock in the same clock region with rgmii_tx
    input           USER_CLK_320_P,
    input           USER_CLK_320_N,
    output          phy_resetn,

    // -- RGMII Interface
    // ------------------
    output  [3:0]   rgmii_txd,
    output          rgmii_tx_ctl,
    output          rgmii_txc,
    input   [3:0]   rgmii_rxd,
    input           rgmii_rx_ctl,
    input           rgmii_rxc,

    // -- MDIO Interface
    //     -----------------
    inout           mdio,
    output          mdc,

    // -- UART Interface
    input           uart_rxd,
    output          uart_txd
);


localparam SCA_PER_DOWNLINK = 4;

localparam FEC5           =1;
localparam FEC12          =2;
localparam DATARATE_5G12  =1;
localparam DATARATE_10G24 =2;
localparam PCS            =0;
localparam PMA            =1;

//unused pins
// assign USER_SMA_GPIO_P =1'b0;
// assign USER_SMA_GPIO_N =1'b0;
assign SFP_TX_DISABLE = 'b0;


// -- Clocks:
wire mgtRefClk_from_smaMgtRefClkbuf;   //external 320MHz reference clock for MGT

wire mgt_freedrpclk;   //async clock for MGT reset and all vios

wire lpgbtfpga_mgttxclk;   // 320MHz clock recovered from MGT tx
wire lpgbtfpga_mgtrxclk;   // 320MHz clock recovered from MGT rx


// -- User CDC for lpGBT-FPGA      
wire lpgbtfpga_clk40;      //40MHz divided from 320MHz clock recovered from MGT tx

wire [2:0] uplinkPhase[UPLINKCOUNT-1:0];
wire [2:0] uplinkPhaseCalib[UPLINKCOUNT-1:0];
wire [UPLINKCOUNT-1:0] uplinkPhaseForce;
               
                                                                 
wire [9:0] downlinkPhase[DOWNLINKCOUNT-1:0];
wire [9:0] downlinkPhaseCalib[DOWNLINKCOUNT-1:0];

wire [DOWNLINKCOUNT-1:0] downlinkPhaseForce;

// -- LpGBT-FPGA
wire [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrst;
wire [DOWNLINKCOUNT-1:0] lpgbtfpga_downlinkrdy;
                             

wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkrst; 
wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkrdy;


// wire [31:0] lpgbtfpga_downlinkUserData_fromgen [DOWNLINKCOUNT-1:0];
wire [31:0] lpgbtfpga_downlinkUserData[DOWNLINKCOUNT-1:0];


wire [1:0] lpgbtfpga_downlinkEcData[DOWNLINKCOUNT-1:0];
wire [1:0] lpgbtfpga_downlinkIcData[DOWNLINKCOUNT-1:0];
assign lpgbtfpga_downlinkEcData = lpgbtfpga_downlinkIcData;

wire [229:0] lpgbtfpga_uplinkUserData[UPLINKCOUNT-1:0];
wire [1:0] lpgbtfpga_uplinkEcData[UPLINKCOUNT-1:0];
wire [1:0] lpgbtfpga_uplinkIcData[UPLINKCOUNT-1:0];
wire [UPLINKCOUNT-1:0] lpgbtfpga_uplinkclk;


wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txpolarity;

wire [UPLINKCOUNT-1:0] lpgbtfpga_mgt_rxpolarity;


wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txaligned;
wire [6:0]lpgbtfpga_mgt_txpiphase[DOWNLINKCOUNT-1:0];
wire [6:0]lpgbtfpga_mgt_txpicalib[DOWNLINKCOUNT-1:0];
wire [DOWNLINKCOUNT-1:0] lpgbtfpga_mgt_txcaliben;
                
wire [DOWNLINKCOUNT-1:0] downLinkBypassInterleaver;
wire [DOWNLINKCOUNT-1:0] downLinkBypassFECEncoder;
wire [DOWNLINKCOUNT-1:0] downLinkBypassScrambler;

wire [UPLINKCOUNT-1:0] upLinkScramblerBypass;
wire [UPLINKCOUNT-1:0] upLinkFecBypass;
wire [UPLINKCOUNT-1:0] upLinkInterleaverBypass;

wire [UPLINKCOUNT-1:0] upLinkFECCorrectedClear;
wire [UPLINKCOUNT-1:0] upLinkFECCorrectedLatched;



// -- Config
wire uplinkSelectDataRate = 1'b0;

wire [DOWNLINKCOUNT-1:0] generator_rst;
wire [1:0] downconfig_g0[DOWNLINKCOUNT-1:0];
wire [1:0] downconfig_g1[DOWNLINKCOUNT-1:0];
wire [1:0] downconfig_g2[DOWNLINKCOUNT-1:0];
wire [1:0] downconfig_g3[DOWNLINKCOUNT-1:0];
wire [15:0] downlink_gen_rdy[DOWNLINKCOUNT-1:0];

wire [55:0] upelink_config[UPLINKCOUNT-1:0];
wire [27:0] uperror_detected[UPLINKCOUNT-1:0];
wire [UPLINKCOUNT-1:0] reset_upchecker;
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// --SCA debug
wire    [SCA_PER_DOWNLINK-1:0]  sca_enable_array_uart[DOWNLINKCOUNT-1:0];
wire    sc_reset_uart;
wire    [DOWNLINKCOUNT-1:0]     sca_start_reset_uart;
wire    [DOWNLINKCOUNT-1:0]     sca_start_connect_uart;
wire    [DOWNLINKCOUNT-1:0]     sca_start_command_uart;
// SCA tx
wire    [7:0]                   sca_tx_address_uart[DOWNLINKCOUNT-1:0];
wire    [7:0]                   sca_tx_transID_uart[DOWNLINKCOUNT-1:0];
wire    [7:0]                   sca_tx_channel_uart[DOWNLINKCOUNT-1:0];
wire    [7:0]                   sca_tx_len_uart[DOWNLINKCOUNT-1:0];
wire    [7:0]                   sca_tx_command_uart[DOWNLINKCOUNT-1:0];
wire    [31:0]                  sca_tx_data_uart[DOWNLINKCOUNT-1:0];

//SCA rx
wire    [159:0]                 sca_rx_parr[DOWNLINKCOUNT-1:0];
wire    [SCA_PER_DOWNLINK-1:0]  sca_rx_done[DOWNLINKCOUNT-1:0];  //  Reply received flag (pulse)
wire    [7:0]                   sca_rx_addr[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //  --! Reply: address field (According to the SCA manual)
// wire    [7:0]                   sca_rx_ctrl[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //  --! Reply: control field (According to the SCA manual)
wire    [7:0]                   sca_rx_trid[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //  --! Reply: transaction ID field (According to the SCA manual)
wire    [7:0]                   sca_rx_ch[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //    --! Reply: channel field (According to the SCA manual)
wire    [7:0]                   sca_rx_len[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //   --! Reply: len field (According to the SCA manual)
wire    [7:0]                   sca_rx_err[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  //   --! Reply: error field (According to the SCA manual)
wire    [31:0]                  sca_rx_data[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];  // --!Reply: data field (According to the SCA manual)

// -- EC line
wire [1:0]sca_line_tx[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];       // --! (TX) Array of bits to be mapped to the TX GBT-Frame
wire [1:0]sca_line_rx[DOWNLINKCOUNT-1:0][SCA_PER_DOWNLINK-1:0];       // --! (RX) Array of bits to be mapped to the RX GBT-Frame



// -- IC Debug
// ------------
wire [DOWNLINKCOUNT-1:0] ic_tx_ready;
wire [UPLINKCOUNT-1:0]  ic_rx_fifo_empty;

wire [7:0] GBTx_address[DOWNLINKCOUNT-1:0];
wire [15:0] Register_ptr[DOWNLINKCOUNT-1:0];
wire [15:0] ic_nb_to_be_read_rd[DOWNLINKCOUNT-1:0];

wire [DOWNLINKCOUNT-1:0] ic_start_wr;
wire [DOWNLINKCOUNT-1:0] ic_wr;
wire [7:0] ic_data_wr[DOWNLINKCOUNT-1:0];  

wire [DOWNLINKCOUNT-1:0] ic_start_rd;    
wire [DOWNLINKCOUNT-1:0] ic_rd;
wire [7:0] ic_data_rd[DOWNLINKCOUNT-1:0];

wire [7:0] GBTx_rd_addr[DOWNLINKCOUNT-1:0];
wire [15:0] GBTx_rd_mem_ptr[DOWNLINKCOUNT-1:0];
wire [15:0] GBTx_rd_mem_size[DOWNLINKCOUNT-1:0];
wire ic_empty_rd;

//to gbtic
wire [7:0] GBTx_address_to_gbtic[DOWNLINKCOUNT-1:0];
wire [15:0] Register_addr_to_gbtic[DOWNLINKCOUNT-1:0];
wire [15:0] nb_to_be_read_to_gbtic[DOWNLINKCOUNT-1:0];

wire [DOWNLINKCOUNT-1:0] start_write_to_gbtic;
wire [DOWNLINKCOUNT-1:0] wr_to_gbtic;
wire [7:0] data_to_gbtic[DOWNLINKCOUNT-1:0];  

wire [DOWNLINKCOUNT-1:0] start_read_to_gbtic; 
wire [DOWNLINKCOUNT-1:0] rd_to_gbtic;
wire [7:0] data_from_gbtic[UPLINKCOUNT-1:0];

wire [7:0] ic_rd_gbtx_addr[DOWNLINKCOUNT-1:0];
wire [15:0] ic_rd_mem_ptr[DOWNLINKCOUNT-1:0];
wire [15:0] ic_rd_nb_of_words[DOWNLINKCOUNT-1:0];

//elink sca data, only comes from master lpGBT
wire [DOWNLINKCOUNT-1:0]txFrameClkPllLocked_from_gbtExmplDsgn;


// -- Frame clock:
// ---------------
wire txFrameClk_from_txPll;

wire fast_clock_for_meas;
wire [31:0] cmd_delay;

wire [1:0] ttc_ctrl;
wire trigger_SMA;
wire trigger_out;


wire LOCK_TOP;


wire    [7:0]                   ic_GBTx_address_uart[DOWNLINKCOUNT-1:0];
wire    [15:0]                  ic_Register_addr_uart[DOWNLINKCOUNT-1:0];
wire    [7:0]                   ic_nb_to_be_read_uart[DOWNLINKCOUNT-1:0];
wire    [DOWNLINKCOUNT-1:0]     ic_wr_start_uart;
wire    [DOWNLINKCOUNT-1:0]     ic_rd_start_uart;
wire    [7:0]                   ic_wfifo_data_uart[DOWNLINKCOUNT-1:0];
wire    [DOWNLINKCOUNT-1:0]     ic_fifo_wr_uart;
wire    [DOWNLINKCOUNT-1:0]     ic_rx_reset_by_tx;


//uart
wire           command_fifo_read,command_fifo_empty;
wire  [159:0]  command;    
wire           uart_data_write;
wire           uart_data_write_sca;
wire  [7:0]    uart_data;
wire  [7:0]    uart_data_sca;
wire           uart_tx_ready;

wire  [7:0]    readback_uart_data;
wire           readback_uart_write;



wire           uplink_reset_uart;
wire           downlink_reset_uart;
wire  [3:0]    uplink_to_uart_mapping;
wire           downlink_reset_uart_freedrp;
wire           uplink_reset_uart_freedrp;

wire           global_rst_uart;
wire           bc_rst_uart;
wire           event_rst_uart;
wire           master_rst_uart;

wire           en_int_trigger;
wire           enable_hit;
wire           hit_inv;
wire           hit_single;
wire  [11:0]   hit_width;
wire  [23:0]   hit_interval;

wire  [19:0]   correct_counter_th;
wire           enable_K28_1;
wire           enable_320M;
wire  [7:0]    trigger_deadtime;
wire  [11:0]   match_window;
wire  [11:0]   search_margin;
wire  [11:0]   bc_offset;
wire  [11:0]   reject_offset;
wire  [11:0]   rollover;
wire  [11:0]   coarse_count_offset;
wire           en_relative_trig_data;
wire           enable_matching;
wire           trigger_redge;
wire  [7:0]    trigger_latency;




// this part needs to be modified if UPLINKCOUNT changes
// All sca data are multiplexed by sca_sendback
assign uart_data_write = (uplink_to_uart_mapping<UPLINKCOUNT)  ?  (~ic_rx_fifo_empty[uplink_to_uart_mapping]):
                         (uplink_to_uart_mapping<UPLINKCOUNT+1)?  uart_data_write_sca:
                         readback_uart_write;

assign uart_data = (uplink_to_uart_mapping<UPLINKCOUNT)  ?  data_from_gbtic[uplink_to_uart_mapping]:
                   (uplink_to_uart_mapping<UPLINKCOUNT+1)?  uart_data_sca:
                   readback_uart_data;



// assign uart_data_write = uplink_to_uart_mapping[0]?(~ic_rx_fifo_empty[0]):
//                          uplink_to_uart_mapping[1]?(~ic_rx_fifo_empty[1]):
//                          uplink_to_uart_mapping[2]?uart_data_write_sca:1'b0;
// assign uart_data = uplink_to_uart_mapping[0] ? data_from_gbtic[0] : 
//                    uplink_to_uart_mapping[1] ? data_from_gbtic[1] : 
//                    uplink_to_uart_mapping[2] ? uart_data_sca : 'b0 ;






//Ethernet
wire sys_clk_160;
// wire clk_ethernet;
wire tx_axis_clk;
wire [7:0] tx_axis_fifo_tdata;
wire tx_axis_fifo_tvalid;
wire tx_axis_fifo_tready;
wire tx_axis_fifo_tlast;
wire [7:0] uplink_320m [DOWNLINKCOUNT-1:0][27:0];

wire data_back_write;
wire [159:0] data_back;
wire [UPLINKCOUNT*10-1:0] locked_dline1;
wire [UPLINKCOUNT*10-1:0] locked_dline0;


// -- Clocks



sys_clk sys_clk_inst
(
    // Clock out ports
    .mgt_freedrpclk (mgt_freedrpclk),     // free running clock required by GTX drp

    // Status and control signals
    .locked         (),       // output locked
    // Clock in ports
    .clk_in1_p      (USER_CLOCK_P),    // input clk_in1_p
    .clk_in1_n      (USER_CLOCK_N)    // input clk_in1_n
);


    
// -- MGT(GTX) reference clock:
// ----------------------------   
// -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
// --          * The MGT reference clock frequency must be 320MHz for the latency-optimized GBT Bank. 

// IBUFDS_GTE3: Gigabit Transceiver Buffer
//              UltraScale
IBUFDS_GTE3 #(
   .REFCLK_EN_TX_PATH(1'b0),   // Refer to Transceiver User Guide.
   .REFCLK_HROW_CK_SEL(2'b00), // Refer to Transceiver User Guide.
   .REFCLK_ICNTL_RX(2'b00)     // Refer to Transceiver User Guide.
)
smaMgtRefClkIbufdsGtxe2 (
   .O(mgtRefClk_from_smaMgtRefClkbuf),         // 1-bit output: Refer to Transceiver User Guide.
   .ODIV2(), // 1-bit output: Refer to Transceiver User Guide.
   .CEB(1'b0),     // 1-bit input: Refer to Transceiver User Guide.
   .I(SMA_MGT_REFCLK_P),         // 1-bit input: Refer to Transceiver User Guide.
   .IB(SMA_MGT_REFCLK_N)        // 1-bit input: Refer to Transceiver User Guide.
);


            
  // -- In this example design, the 40MHz clock used for the user logic is derived from a division of the Tx user clock of the MGT
  // -- It should be noted, that in realistic cases, this clock typically comes from an external PLL (sync. to the MGT Tx reference clock)
  
BUFGCE_DIV #(
   .BUFGCE_DIVIDE(8),              // 1-8
   // Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
   .IS_CE_INVERTED(1'b0),          // Optional inversion for CE
   .IS_CLR_INVERTED(1'b0),         // Optional inversion for CLR
   .IS_I_INVERTED(1'b0),           // Optional inversion for I
   .SIM_DEVICE("ULTRASCALE"))  // ULTRASCALE, ULTRASCALE_PLUS
BUFGCE_DIV_inst (
   .O(lpgbtfpga_clk40),     // 1-bit output: Buffer
   .CE(1'b1),   // 1-bit input: Buffer enable
   .CLR(1'b0), // 1-bit input: Asynchronous clear
   .I(lpgbtfpga_mgttxclk)      // 1-bit input: Buffer 320M clk_mgtRxClk_o
);


OBUFDS #(
   .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
   .SLEW("SLOW")           // Specify the output slew rate
) OBUFDS_inst (
   .O(USER_SMA_GPIO_P),     // Diff_p output (connect directly to top-level port)
   .OB(USER_SMA_GPIO_N),   // Diff_n output (connect directly to top-level port)
   .I(lpgbtfpga_clk40)      // Buffer input
);

IBUFDS 
IBUFDS_inst (
   .O(trigger_SMA),   // 1-bit output: Buffer output
   .I(TRIGGER0_P),   // 1-bit input: Diff_p buffer input (connect directly to top-level port)
   .IB(TRIGGER0_N)  // 1-bit input: Diff_n buffer input (connect directly to top-level port)
);

IBUFDS 
IBUFDS_trigger_out (
   .O(trigger_out),   // 1-bit output: Buffer output
   .I(TRIGGER1_P),   // 1-bit input: Diff_p buffer input (connect directly to top-level port)
   .IB(TRIGGER1_N)  // 1-bit input: Diff_n buffer input (connect directly to top-level port)
);

// OBUF #(
//    .DRIVE(12),   // Specify the output drive strength
//    .IOSTANDARD("LVCMOS18"), // Specify the output I/O standard
//    .SLEW("FAST") // Specify the output slew rate
// ) OBUF_inst (
//    .O(USER_SMA_GPIO_P),     // Buffer output (connect directly to top-level port)
//    .I(trigger_out)      // Buffer input
// );


genvar j;
for (j = 0; j < UPLINKCOUNT; j++) begin : uplink
    

    ic_rx #(
        .g_FIFO_DEPTH(453)
        )
    ic_rx_inst(
        .rx_clk_i   (lpgbtfpga_clk40),
        .rx_clk_en  (1'b1),
        .reset_i    (sc_reset_uart|ic_rx_reset_by_tx[j/2]),
        .rx_empty_o (ic_rx_fifo_empty[j]),         //--! Rx FIFO is empty (no reply from ic)
        .rd_clk_i   (lpgbtfpga_clk40),
        .rd_i       (uart_tx_ready),       //     --! Request a read operation
        .data_o     (data_from_gbtic[j]),  //     --! Data from the FIFO
        .rx_data_i  (lpgbtfpga_uplinkIcData[j])     //            --! (RX) Array of bits to be mapped to the RX GBT-Frame (bits 83/84)
    );

    vio_uplink vio_uplink_inst (
        .clk(mgt_freedrpclk),                // input wire clk
        .probe_in0(lpgbtfpga_uplinkrdy[j]),    // input wire [0 : 0] probe_in0
        .probe_in1(uperror_detected[j]),    // input wire [27 : 0] probe_in1
        .probe_in2(upLinkFECCorrectedLatched[j]),    // input wire [0 : 0] probe_in2
        .probe_in3(uplinkPhase[j]),    // input wire [2 : 0] probe_in3

        .probe_out0(lpgbtfpga_uplinkrst[j]),  // output wire [0 : 0] probe_out0
        .probe_out1(upLinkInterleaverBypass[j]),  // output wire [0 : 0] probe_out1
        .probe_out2(upLinkFecBypass[j]),  // output wire [0 : 0] probe_out2
        .probe_out3(upLinkScramblerBypass[j]),  // output wire [0 : 0] probe_out3
        .probe_out4(reset_upchecker[j]),  // output wire [0 : 0] probe_out4
        .probe_out5(upelink_config[j]),  // output wire [53 : 0] probe_out5
        .probe_out6(lpgbtfpga_mgt_rxpolarity[j]),  // output wire [0 : 0] probe_out6
        .probe_out7(upLinkFECCorrectedClear[j]),  // output wire [0 : 0] probe_out7
        .probe_out8(uplinkPhaseCalib[j]),  // output wire [2 : 0] probe_out8
        .probe_out9(uplinkPhaseForce[j])  // output wire [0 : 0] probe_out9
    );
end

genvar i,k,m;
for (i = 0; i < DOWNLINKCOUNT; i++) begin : downlink

    assign ic_rx_reset_by_tx[i] = ic_rd_start_uart[i]|ic_fifo_wr_uart[i];
    
    ila_ic_downlink ila_ic_downlink_inst (
        .clk(lpgbtfpga_clk40), // input wire clk

        .probe0(ic_GBTx_address_uart[i]), // input wire [7:0]  probe0  
        .probe1(ic_Register_addr_uart[i]), // input wire [15:0]  probe1 
        .probe2(ic_nb_to_be_read_uart[i]), // input wire [7:0]  probe2 
        .probe3(ic_wr_start_uart[i]), // input wire [0:0]  probe3 
        .probe4(ic_rd_start_uart[i]), // input wire [0:0]  probe4 
        .probe5(ic_wfifo_data_uart[i]), // input wire [7:0]  probe5 
        .probe6(ic_fifo_wr_uart[i]), // input wire [0:0]  probe6 
        .probe7(ic_rx_reset_by_tx[i]) // input wire [0:0]  probe7
    );

    sca_mapping #(
        .SCA_PER_DOWNLINK(SCA_PER_DOWNLINK)
    ) inst_sca_mapping (
        .sca_line_tx      (sca_line_tx[i]),
        .ttc_ctrl         (ttc_ctrl),
        .uplinkUserData   (lpgbtfpga_uplinkUserData[2*i]),
        .sca_line_rx      (sca_line_rx[i]),
        .downlinkUserData (lpgbtfpga_downlinkUserData[i])
    );

    vio_downlink vio_downlink_inst (
        .clk(mgt_freedrpclk),                  // input wire clk
        .probe_in0(lpgbtfpga_downlinkrdy[i]),      // input wire [0 : 0] probe_in0
        // .probe_in1(downlink_gen_rdy[i]),      // input wire [15 : 0] probe_in1
        .probe_in1(downlinkPhase[i]),      // input wire [9 : 0] probe_in1
        .probe_in2(lpgbtfpga_mgt_txaligned[i]),      // input wire [0 : 0] probe_in2
        .probe_in3(lpgbtfpga_mgt_txpiphase[i]),      // input wire [6 : 0] probe_in3
        

        .probe_out0(lpgbtfpga_downlinkrst[i]),    // output wire [0 : 0] probe_out0
        .probe_out1(downLinkBypassInterleaver[i]),    // output wire [0 : 0] probe_out1
        .probe_out2(downLinkBypassFECEncoder[i]),    // output wire [0 : 0] probe_out2
        .probe_out3(downLinkBypassScrambler[i]),    // output wire [0 : 0] probe_out3
        .probe_out4(lpgbtfpga_mgt_txpicalib[i]),    // output wire [6 : 0] probe_out4
        .probe_out5(lpgbtfpga_mgt_txcaliben[i]),    // output wire [0 : 0] probe_out5

        .probe_out6(lpgbtfpga_mgt_txpolarity[i]),  // output wire [0 : 0] probe_out6
        .probe_out7(downlinkPhaseCalib[i]),  // output wire [9 : 0] probe_out7
        .probe_out8(downlinkPhaseForce[i])  // output wire [0 : 0] probe_out8    
    );

    ic_tx #(
        .g_FIFO_DEPTH      (453),  //Defines the depth of the FIFO used to handle the Internal control (Max. number of words/bytes can be read/write from/to a GBTx)
        .g_LPGBT_VERS      (1'b1)) //Select lpGBT version ('0': 0, '1': 1)
    ic_tx_inst(
        .tx_clk_i          (lpgbtfpga_clk40),
        .tx_clk_en         (1'b1),
        .reset_i           (sc_reset_uart),    //Reset all of the TX processes
        .tx_ready_o        (ic_tx_ready[i]),  //IC core ready for a transaction
        .GBTx_address_i    (ic_GBTx_address_uart[i]),   //I2C address of the GBTx
        .Register_addr_i   (ic_Register_addr_uart[i]),  //Address of the first register to be accessed
        .nb_to_be_read_i   ({8'b0,ic_nb_to_be_read_uart[i]}),  //Number of words/bytes to be read (only for read transactions)
        .parity_err_mask_i ('b0),
        .wr_clk_i          (lpgbtfpga_clk40),  //IC tx Fifo's writing clock
        .wr_i              (ic_fifo_wr_uart[i]), //Request a write operation into the internal FIFO (Data to GBTx)
        .data_i            (ic_wfifo_data_uart[i]),//Data to be written into the internal FIFO
        .start_read        (ic_rd_start_uart[i]), //Request a write config. to the GBTx
        .start_write       (ic_wr_start_uart[i]), //Request a read config. to the GBTx
        .tx_data_o         (lpgbtfpga_downlinkIcData[i])   //(TX) Array of bits to be mapped to the TX GBT-Frame (bits 83/84)
    );


    sca_top #(
        .g_SCA_COUNT(SCA_PER_DOWNLINK)
    )
    sca_inst(
        .tx_clk_i           (lpgbtfpga_clk40),
        .tx_clk_en          (1'b1),
        .rx_clk_i           (lpgbtfpga_clk40),
        .rx_clk_en          (1'b1),
        .rx_reset_i         (sc_reset_uart),
        .tx_reset_i         (sc_reset_uart),
        .enable_i           (sca_enable_array_uart[i]), //Enable flag to select SCAs
        .start_reset_cmd_i  (sca_start_reset_uart[i]),  //Send a reset command to the enabled SCAs
        .start_connect_cmd_i(sca_start_connect_uart[i]), //Send a connect command to the enabled SCAs
        .start_command_i    (sca_start_command_uart[i]), //Send the command set in input to the enabled SCAs
        .inject_crc_error   (1'b0), //Emulate a CRC error
        .tx_address_i       (sca_tx_address_uart[i]),
        .tx_transid_i       (sca_tx_transID_uart[i]),
        .tx_channel_i       (sca_tx_channel_uart[i]),
        .tx_len_i           (8'h4), //fixed len, 4 bytes
        .tx_command_i       (sca_tx_command_uart[i]),
        .tx_data_i          (sca_tx_data_uart[i]),
        .rx_received_o      (sca_rx_done[i]),  //Reply received flag (pulse)
        .rx_address_o       (sca_rx_addr[i]),
        .rx_control_o       (),
        .rx_transid_o       (sca_rx_trid[i]),
        .rx_channel_o       (sca_rx_ch[i]),
        .rx_len_o           (sca_rx_len[i]),
        .rx_error_o         (sca_rx_err[i]),
        .rx_data_o          (sca_rx_data[i]),
        .tx_data_o          (sca_line_tx[i]),  // downlink -- txData_to_gbtExmplDsgn(81 downto 80), (TX) Array of bits to be mapped to the TX GBT-Frame
        .rx_data_i          (sca_line_rx[i])  //uplink  -- rxData_from_gbtExmplDsgn(81 downto 80),  (RX) Array of bits to be mapped to the RX GBT-Frame
    );
end


    //     // -- Data pattern generator / checker (PRBS7)
    // lpgbtfpga_patterngen lpgbtfpga_patterngen_inst(
    //     // --clk40Mhz_Tx_i      : in  std_logic;
    //     .clk320DnLink_i(lpgbtfpga_clk40),
    //     .clkEnDnLink_i(1'b1),
    //     .generator_rst_i(generator_rst[i]),

    //     // -- Group configurations:
    //     // --    "11": 320Mbps
    //     // --    "10": 160Mbps
    //     // --    "01": 80Mbps
    //     // --    "00": Fixed pattern
    //     .config_group0_i           (downconfig_g0[i]),
    //     .config_group1_i           (downconfig_g1[i]),
    //     .config_group2_i           (downconfig_g2[i]),
    //     .config_group3_i           (downconfig_g3[i]),
    //     .downlink_o                (lpgbtfpga_downlinkUserData_fromgen[i]),
    //     .fixed_pattern_i           ('h12345678),
    //     .eport_gen_rdy_o           (downlink_gen_rdy[i])
    // );

sca_sendback # (
    .DOWNLINKCOUNT(DOWNLINKCOUNT),
    .SCA_PER_DOWNLINK(SCA_PER_DOWNLINK)) 
inst_sca_sendback
(
    .clk                 (lpgbtfpga_clk40),
    .rst                 (reset_upchecker[0]),
    .rx_reply_received_i (sca_rx_done),
    .rx_address_i        (sca_rx_addr),
    .rx_transID_i        (sca_rx_trid),
    .rx_channel_i        (sca_rx_ch),
    .rx_len_i            (sca_rx_len),
    .rx_error_i          (sca_rx_err),
    .rx_data_i           (sca_rx_data),
    .uart_tx_ready_i     (uart_tx_ready),
    .uart_data_o         (uart_data_sca),
    .uart_data_write_o   (uart_data_write_sca)
);


xlx_ku_mgt_ip_reset_synchronizer uplink_uart_rst_sync(
 .clk_in(mgt_freedrpclk),
 .rst_in(uplink_reset_uart),
 .rst_out(uplink_reset_uart_freedrp)
);

xlx_ku_mgt_ip_reset_synchronizer downlink_uart_rst_sync(
 .clk_in(mgt_freedrpclk),
 .rst_in(downlink_reset_uart),
 .rst_out(downlink_reset_uart_freedrp)
);

wire [159:0] command_r;
wire MiniDAQ_reg_write;
wire MiniDAQ_reg_rst;
wire MiniDAQ_reg_read;



command_resolve_multi # (
    .DOWNLINKCOUNT(DOWNLINKCOUNT),
    .SCA_PER_DOWNLINK(SCA_PER_DOWNLINK))
inst_command_resolve_multi
(
    .clk                      (lpgbtfpga_clk40),
    .rst                      (reset_upchecker[0]),
    .command_i                (command),
    .command_fifo_empty_i     (command_fifo_empty),
    .command_fifo_read_o      (command_fifo_read),

    .uplink_to_uart_mapping_i (uplink_to_uart_mapping),

    .command_r_o              (command_r),
    .MiniDAQ_reg_write_o      (MiniDAQ_reg_write),
    .MiniDAQ_reg_rst_o        (MiniDAQ_reg_rst),
    .MiniDAQ_reg_read_o       (MiniDAQ_reg_read),
    
    .ic_GBTx_address_o        (ic_GBTx_address_uart),
    .ic_Register_addr_o       (ic_Register_addr_uart),
    .ic_nb_to_be_read_o       (ic_nb_to_be_read_uart),
    .ic_wr_start_o            (ic_wr_start_uart),
    .ic_rd_start_o            (ic_rd_start_uart),
    .ic_wfifo_data_o          (ic_wfifo_data_uart),
    .ic_fifo_wr_o             (ic_fifo_wr_uart),

    .sca_start_reset_o        (sca_start_reset_uart),
    .sca_start_connect_o      (sca_start_connect_uart),
    .sca_start_command_o      (sca_start_command_uart),
    .sca_enable_array_o       (sca_enable_array_uart),
    
    .sca_tx_address_o         (sca_tx_address_uart),
    .sca_tx_transID_o         (sca_tx_transID_uart),
    .sca_tx_channel_o         (sca_tx_channel_uart),
    .sca_tx_len_o             (sca_tx_len_uart),
    .sca_tx_command_o         (sca_tx_command_uart),
    .sca_tx_data_o            (sca_tx_data_uart)
);

MiniDAQ_reg #(
      .UPLINKCOUNT(UPLINKCOUNT),
      .DOWNLINKCOUNT(DOWNLINKCOUNT)
   ) inst_MiniDAQ_reg (
      .clk                      (lpgbtfpga_clk40),
      .rst                      (rst),
      .MiniDAQ_reg_rst          (MiniDAQ_reg_rst),
      .command_r                (command_r),
      .MiniDAQ_reg_write_i      (MiniDAQ_reg_write),
      .MiniDAQ_reg_read_i       (MiniDAQ_reg_read),

      .locked_dline0            (locked_dline0),
      .locked_dline1            (locked_dline1),
      .lpgbtfpga_uplinkrdy      (lpgbtfpga_uplinkrdy),
      .lpgbtfpga_downlinkrdy    (lpgbtfpga_downlinkrdy),

      .data_back_write_o        (data_back_write),
      .data_back_o              (data_back),

      .sc_reset_o               (sc_reset_uart),
      .global_rst_uart_o        (global_rst_uart),
      .bc_rst_uart_o            (bc_rst_uart),
      .event_rst_uart_o         (event_rst_uart),
      .master_rst_uart_o        (master_rst_uart),
      .mezz_TRST_o              (mezz_TRST),
      .uplink_to_uart_mapping_o (uplink_to_uart_mapping),
      .uplink_reset_uart_o      (uplink_reset_uart),
      .downlink_reset_uart_o    (downlink_reset_uart),

      .en_int_trigger_o         (en_int_trigger),
      .enable_hit_o             (enable_hit),
      .hit_inv_o                (hit_inv),
      .hit_single_o             (hit_single),
      .hit_width_o              (hit_width),
      .hit_interval_o           (hit_interval),

      .correct_counter_th_o     (correct_counter_th),
      .enable_K28_1_o           (enable_K28_1),
      .enable_320M_o            (enable_320M),
      .trigger_deadtime_o       (trigger_deadtime),
      .match_window_o           (match_window),
      .search_margin_o          (search_margin),
      .bc_offset_o              (bc_offset),
      .reject_offset_o          (reject_offset),
      .rollover_o               (rollover),
      .coarse_count_offset_o    (coarse_count_offset),
      .en_relative_trig_data_o  (en_relative_trig_data),
      .enable_matching_o        (enable_matching),
      .trigger_redge_o          (trigger_redge),
      .trigger_latency_o        (trigger_latency)
   );




UART_interface UART_interface_inst
(
    .clk40                (lpgbtfpga_clk40),
    .reset                (reset_upchecker[0]),
    .rxd_i                (uart_rxd),
    .txd_o                (uart_txd),

    .databack_8bit        (readback_uart_data),
    .databack_8bit_write  (readback_uart_write),

    .command_fifo_rd_en_i (command_fifo_read),
    .command_o            (command),
    .command_fifo_empty_o (command_fifo_empty),
    .data_back            (data_back),
    .data_back_fifo_wr_en (data_back_write),

    .tx_start_i           (uart_data_write),
    .tx_data_i            (uart_data),
    .tx_ready_o           (uart_tx_ready)

);



lpgbtFpga_10g24 #(
    .FEC(FEC5),
    .UPLINKCOUNT(UPLINKCOUNT),
    .DOWNLINKCOUNT(DOWNLINKCOUNT))
lpgbtFpga_top_inst (
//Clocks
    .downlinkClk_i               (lpgbtfpga_clk40), //all downlink data starts from this clock, and will be
                                                    //synced to individual downlink by cdc_tx
    .uplinkClk_i                 (lpgbtfpga_clk40), //all data from different uplinks are synced to 
                                                    //this clock by cdc_rx
    .downlinkRst_i               (lpgbtfpga_downlinkrst[0]|downlink_reset_uart_freedrp),
    .uplinkRst_i                 (lpgbtfpga_uplinkrst[0]|uplink_reset_uart_freedrp),
//Downlink
    .downlinkUserData_i          (lpgbtfpga_downlinkUserData),
    .downlinkEcData_i            (lpgbtfpga_downlinkIcData),  //IC data are copied to EC line, distinguished by GBT address
    .downlinkIcData_i            (lpgbtfpga_downlinkIcData),
    .downLinkBypassInterleaver_i (downLinkBypassInterleaver),
    .downLinkBypassFECEncoder_i  (downLinkBypassFECEncoder),
    .downLinkBypassScrambler_i   (downLinkBypassScrambler),
    .downlinkReady_o             (lpgbtfpga_downlinkrdy),
    .downlinkPhase_o             (downlinkPhase),
    .downlinkPhaseCalib_i        (downlinkPhaseCalib),
    .downlinkPhaseForce_i        (downlinkPhaseForce),
//uplink
    .uplinkUserData_o            (lpgbtfpga_uplinkUserData),
    .uplinkEcData_o              (lpgbtfpga_uplinkEcData),
    .uplinkIcData_o              (lpgbtfpga_uplinkIcData),
    .uplinkBypassInterleaver_i   (upLinkInterleaverBypass),
    .uplinkBypassFECEncoder_i    (upLinkFecBypass),
    .uplinkBypassScrambler_i     (upLinkScramblerBypass),
    .uplinkFECCorrectedClear_i   (upLinkFECCorrectedClear),
    .uplinkFECCorrectedLatched_o (upLinkFECCorrectedLatched),
    .uplinkReady_o               (lpgbtfpga_uplinkrdy),
    .uplinkPhase_o               (uplinkPhase),
    .uplinkPhaseCalib_i          (uplinkPhaseCalib),
    .uplinkPhaseForce_i          (uplinkPhaseForce),
//MGT
    .mgt_rxn_i                   (SFP_RX_N),
    .mgt_rxp_i                   (SFP_RX_P),
    .mgt_txn_o                   (SFP_TX_N),
    .mgt_txp_o                   (SFP_TX_P),
    .clk_mgtrefclk_i             (mgtRefClk_from_smaMgtRefClkbuf),
    .clk_mgtfreedrpclk_i         (mgt_freedrpclk),
    // .clk_mgtRxClk_o              (lpgbtfpga_mgtrxclk),
    .clk_mgtRxClk_o              (),
    .clk_mgtTxClk_o              (lpgbtfpga_mgttxclk),
    .mgt_txpolarity_i            (lpgbtfpga_mgt_txpolarity),
    .mgt_rxpolarity_i            (lpgbtfpga_mgt_rxpolarity),
    .mgt_txcaliben_i             (lpgbtfpga_mgt_txcaliben),
    .mgt_txcalib_i               (lpgbtfpga_mgt_txpicalib),
    .mgt_txaligned_o             (lpgbtfpga_mgt_txaligned),
    .mgt_txphase_o               (lpgbtfpga_mgt_txpiphase)
);

 

// --    lpgbtfpga_patternchecker_inst: lpgbtfpga_patternchecker
// --        port map(
// --            reset_checker_i  => reset_upchecker_s,
// --            ser320_clk_i     => lpgbtfpga_clk40,
// --            ser320_clkEn_i   => '1',
    
// --            data_rate_i      => uplinkSelectDataRate_s,
    
// --            elink_config_i   => upelink_config_s,
    
// --            error_detected_o => uperror_detected_s,
    
// --            userDataUpLink_i => lpgbtfpga_uplinkUserData_s
// --        );


// assign encode_ttc    = 'b0;
// assign locked_dline0 = 'b0;
// assign locked_dline1 = 'b0;
// assign rgmii_txc     = 'b0;
// assign rgmii_txd     = 'b0;
// assign rgmii_tx_ctl  = 'b0;
// assign mdc           = 'b0;


tdc_decoder_top# (
    .UPLINKCOUNT(UPLINKCOUNT),
    .TDC_COUNT_SINGLE(10))
tdc_decoder_top_inst
(
    .clk_40                (lpgbtfpga_clk40),
    .clk_asyn              (mgt_freedrpclk),
    .rst_in                (reset_upchecker[0]|global_rst_uart),
    .userDataUpLink        (lpgbtfpga_uplinkUserData),
    .trigger_in            (trigger_SMA),
    // .error_detected_o    (error_detected_o),
    .encode_ttc            (ttc_ctrl),

    .locked_dline1         (locked_dline1),
    .locked_dline0         (locked_dline0),

    .bc_rst                (bc_rst_uart),
    .event_rst             (event_rst_uart),
    .master_rst            (master_rst_uart),

    .en_int_trigger        (en_int_trigger),
    .enable_hit            (enable_hit),
    .hit_inv               (hit_inv),
    .hit_single            (hit_single),
    .hit_width             (hit_width),
    .hit_interval          (hit_interval),
    
    .correct_counter_th    (correct_counter_th),
    .enable_K28_1          (enable_K28_1),
    .enable_320M           (enable_320M),
    .trigger_deadtime      (trigger_deadtime),
    .match_window          (match_window),
    .search_margin         (search_margin),
    .bc_offset             (bc_offset),
    .reject_offset         (reject_offset),
    .rollover              (rollover),
    .coarse_count_offset   (coarse_count_offset),
    .en_relative_trig_data (en_relative_trig_data),
    .enable_matching       (enable_matching),
    .trigger_redge         (trigger_redge),
    .trigger_latency       (trigger_latency),

    // .sys_clk_160         (sys_clk_160),
    .tx_axis_clk           (tx_axis_clk),
    .tx_axis_fifo_tdata    (tx_axis_fifo_tdata),
    .tx_axis_fifo_tvalid   (tx_axis_fifo_tvalid),
    .tx_axis_fifo_tready   (tx_axis_fifo_tready),
    .tx_axis_fifo_tlast    (tx_axis_fifo_tlast)
);


tri_mode_ethernet_mac_0_example_design ethernet_mac
(
    .glbl_rst            (reset_upchecker[0]),

    // //-- 125 MHz clock from MMCM
    // .clk_125             (clk_ethernet),
    .clk_in_p            (USER_CLK_320_P),
    .clk_in_n            (USER_CLK_320_N),
    .gtx_clk_bufg_out    (tx_axis_clk),
    .phy_resetn          (phy_resetn),

    //-- RGMII terface
    .rgmii_txd           (rgmii_txd),
    .rgmii_tx_ctl        (rgmii_tx_ctl),
    .rgmii_txc           (rgmii_txc),
    .rgmii_rxd           (rgmii_rxd),
    .rgmii_rx_ctl        (rgmii_rx_ctl),
    .rgmii_rxc           (rgmii_rxc),


    //-- MDIO terface
    .mdio                (mdio),
    .mdc                 (mdc),

    //-- USER side TX AXI-S terface       
    .tx_axis_fifo_tdata  (tx_axis_fifo_tdata),
    .tx_axis_fifo_tvalid (tx_axis_fifo_tvalid),
    .tx_axis_fifo_tready (tx_axis_fifo_tready),
    .tx_axis_fifo_tlast  (tx_axis_fifo_tlast)
);


endmodule
