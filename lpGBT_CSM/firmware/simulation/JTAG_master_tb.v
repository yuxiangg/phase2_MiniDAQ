/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  

// Author : Yuxiang Guo  gyuxiang@umich.edu
// File   : JTAG_master_tb.v
// Create : 2024-11-04 14:49:19
// Revise : 2024-11-04 15:03:39
// Editor : sublime text4, tab size (4)
// Description: 
//
// -----------------------------------------------------------------------------


`timescale 1ns / 1ps


module JTAG_master_tb;

    // Parameters
    parameter DATA_BIT = 8;
    parameter INSTR_LENGTH = 5;
    parameter DEVICE_BIT = 5;

    // Inputs
    reg clk;
    reg rst;
    reg tdo;
    reg start_action_tdc;
    reg start_action_asd;
    reg [11:0] clk_div;
    reg [255:0] JTAG_bits;
    reg [7:0] bit_length;
    reg [INSTR_LENGTH-1:0] JTAG_inst;
    reg [4:0] device_count;

    // Outputs
    wire tck;
    wire tms;
    wire tdi;
    wire [4095:0] JTAG_readback;
    wire JTAG_busy;
    wire tdi_tdo_equal;

    // Instantiate the JTAG_master
    JTAG_master #(
        .DATA_BIT(DATA_BIT),
        .INSTR_LENGTH(INSTR_LENGTH),
        .DEVICE_BIT(DEVICE_BIT)
    ) uut (
        .clk(clk),
        .rst(rst),
        .tck(tck),
        .tms(tms),
        .tdi(tdi),
        .tdo(tdo),
        .start_action_tdc(start_action_tdc),
        .start_action_asd(start_action_asd),
        .clk_div(clk_div),
        .JTAG_bits(JTAG_bits),
        .bit_length(bit_length),
        .JTAG_inst(JTAG_inst),
        .device_count(device_count),
        .JTAG_readback(JTAG_readback),
        .JTAG_busy(JTAG_busy),
        .tdi_tdo_equal(tdi_tdo_equal)
    );

    // Clock generation
    initial begin
        clk = 0;
        forever #25 clk = ~clk; // 20 MHz clock
    end

    // Stimulus generation
    initial begin
        // Initialize Inputs
        rst = 1;
        tdo = 0;
        start_action_tdc = 0;
        start_action_asd = 0;
        clk_div = 12'h000; // Example clock division
        JTAG_bits = 9'b101001000;
        bit_length = 8'h9; // Example bit length
        JTAG_inst = 5'b11001; // Example instruction
        device_count = 5'h1; // Example device count
        
        // Reset the DUT
        #100 rst = 0;
        
        // Start a JTAG action using `start_action_tdc`
        #100 start_action_tdc = 1;
        #100 start_action_tdc = 0;
        

       
        // Wait for the JTAG master to finish
        wait(JTAG_busy == 1);
        wait(JTAG_busy == 0);
        
        // Monitor readback and check for tdi_tdo_equal flag
        #1000;
        
        // End the simulation
        $finish;
    end

    // Monitor the output signals
    initial begin
        $monitor("Time: %0t | tck: %b | tms: %b | tdi: %b | JTAG_busy: %b | tdi_tdo_equal: %b", 
                 $time, tck, tms, tdi, JTAG_busy, tdi_tdo_equal);
    end

endmodule
