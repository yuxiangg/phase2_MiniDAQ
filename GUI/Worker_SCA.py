from PyQt5.QtCore import *
import subprocess
import time
import sys
import pandas as pd
from TDC_config_low_level_function import *
import csv


class Worker_SCA(QObject):
    finished = pyqtSignal()
    message_out = pyqtSignal(str)
    # update_chip_total_no = pyqtSignal()
    # update_log_file_name = pyqtSignal(str)
    # plot_bin = pyqtSignal()
    # single_chip_test_done = pyqtSignal()

    def __init__(self,sca_tab):
        super(Worker_SCA, self).__init__()
        self.ser = sca_tab.ser
        self.sca_tab = sca_tab
        try:
            self.df = pd.read_csv("sca_mapping.csv",header=[0],index_col=[0])
        except Exception as e:
            print("Error loading sca_mapping.csv")
            print(e)
        else:
            print("sca_mapping.csv loaded successfully.")
            # print(self.df)
            # print(self.df['0'])
            # print(self.df['0']['SCA'])
            # print(self.df['0']['AVDD'])


    def run(self):
        tdc_list = self.sca_tab.mezz_enable
        mux_list = ['AVDD','AVDD_GND','DVDD','DVDD_GND','TEMP','TEMP_GND']
        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        
        for i in range(len(self.sca_tab.mezz_enable)):
            if i==0 or i==20:
                SCA_connect(self.ser,sca_enable=0b1111,which_csm=i//20,sca_address=0x00,verbose=1)
                SCA_start_reset(self.ser,sca_enable=0b1111,which_csm=i//20,sca_address=0x00,verbose=1)
                SCA_tx_rx_reset(self.ser,sca_enable=0b1111,which_csm=i//20,sca_address=0x00,verbose=1)
                SCA_config(self.ser,sca_enable=0b1111,which_csm=i//20,sca_address=0x00,transID=self.sca_tab.get_transID(),channel=SCA_share.CR_CHANNEL,datalen=4,command=SCA_share.CTRL_W_CRD,data=SCA_share.ENADC_DATA)

            if self.sca_tab.mezz_enable[i]=='1':
                for j in range(len(mux_list)):
                    try:
                        SCA_config(self.ser,sca_enable=2**self.df[str(i%20)]['SCA'],which_csm=i//20,sca_address=0x00,transID=self.sca_tab.get_transID(),channel=SCA_share.ADC_CHANNEL,datalen=4,command=SCA_share.ADC_W_MUX,data=self.df[str(i%20)][mux_list[j]])
                        [SCA_channel,error,adc]=SCA_config(self.ser,sca_enable=2**self.df[str(i%20)]['SCA'],which_csm=i//20,sca_address=0x00,transID=self.sca_tab.get_transID(),channel=SCA_share.ADC_CHANNEL,datalen=4,command=SCA_share.ADC_GO,data=SCA_share.ADC_GO_DATA)
                    except Exception as e:
                        self.message_out.emit(('TDC'+str(i%20)+' '+mux_list[j]+' read back error'));
                        self.message_out.emit(str(e))

                    else:
                        voltage = adc/4096.0
                        message = 'TDC'+str(i%20)+' '+mux_list[j]+'= '+"{:.4f}".format(voltage)[:8]
                        self.message_out.emit(message)
                        self.sca_tab.cell_list[i//20][i%20].value_list[j]=voltage
                        if j==1 or j==3:

                            # 1k resistor in mezz, 4.7k + 1k resistor in CSM, probe point on 1K
                            cal = self.sca_tab.cell_list[i//20][i%20].value_list[j-1]*6.7-\
                                self.sca_tab.cell_list[i//20][i%20].value_list[j]
                            self.sca_tab.cell_list[i//20][i%20].label_list[j//2].setText("{:.4f}".format(cal)[:8])
                        elif j==5:
                            # 1k resistor in mezz, 1k + 1k resistor in CSM, probe point on 1K
                            cal = self.sca_tab.cell_list[i//20][i%20].value_list[j-1]*3-\
                                self.sca_tab.cell_list[i//20][i%20].value_list[j]
                            temp = (cal-0.5)*100
                            self.sca_tab.cell_list[i//20][i%20].label_list[j//2].setText("{:.4f}".format(temp)[:5])
        self.finished.emit()
