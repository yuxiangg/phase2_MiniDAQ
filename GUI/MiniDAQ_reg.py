class MiniDAQ_reg(object):
    def __init__(self):
        self.register_table = [
            [1, 'uart_mapping', 4, 0],
            [2, 'uplink_rst', 1, 0],
            [3, 'down_rst', 1, 0],
            [4, 'sca_reset', 1, 0],
            [5, 'global_rst', 1, 0],
            [6, 'bc_rst', 1, 0],
            [7, 'event_rst', 1, 0],
            [8, 'master_rst', 1, 0],
            [9, 'TRST', 1, 1],
            [10, 'en_int_trigger', 1, 0],
            [11, 'enable_hit', 1, 0],
            [12, 'hit_inv', 1, 1],
            [13, 'hit_single', 1, 0],
            [14, 'hit_width', 12, 4],
            [15, 'hit_interval', 24, 40000],
            [16, 'correct_counter_th', 20, 1000000],
            [17, 'enable_K28_1', 1, 1],
            [18, 'enable_320M', 1, 1],
            [19, 'trigger_deadtime', 8, 10],
            [20, 'match_window', 12, 60],
            [21, 'search_margin', 12, 100],
            [22, 'bc_offset', 12, 0],
            [23, 'reject_offset', 12, 300],
            [24, 'rollover', 12, 4095],
            [25, 'coarse_count_offset', 12, 4070],
            [26, 'en_relative_trig_data', 1, 0],
            [27, 'enable_matching', 1, 1],
            [28, 'trigger_redge', 1, 0],
            [29, 'trigger_latency', 8, 100]
        ]

    def reset(self):
        """ Reset all registers to their default values """
        default_table = [
            [1, 'uart_mapping', 4, 0],
            [2, 'uplink_rst', 1, 0],
            [3, 'down_rst', 1, 0],
            [4, 'sca_reset', 1, 0],
            [5, 'global_rst', 1, 0],
            [6, 'bc_rst', 1, 0],
            [7, 'event_rst', 1, 0],
            [8, 'master_rst', 1, 0],
            [9, 'TRST', 1, 1],
            [10, 'en_int_trigger', 1, 0],
            [11, 'enable_hit', 1, 0],
            [12, 'hit_inv', 1, 1],
            [13, 'hit_single', 1, 0],
            [14, 'hit_width', 12, 4],
            [15, 'hit_interval', 24, 40000],
            [16, 'correct_counter_th', 20, 1000000],
            [17, 'enable_K28_1', 1, 1],
            [18, 'enable_320M', 1, 1],
            [19, 'trigger_deadtime', 8, 10],
            [20, 'match_window', 12, 60],
            [21, 'search_margin', 12, 100],
            [22, 'bc_offset', 12, 0],
            [23, 'reject_offset', 12, 300],
            [24, 'rollover', 12, 4095],
            [25, 'coarse_count_offset', 12, 4070],
            [26, 'en_relative_trig_data', 1, 0],
            [27, 'enable_matching', 1, 1],
            [28, 'trigger_redge', 1, 0],
            [29, 'trigger_latency', 8, 100]
        ]
        
        # Reset all registers
        for i in range(len(self.register_table)):
            self.register_table[i] = default_table[i]
