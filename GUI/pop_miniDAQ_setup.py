from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from functools import partial
from GUI_lowlevel import *
from TDC_config_low_level_function import *
from MiniDAQ_reg import *

class pop_miniDAQ_setup(QtWidgets.QDialog):
    def __init__(self, ser, miniDAQ_reg, parent=None):
        super().__init__(parent)
        self.ser = ser
        self.miniDAQ_reg = miniDAQ_reg
        self.setWindowTitle("Trigger Setup")
        
        self.setup_UI()

    def setup_UI(self):
        sectionLayout = QtWidgets.QVBoxLayout(self)
        buttonWindow = QtWidgets.QWidget()
        buttonWindow.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        self.gridLayout = QtWidgets.QGridLayout(buttonWindow)

        row = 0
        column = 0

        for index, name, bit_width, default in self.miniDAQ_reg.register_table:
            if index < 1:
                create_button(name, row, column, 1, partial(self.handle_button, index, name), layout=self.gridLayout)
                column += 1
            elif bit_width == 1:
                create_checkbox(name, row, column, 1, partial(self.handle_checkbox, index, name), setchecked=bool(default), layout=self.gridLayout)
                column += 1
            else:
                self.create_spinbox(name, row, column, 1, partial(self.handle_spinbox, index, name), initvalue=default, layout=self.gridLayout)
                column += 2
            
            if column >= 4:
                column = 0
                row += 1

        sectionLayout.addWidget(buttonWindow)

    


    def create_spinbox(self, label, row, column, width, slot_function, initvalue=0, layout=None):
        qlabel = QtWidgets.QLabel(label)
        layout.addWidget(qlabel, row, column, 1, width)

        if layout is None:
            raise ValueError("Layout must be provided for spinbox placement.")
        spinbox = QtWidgets.QSpinBox()
        spinbox.setMaximum(2**24-1)
        spinbox.setValue(initvalue)

        layout.addWidget(spinbox, row, column+1, 1, width)
        spinbox.valueChanged.connect(slot_function)
        return spinbox

    def handle_button(self, index, name):
        print(f"sending {name}")
        fpga_config(self.ser, index, [1])
        fpga_config(self.ser, index, [0])

    def handle_checkbox(self, index, name, state):
        value = 1 if state == Qt.Checked else 0
        print(f"{name} changed to {value}")
        self.miniDAQ_reg.register_table[index-1][3]=value
        fpga_config(self.ser, index, [value])

    def handle_spinbox(self, index, name):
        spinbox = self.sender()
        value = spinbox.value()
        print(f"{name} changed to {value}")
        self.miniDAQ_reg.register_table[index-1][3]=value
        fpga_config(self.ser, index, value_to_list(value))
