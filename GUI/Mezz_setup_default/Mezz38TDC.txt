enable_new_ttc,0
enable_master_reset_code,1
enable_direct_bunch_reset,0
enable_direct_event_reset,0
enable_direct_trigger,0
auto_roll_over,0
bypass_bcr_distribution,0
enable_trigger,0
channel_data_debug,0
enable_leading,0
enable_pair,1
enable_fake_hit,0
rising_is_leading,000000000000000000000000
channel_enable_r,111111111111111111111111
channel_enable_f,111111111111111111111111
TDC_ID,1111010101010101010
enable_trigger_timeout,0
enable_high_speed,1
enable_legacy,0
full_width_res,0
width_select,001
enable_8b10b,1
enable_insert,0
enable_error_packet,0
enable_TDC_ID,0
enable_error_notify,0
combine_time_out_config,0000101000
fake_hit_time_interval,000100000000
syn_packet_number,111111111111
roll_over,111111111111
coarse_count_offset,000000000000
bunch_offset,111110011100
event_offset,000000000000
match_window,000000011111
fine_sel,0011
lut0,00
lut1,01
lut2,10
lut3,01
lut4,11
lut5,00
lut6,10
lut7,10
lut8,00
lut9,00
luta,00
lutb,01
lutc,11
lutd,00
lute,11
lutf,00
rst_ePLL,0
reset_jtag_in,0
event_reset_jtag_in,0
chnl_fifo_overflow_clear,0
debug_port_select,0000
phase_clk160,01000
phase_clk320_0,0100
phase_clk320_1,0000
phase_clk320_2,0010
ePllRes,0010
ePllIcp,0100
ePllCap,10
instruction_error,0
CRC,00000000000000000000000000000000
ePll_lock,0
chnl_fifo_overflow,000000000000000000000000
IDCODE,00000000000000000000000000000000
