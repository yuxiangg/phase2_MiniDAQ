from PyQt5.QtCore import *
import subprocess
import time
import sys
import pandas as pd
from TDC_config_low_level_function import *
import csv


class Worker_CSM(QObject):
    finished = pyqtSignal()
    message_out = pyqtSignal(str)

    def __init__(self,csm_tab):
        super(Worker_CSM, self).__init__()
        self.ser = csm_tab.ser
        self.csm_tab = csm_tab



    def run(self):    
        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        for i in range(self.csm_tab.CSM_count):
            if(self.csm_tab.checkbox_list[i].isChecked()):
                SCA_connect(self.ser,sca_enable=0b1111,which_csm=i,sca_address=0x00,verbose=1)
                SCA_start_reset(self.ser,sca_enable=0b1111,which_csm=i,sca_address=0x00,verbose=1)
                SCA_tx_rx_reset(self.ser,sca_enable=0b1111,which_csm=i,sca_address=0x00,verbose=1)
                SCA_config(self.ser,sca_enable=0b1111,which_csm=i,sca_address=0x00,transID=self.csm_tab.get_transID(),channel=SCA_share.CR_CHANNEL,datalen=4,command=SCA_share.CTRL_W_CRD,data=SCA_share.ENADC_DATA,wait = 0.001)

                for j in range(6):
                    try:
                        SCA_config(self.ser,sca_enable=self.csm_tab.cell_list[i].SCA_mapping[j],which_csm=i,sca_address=0x00,transID=self.csm_tab.get_transID(),channel=SCA_share.ADC_CHANNEL,datalen=4,command=SCA_share.ADC_W_MUX,data=self.csm_tab.cell_list[i].SCA_ADC_input[j],wait = 0.001)
                        [SCA_channel,error,adc]=SCA_config(self.ser,sca_enable=self.csm_tab.cell_list[i].SCA_mapping[j],which_csm=i,sca_address=0x00,transID=self.csm_tab.get_transID(),channel=SCA_share.ADC_CHANNEL,datalen=4,command=SCA_share.ADC_GO,data=SCA_share.ADC_GO_DATA,wait = 0.001)
                    except Exception as e:
                        self.message_out.emit((f'CSM{i} {self.csm_tab.cell_list[i].SCA_temp_label[j]} read back error'));
                        self.message_out.emit(str(e))

                    else:
                        
                        if(j<2):
                            voltage=adc/4096.0*5.7
                        else:
                            voltage = 80-adc/4096*110
                        message = 'CSM'+str(i)+' '+self.csm_tab.cell_list[i].SCA_temp_label[j]+' '+"{:.4f}".format(voltage)[:8]
                        self.message_out.emit(message)
                        self.csm_tab.cell_list[i].SCA_temp_value_widget[j].setText("{:.4f}".format(voltage)[:8])

                # set which CSM
                for k in range(2):  # lpgbt master and slave
                    fpga_config(self.ser, 1, [i*2+k])
                    # SCA_tx_rx_reset(self.ser,sca_enable=0b1111,which_csm=i,sca_address=0x00,verbose=1)
                    lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.VREFCNTR.address, value_list=[128], which_csm=i, lpgbt_addr=self.csm_tab.cell_list[i].lpGBT_list[k].address)
                    for j in range(6):
                        try:
                            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ADCSELECT.address, value_list=[self.csm_tab.cell_list[i].lpGBT_list[k].lpGBT_monitor_index[j]*16+self.csm_tab.cell_list[i].lpGBT_list[k].lpGBT_monitor_index[6]], which_csm=i, lpgbt_addr=self.csm_tab.cell_list[i].lpGBT_list[k].address)
                            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ADCMON.address, value_list=[self.csm_tab.cell_list[i].lpGBT_list[k].lpGBT_monitor_ADCmon[j]], which_csm=i, lpgbt_addr=self.csm_tab.cell_list[i].lpGBT_list[k].address)
                            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ADCCONFIG.address, value_list=[0+4+0], which_csm=i, lpgbt_addr=self.csm_tab.cell_list[i].lpGBT_list[k].address) #reset start bit
                            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ADCCONFIG.address, value_list=[128+4+0], which_csm=i, lpgbt_addr=self.csm_tab.cell_list[i].lpGBT_list[k].address)
                            readback=lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ADCSTATUSH.address, value_list=[0,0], which_csm=i, lpgbt_addr=self.csm_tab.cell_list[i].lpGBT_list[k].address)
                            adcbusy = (readback[0]&LpgbtRegisterMapV1.ADCSTATUSH.ADCBUSY.bit_mask)>>LpgbtRegisterMapV1.ADCSTATUSH.ADCBUSY.offset
                            adcdone = (readback[0]&LpgbtRegisterMapV1.ADCSTATUSH.ADCDONE.bit_mask)>>LpgbtRegisterMapV1.ADCSTATUSH.ADCDONE.offset
                            adcvalue= (((readback[0]&LpgbtRegisterMapV1.ADCSTATUSH.ADCVALUE.bit_mask)>>LpgbtRegisterMapV1.ADCSTATUSH.ADCVALUE.offset)<<8)+((readback[1]&LpgbtRegisterMapV1.ADCSTATUSL.ADCVALUE.bit_mask)>>LpgbtRegisterMapV1.ADCSTATUSL.ADCVALUE.offset)
                            print(f'ADCbusy={adcbusy},ADCdone={adcdone},ADCvalue={adcvalue}')
                        except Exception as e:
                            # self.message_out.emit((f'CSM{i} {self.csm_tab.cell_list[i].SCA_temp_label[j]} read back error'));
                            self.message_out.emit(str(e))

                        else:
                            # (voltage*0.42-0.5)*2=(adcvalue-512)/1024*2  ## diff = -1 when adcvalue=0
                            voltage = ((adcvalue-512)/1024.0+0.5)/0.42
                            self.csm_tab.cell_list[i].lpGBT_list[k].lpGBT_monitor_value_widget[j].setText("{:.4f}".format(voltage)[:8])
        self.finished.emit()
