from PyQt5 import QtCore, QtGui, QtWidgets

from TDC_config_low_level_function import *

import time
import datetime
import pandas as pd
from lpgbt_register_map_v1 import LpgbtRegisterMapV1

from Worker_CSM import *

class lpGBT_cell(object):
    def __init__(self,address):
        self.address = address
        self.lpGBT_monitor_label = ['VSSA','VDDTX','VDDRX','VDD','VDDA','Temp','VREF']
        self.lpGBT_monitor_label_widget = [QtWidgets.QLabel(data) for data in self.lpGBT_monitor_label]
        self.lpGBT_monitor_index = [9,10,11,12,13,14,15]
        self.lpGBT_monitor_ADCmon = [0,8,4,16,1,32,0]
        self.lpGBT_monitor_value_widget = [QtWidgets.QLabel('') for data in self.lpGBT_monitor_label]


    def add_to_layout(self,layout,row,column,first_row=True):
        for i in range(len(self.lpGBT_monitor_label_widget)-1):
            if first_row:
                layout.addWidget(self.lpGBT_monitor_label_widget[i],   row,    column+i, 1, 1)
                layout.addWidget(self.lpGBT_monitor_value_widget[i],   row+1,  column+i, 1, 1)
            else:
                layout.addWidget(self.lpGBT_monitor_value_widget[i],   row,  column+i, 1, 1)

class CSM_cell(object):
    def __init__(self):
        self.SCA_temp_label = ['VDD2V5','VDD1V2','SCA0_Temp','SCA1_Temp','SCA2_Temp','SCA3_Temp','VTRX_Temp']
        self.SCA_mapping =[0b0100,0b1000,0b0001,0b0010,0b0100,0b1000]
        self.SCA_ADC_input = [SCA_share.CSM_2V5_SENSE,SCA_share.CSM_1V2_SENSE,SCA_share.SCA_INT_TEMP,SCA_share.SCA_INT_TEMP,SCA_share.SCA_INT_TEMP,SCA_share.SCA_INT_TEMP]
        self.SCA_temp_label_widget = [QtWidgets.QLabel(data) for data in self.SCA_temp_label]
        self.SCA_temp_value_widget = [QtWidgets.QLabel('') for data in self.SCA_temp_label]
        self.lpGBT_list = []
        self.lpGBT_list.append(lpGBT_cell(117))
        self.lpGBT_list.append(lpGBT_cell(115))


    def add_to_layout(self,layout,row,column):


        for i in range(len(self.SCA_temp_label_widget)):
            layout.addWidget(self.SCA_temp_label_widget[i],   row,    column+i+1, 1, 1)
            layout.addWidget(self.SCA_temp_value_widget[i],   row+1,  column+i+1, 1, 1)
        layout.addWidget(QtWidgets.QLabel('LpGBT master'),   row+3,    column, 1, 1)
        self.lpGBT_list[0].add_to_layout(layout,row+2,column+1)
        layout.addWidget(QtWidgets.QLabel('LpGBT slave'),   row+4,    column, 1, 1)
        self.lpGBT_list[1].add_to_layout(layout,row+4,column+1,first_row=False)


class tab_csm(object):


    def __init__(self, MainWindow,ser):
        self.transID = 0

        self.cell_list = []
        self.checkbox_list = []
        self.CSM_count = 2
        self.ser = ser
        self.setup_UI(MainWindow) 
        
    def setup_UI(self, MainWindow):

        sectionLayout = QtWidgets.QVBoxLayout(MainWindow)
        buttonWindow = QtWidgets.QWidget()
        buttonWindow.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        self.gridLayout = QtWidgets.QGridLayout(buttonWindow)

        column = 0
        row = 0
        self.pushButton_CSM_start = QtWidgets.QPushButton()
        self.pushButton_CSM_start.setText("CSM start")
        self.gridLayout.addWidget(self.pushButton_CSM_start,row,column,1,3)
        self.pushButton_CSM_start.clicked.connect(self.CSM_start)

        column = 0
        row += 1
        self.checkBox_checkCSM0 = QtWidgets.QCheckBox()
        self.checkBox_checkCSM0.setText("CSM0")
        self.checkBox_checkCSM0.setChecked(True)
        self.gridLayout.addWidget(self.checkBox_checkCSM0,   row,    0, 1, 1)
        self.checkbox_list.append(self.checkBox_checkCSM0)

        self.checkBox_checkCSM1 = QtWidgets.QCheckBox()
        self.checkBox_checkCSM1.setText("CSM1")
        self.checkBox_checkCSM1.setChecked(False)
        self.gridLayout.addWidget(self.checkBox_checkCSM1,   row+5,    0, 1, 1)
        self.checkbox_list.append(self.checkBox_checkCSM1)

        for j in range(self.CSM_count):
            csm_cell = CSM_cell()
            csm_cell.add_to_layout(self.gridLayout,row+5*j,column)
            self.cell_list.append(csm_cell)


        sectionLayout.addWidget(buttonWindow)


    def get_transID(self):
        if self.transID<0xff:
            self.transID += 1
        else:
            self.transID = 1
        return self.transID


 


    def print_message(self,str):
        print(str)

    def CSM_start(self):
        self.pushButton_CSM_start.setEnabled(False)
        # Step 2: Create a QThread object
        self.thread_csm = QThread()
        # Step 3: Create a worker object
        self.worker_csm = Worker_CSM(self)
        # Step 4: Move worker to the thread
        self.worker_csm.moveToThread(self.thread_csm)
        # Step 5: Connect signals and slots
        self.thread_csm.started.connect(self.disable_csm_button)
        self.thread_csm.started.connect(self.worker_csm.run)

        self.thread_csm.finished.connect(self.thread_csm.deleteLater)
        self.thread_csm.finished.connect(self.enable_csm_button)

        # self.thread_csm.finished.connect(self.print_res_result)

        self.worker_csm.message_out.connect(self.print_message)

        self.worker_csm.finished.connect(self.thread_csm.quit)
        self.worker_csm.finished.connect(self.worker_csm.deleteLater)

        # Step 6: Start the thread_csm
        self.thread_csm.start()

    def disable_csm_button(self):
        self.pushButton_CSM_start.setEnabled(False)

    def enable_csm_button(self):
        self.pushButton_CSM_start.setEnabled(True)


