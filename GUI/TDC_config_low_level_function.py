import time
from SCA_share import *
from lpgbt_register_map_v1 import LpgbtRegisterMapV1
import math
from JTAGTAPController import *
        

def str_to_hex(s,interval=''):
    s_r = ''
    s = s.decode('latin_1')
    for x in range(0, len(s)):
        s_raw="{:0>2x}".format(ord(s[x]))
        s_r = s_r + interval + s_raw
    return s_r


def str_to_bin(s,interval=''):
    s_r = ''
    s = s.decode('latin_1')
    for x in range(0, len(s)):
        s_raw="{:0>8b}".format(ord(s[x]))
        s_r = s_r + interval + s_raw
    return s_r



def bin_to_hex_8bit(s):
    if len(s)  !=8 :
        print("Error the length of string is not 8")
        return ''
    hex_raw_str=hex(int(s,2))
    if len(hex_raw_str) == 3:
        return('0'+hex_raw_str[2])
    else:
        return(hex_raw_str[2:])

def bin_to_hex(s):
    if len(s) % 8 !=0 :
        print("Error the length of string is not 8N")
        return ''
    s_r = ''
    for x in range(0,len(s) // 8):
        s_r = s_r + chr(int(s[x*8:x*8+8],2))
    return s_r

def chk_len(value,length):
    if len(format(value,   'b'))>length:
        print("variable length incorrect! %d"%value)
        return 1
    else: 
        return 0


def lpgbt_config_read(serial_port,lpgbt_addr,reg_addr,nb_read):
    if chk_len(lpgbt_addr,8) or chk_len(reg_addr,16) or chk_len(nb_read,8):
        return 1
    s0 = '\x00'
    s0 += chr(1*16+1)
    s0 += chr(2*16+int(format(lpgbt_addr,   'b').zfill( 8)[0 :4 ],2))
    s0 += chr(3*16+int(format(lpgbt_addr,   'b').zfill( 8)[4 :8 ],2))
    s0 += chr(4*16+int(format(reg_addr,     'b').zfill(16)[0 :4 ],2))
    s0 += chr(5*16+int(format(reg_addr,     'b').zfill(16)[4 :8 ],2))
    s0 += chr(6*16+int(format(reg_addr,     'b').zfill(16)[8 :12],2))
    s0 += chr(7*16+int(format(reg_addr,     'b').zfill(16)[12:16],2))
    s0 += chr(8*16+int(format(nb_read,      'b').zfill( 8)[0 :4 ],2))
    s0 += chr(9*16+int(format(nb_read,      'b').zfill( 8)[4 :8 ],2))
    s0 += '\x0f'
    serial_port.reset_input_buffer()
    serial_port.reset_output_buffer()

    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)

    readback = serial_port.read(nb_read+1000)
    print(readback)
    chk_readback = (lpgbt_addr*2+1)==readback[0] and readback[2]==1 and \
                readback[3]==nb_read and readback[4]==0 and \
                (readback[6]*256+readback[5])==reg_addr
    if not chk_readback:
        print("readback error!")
        return 1
    else:
        return readback[7:-1]


def lpgbt_config_write(serial_port,reg_addr,value_list=[],which_csm=0,lpgbt_addr=117):
    parity_cal = (lpgbt_addr%0x80*2) ^ (reg_addr//0x100) ^ (reg_addr%0x100) ^ len(value_list)
    for value in value_list:
        parity_cal = parity_cal^value
    parity_cal=format(parity_cal,'b').zfill(8)
    if len(value_list)>15:
        print("Error! Value_list length should be no more than 15")
        return -1
    else:
        print("Writing to CSM%s, lpGBT%s, reg%s, value=%s, parity_cal=%s" % (which_csm, lpgbt_addr,reg_addr, value_list,parity_cal))
        if len(value_list)==1:
            data=lpgbt_write(serial_port,lpgbt_addr=lpgbt_addr,reg_addr=reg_addr,reg_data=value_list[0])
        else:
            data=lpgbt_write_multi(serial_port,lpgbt_addr=lpgbt_addr,reg_addr=reg_addr,value_list=value_list)
        return data


    # lpgbt_read_parity(serial_port,which_csm=which_csm,lpgbt_addr=lpgbt_addr)

def lpgbt_read_parity(serial_port,lpgbt_addr=117):
    lpgbt_read(serial_port,lpgbt_addr,reg_addr=0x1e7,nb_to_be_read=1)

def value_to_list(value):
    value_list = []
    while(1):
        value_list.insert(0,value&0xFF)
        value = value>>8
        if value==0:
            break
    return value_list

def fpga_readback(serial_port,fpga_reg_addr):
    s0 = '\x00'
    i = 1
    s0 += chr(i*16+2) # set [157]=1'b1
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16) 
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(fpga_reg_addr,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(fpga_reg_addr,   'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    for j in range(40-4):    #fill 0 for 160 bits command length
        s0 += chr(i*16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(10000)
    if len(readback):
        # print(readback)
        # print(str_to_hex(readback))
        bin_str=str_to_bin(readback)
        return bin_str
    else:
        print("readback empty!")
    return 0

def fpga_reg_reset(serial_port):
    s0 = '\x00'
    i = 1
    s0 += chr(i*16+4+1) # set [158]=1'b1, [156]=1'b1
    i = 1 if i == 13 else i + 1
    for j in range(40-1):    #fill 0 for 160 bits command length
        s0 += chr(i*16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    return 0

def fpga_config(serial_port,fpga_reg_addr,value_list):
    value_length = len(value_list) #value_length unit is byte (1 = 8 bits)
    for value in value_list:
        if chk_len(value,8):
            return 1
    if chk_len(fpga_reg_addr,8):
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i*16+4) # set [158]=1'b1
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16) 
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(fpga_reg_addr,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(fpga_reg_addr,   'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    for j in range(40-(2*value_length+4)):    #fill 0 for 160 bits command length
        s0 += chr(i*16)
        i = 1 if i == 13 else i + 1
    for value in value_list:   # value list
        s0 += chr(i*16+int(format(value,   'b').zfill( 8)[0 :4 ],2))
        i = 1 if i == 13 else i + 1
        s0 += chr(i*16+int(format(value,   'b').zfill( 8)[4 :8 ],2))
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    return 0

def lpgbt_write_multi(serial_port,lpgbt_addr,reg_addr,value_list):
    value_length = len(value_list) #value_length unit is byte (1 = 8 bits)
    for value in value_list:
        if chk_len(value,8):
            return 1
    if chk_len(lpgbt_addr,8) or chk_len(reg_addr,16):
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i*16+8) # set [159]=1'b1
    i = 1 if i == 13 else i + 1

    for j in range(40-(2+2*value_length+6+1)):    #fill 0 for 160 bits command length
        # 40*4 bits in total. 2*4 bits for value length, 2*value_length*4 bits for data,
        # 2*4 bits for lpgbt_addr, 4*4 bits for reg_addr, 1*4 bits for bit [159].
        s0 += chr(i*16)
        i = 1 if i == 13 else i + 1

    s0 += chr(i*16+int(format(lpgbt_addr,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(lpgbt_addr,   'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[8 :12],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[12:16],2))
    i = 1 if i == 13 else i + 1

    for value in value_list:   # value list
        s0 += chr(i*16+int(format(value,   'b').zfill( 8)[0 :4 ],2))
        i = 1 if i == 13 else i + 1
        s0 += chr(i*16+int(format(value,   'b').zfill( 8)[4 :8 ],2))
        i = 1 if i == 13 else i + 1

    s0 += chr(i*16+int(format(value_length,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(value_length,   'b').zfill( 8)[4 :8 ],2))

    s0 += '\x0f'
    # print(str_to_hex(s0.encode('latin_1')))

    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    # print()

    readback = serial_port.read(10000)
    if len(readback):
        # print(readback)
        # print(str_to_hex(readback))
        data=lpgbt_read_back_print(str_to_hex(readback))
        return data
    else:
        print("readback empty!")
    return 0

def lpgbt_write(serial_port,lpgbt_addr,reg_addr,reg_data):
    value_length = 1 #value_length unit is byte (1 = 8 bits)

    if chk_len(lpgbt_addr,8) or chk_len(reg_addr,16) or chk_len(reg_data,8):
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i*16+2) # set [41]=1'b1
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(lpgbt_addr,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(lpgbt_addr,   'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[8 :12],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[12:16],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_data,     'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_data,     'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1

    s0 += chr(i*16+int(format(value_length,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(value_length,   'b').zfill( 8)[4 :8 ],2))

    s0 += '\x0f'
    # print(str_to_hex(s0.encode('latin_1')))

    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    # print()

    readback = serial_port.read(20)
    if len(readback):
        # print(readback)
        # print(str_to_hex(readback))
        data=lpgbt_read_back_print(str_to_hex(readback))
        return data
    else:
        print("readback empty!")
    return 0

def lpgbt_read(serial_port,lpgbt_addr,reg_addr,nb_to_be_read): #only work when nb_to_be_read = 1, need to address
    value_length = nb_to_be_read #value_length unit is byte (1 = 8 bits)

    if chk_len(lpgbt_addr,8) or chk_len(reg_addr,16) or chk_len(nb_to_be_read,8):
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i*16+1) # set [40]=1'b1
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(lpgbt_addr,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(lpgbt_addr,   'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[4 :8 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[8 :12],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(reg_addr,     'b').zfill(16)[12:16],2))
    i = 1 if i == 13 else i + 1

    s0 += chr(i*16+int(format(0,            'b').zfill( 8)[0 :4 ],2))  # fill 0 for data (1 byte)
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(0,            'b').zfill( 8)[4 :8 ],2))
    i = 1 if i == 13 else i + 1

    s0 += chr(i*16+int(format(value_length,   'b').zfill( 8)[0 :4 ],2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i*16+int(format(value_length,   'b').zfill( 8)[4 :8 ],2))

    s0 += '\x0f'
    # print(str_to_hex(s0.encode('latin_1')))

    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    # print()

    readback = serial_port.read(20)
    if len(readback):

        print(str_to_hex(readback))
        data=lpgbt_read_back_print(str_to_hex(readback))
        return data
    else:
        print("readback empty!")
        return -1



def lpgbt_read_back_print(readback_hex):
    lpGBT_address = int(readback_hex[2:4],16)//2
    command = int(readback_hex[4:6],16)
    nb_of_data = int(readback_hex[6:8],16) + int(readback_hex[8:10],16)*256
    reg_addr = int(readback_hex[10:12],16) + int(readback_hex[12:14],16)*256
    data=[]
    if len(readback_hex)!=16+2*nb_of_data:
        print("Readback"+str(len(readback_hex)/2.0)+ " bytes not as expected "+str(9+nb_of_data))
        return -1
    else:
        for i in range(nb_of_data):            
            data.append(int(readback_hex[14+2*i:16+2*i],16))
        parity = format(int(readback_hex[16+2*i:18+2*i],16),'b').zfill(8)
        parity_cal = 0
        if nb_of_data == 1:
            print("Lpgbt%s reply %s regs from reg%s(%s), value=%s, parity=%s"%(lpGBT_address,nb_of_data,reg_addr,str(LpgbtRegisterMapV1.Reg(reg_addr))[4:],data,parity))
        else:
            print("Lpgbt%s reply %s regs from reg%s, value=%s, parity=%s"%(lpGBT_address,nb_of_data,reg_addr,data,parity))
        if command!=1:
            print("Parity check failed for downlink data. Reply command != 1.")
        for i in range(2,len(readback_hex)//2):
            parity_cal = parity_cal ^ int(readback_hex[i:i+2],16)
        parity_cal = format(parity_cal,'b').zfill(8)
        # print("Readback parity_cal = " + parity_cal)
        return data


def SCA_config_bitfile_bak(serial_port,command,data):
    if chk_len(data,32) or chk_len(command,8):
        print("Check input value!")
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8+((command&0x7F)>>4))  # set bits command_r[35:32]  35:flag 34:32 command
    i = 1 if i == 13 else i + 1
    for j in range(8):
        s0 += chr(i * 16 + int(format(data, 'b').zfill(32)[j * 4:j * 4 + 4], 2))
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(str_to_hex(s0.encode('latin_1')))
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()

def SCA_config_bitfile(serial_port,command,data):  #must follow a valid which_CSM selection
    if chk_len(data,32) or chk_len(command,8):
        print("Check input value!")
        return 1
    s0 = ''
    s0 += chr(15 * 16 + 8+((command&0x7F)>>4))  # set bits command_r[35:32]  35:flag 34:32 command
    for j in range(4):
        s0 += chr(int(format(data, 'b').zfill(32)[j * 8:j * 8 + 8], 2))
    # print(str_to_hex(s0.encode('latin_1')))
    serial_port.write(s0.encode('latin_1'))
    # serial_port.flush()  # saves time if transmit bulk data


def SCA_config_JTAG_GO_REQ(serial_port,command,read_required=0):
    # readback = serial_port.read(100)
    if chk_len(command,4):
        print("Check input value!")
        return 1
    s0 = ''
    s0 += chr(14 * 16 + 8+(command&0x01))  # set bits command_r[3:0] 
    # command_r[3] flag command_r[1]=1:JTAG_GO command_r[1]=0:JTAG_BUSY_Req 
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    readback = serial_port.read(200)
    if read_required:
        i=1
        while(len(readback)==0):
            print(f"SCA readback empty, tried {i} times")
            time.sleep(0.001)
            readback = serial_port.read(100)
            i += 1
            if i >100:
                print("SCA readback failed after 100 attempts")
    # SCA_reply_print(str_to_hex(readback))
    readback_hex = str_to_hex(readback)
    if len(readback_hex)>10:
        SCA_channel = int(readback_hex[4:6],16)
        error = int(readback_hex[6:8],16)
        data = int(readback_hex[10:],16)
        return [SCA_channel,error,data]
    else:
        return [-1,-1,-1]

    
def SCA_config_JTAG_GO_REQ_bak(serial_port,command,read_required=0):
    if chk_len(command,4):
        print("Check input value!")
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8+(command&0x01))  # set bits command_r[3:0] 
    # command_r[3] flag command_r[1]=1:JTAG_GO command_r[1]=0:JTAG_BUSY_Req 
    i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(str_to_hex(s0.encode('latin_1')))
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    readback = serial_port.read(100)
    if read_required:
        
        i=1
        while(len(readback)==0):
            print(f"SCA readback empty, tried {i} times")
            time.sleep(0.002)
            readback = serial_port.read(100)
            i += 1
            if i >100:
                print("SCA readback failed after 100 attempts")
    # SCA_reply_print(str_to_hex(readback))
    readback_hex = str_to_hex(readback)
    if len(readback_hex)>10:
        SCA_channel = int(readback_hex[4:6],16)
        error = int(readback_hex[6:8],16)
        data = int(readback_hex[10:],16)
        return [SCA_channel,error,data]
    else:
        return [-1,-1,-1]

def SCA_config(serial_port,sca_enable,which_csm,sca_address,transID,channel,datalen,command,data,read_required=0,wait=0):
    if chk_len(sca_address,8) or chk_len(transID,8) or chk_len(channel,8) or \
    chk_len(datalen,8) or chk_len(command,8) or chk_len(data,32) or chk_len(sca_enable,4):
        print("Check input value!")
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(transID, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(transID, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(channel, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(channel, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(datalen, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(datalen, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(command, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(command, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(8):
        s0 += chr(i * 16 + int(format(data, 'b').zfill(32)[j * 4:j * 4 + 4], 2))
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(str_to_hex(s0.encode('latin_1')))
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    if wait>0:
        time.sleep(wait)

    readback = serial_port.read(100)
    if read_required:
        i=1
        while(len(readback)==0):
            print(f"SCA readback empty, tried {i} times")
            readback = serial_port.read(100)
            i += 1
            if i >100:
                print("SCA readback failed after 100 attempts")
    # SCA_reply_print(str_to_hex(readback))
    readback_hex = str_to_hex(readback)
    if len(readback_hex)>10:
        SCA_channel = int(readback_hex[4:6],16)
        error = int(readback_hex[6:8],16)
        data = int(readback_hex[10:],16)
        return [SCA_channel,error,data]
    else:
        return [-1,-1,-1]

def SCA_reply_print(readback):
    if(len(readback)==0):
        print("readback empty!")
        return 1
    string = f'Total len={len(readback)}'
    # string+= 'SCA'+ readback[0:2]
    # string+= 'tID'+ readback[2:4]+' '
    SCA_channel = readback[4:6]
    error = readback[6:8]
    if int(SCA_channel,16)==SCA_share.ADC_CHANNEL:  
        SCA_channel = 'ADC'
        string+=SCA_channel+' :'
        adc = int(readback[10:],16)
        voltage = adc/4096.0
        string += str(voltage)[0:7] + 'V'
        if error != '00':
            print('Error code='+error)
    else:
        string+='Channel: 0x'
        string+= SCA_channel
        string+= ' :Error: 0x'+ readback[6:8]
        string+= 'length: 0x'+ readback[8:10]
        string+= 'Data: 0x'+ readback[10:]
    print(string)
    return 0



def SCA_connect(serial_port,sca_enable,which_csm,sca_address,verbose=0):
    if chk_len(sca_address,8) or chk_len(sca_enable,4):
        print("Check input value!")
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2)+1) # set bit command_r[72]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(16):
        s0 += chr(i * 16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(s0)
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(100)
    if verbose:
        print(str_to_hex(readback))

    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2)) # unset bit command_r[72]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(16):
        s0 += chr(i * 16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(s0)
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(100)
    if verbose:
        print(str_to_hex(readback))
    return 0

def SCA_start_reset(serial_port,sca_enable,which_csm,sca_address,verbose=0):
    if chk_len(sca_address,8) or chk_len(sca_enable,4):
        print("Check input value!")
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2)+2) # set bit command_r[73]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(16):
        s0 += chr(i * 16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(s0)
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(100)
    if verbose:
        print(str_to_hex(readback))

    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2)) # unset bit command_r[73]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(16):
        s0 += chr(i * 16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(s0)
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(100)
    if verbose:
        print(str_to_hex(readback))
    return 0

def SCA_tx_rx_reset(serial_port,sca_enable,which_csm,sca_address,verbose=0):
    if chk_len(sca_address,8) or chk_len(sca_enable,4):
        print("Check input value!")
        return 1
    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2)+4) # set bit command_r[74]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(16):
        s0 += chr(i * 16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(s0)
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(100)
    if verbose:
        print(str_to_hex(readback))


    s0 = '\x00'
    i = 1
    s0 += chr(i * 16 + 8 + int(format(sca_enable, 'b').zfill(4)[0:3], 2))  # set bits command_r[79:76]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*int(format(sca_enable, 'b').zfill(4)[3], 2)) # unset bit command_r[74]
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + 8*which_csm+int(format(sca_address, 'b').zfill(8)[0:4], 2))
    i = 1 if i == 13 else i + 1
    s0 += chr(i * 16 + int(format(sca_address, 'b').zfill(8)[4:8], 2))
    i = 1 if i == 13 else i + 1
    for j in range(16):
        s0 += chr(i * 16)
        i = 1 if i == 13 else i + 1
    s0 += '\x0f'
    # print(s0)
    serial_port.write(s0.encode('latin_1'))
    serial_port.flush()
    time.sleep(0.01)
    readback = serial_port.read(100)
    if verbose:
        print(str_to_hex(readback))


    return 0

def SCA_JTAGset(ser,transID,sca_enable,which_csm,data_len):
    SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.CR_CHANNEL,datalen=4,command=SCA_share.CTRL_W_CRD,data=SCA_share.ENJTAG_DATA)
    # data_len = 100        
    RXEDGE = 0 #RXEDGE = 0 TDI signal is sampled on the rising edge of TCK (STANDARD)
    TXEDGE = 0 #TXEDGE = 0 TDO and TMS signal change on the rising edge of TCK (STANDARD)
    MSB=1 # MSB=1 Bits are transmitted from the most significant to the least significant
    IVNTCK = 0 #IVNTCK TCK idle level is high (STANDARD)
    data = data_len + (RXEDGE<<9) + (TXEDGE<<10) + (MSB<<11)+(IVNTCK<<14)
    try:
        [SCA_channel,error,databack]=SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_CTRL,data=data)
    except Exception as e:
        print("JTAG_W_CTRL failed for CSM%d, SCA%d" %(which_csm,math.log2(sca_enable)))
        print(e)
    else:
        data_len_back = int(format(data,'b').zfill(16)[-7:],2)
        print("JTAG_W_CTRL configured data_len to %d for CSM%d, SCA%d" %(data_len_back if data_len_back != 0 else 128,which_csm,math.log2(sca_enable)))


def SCA_clk_div(ser,transID,sca_enable,which_csm,data):
    SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.CR_CHANNEL,datalen=4,command=SCA_share.CTRL_W_CRD,data=SCA_share.ENJTAG_DATA)
    SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_FREQ,data=data)
    try:
        [SCA_channel,error,data]=SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=6,command=SCA_share.JTAG_R_FREQ,data=0)
    except Exception as e:
        print("JTAG div configure failed for CSM%d, SCA%d" %(which_csm,math.log2(sca_enable)))
        print(e)
    else:
        print("JTAG div configured to %d for CSM%d, SCA%d" %(data,which_csm,math.log2(sca_enable)))


def write_TMS(ser,transID,sca_enable,which_csm,data):
    length = len(data)
    if length>128:
        print("Too long!")
        return
    if length>96:
        tms= int(data[:-96],2)
        # print("Writing to TMS3: 0x%08x"%tms)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TMS3,data=tms)
    if length>64:
        tms= int(data[:-64].zfill(32)[-32:],2)
        # print("Writing to TMS2: 0x%08x"%tms)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TMS2,data=tms)
    if length>32:
        tms= int(data[:-32].zfill(32)[-32:],2)
        # print("Writing to TMS1: 0x%08x"%tms)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TMS1,data=tms)
    if length>0:
        tms= int(data.zfill(32)[-32:],2)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TMS0,data=tms)
        # print("Writing to TMS0: 0x%08x"%tms)

def write_TMS_integ(ser,transID,sca_enable,which_csm,data):
    length = len(data)
    if length>4:
        print("Too long for a single transaction, please split TMS!")
        return
    if length>3:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TMS3,data=data[-4])
    if length>2:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TMS2,data=data[-3])
    if length>1:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TMS1,data=data[-2])
    if length>0:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TMS0,data=data[-1])


def write_TDO(ser,transID,sca_enable,which_csm,data):
    length = len(data)
    if length>128:
        print("Too long for a single transaction, please split TDO!")
        return
    if length>96:
        tdo= int(data[:-96],2)
        # print("Writing to TDO3: 0x%08x"%tms)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO3,data=tdo)
    if length>64:
        tdo= int(data[:-64].zfill(32)[-32:],2)
        # print("Writing to TDO2: 0x%08x"%tms)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO2,data=tdo)
    if length>32:
        tdo= int(data[:-32].zfill(32)[-32:],2)
        # print("Writing to TDO1: 0x%08x"%tms)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO1,data=tdo)
    if length>0:
        tdo= int(data.zfill(32)[-32:],2)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO0,data=tdo)
        # print("Writing to TDO0: 0x%08x"%tms)

def write_TDO_integ(ser,transID,sca_enable,which_csm,data):
    length = len(data)
    if length>4:
        print("Too long for a single transaction, please split TDO!")
        return
    if length>3:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TDO3,data=data[-4])
    if length>2:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TDO2,data=data[-3])
    if length>1:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TDO1,data=data[-2])
    if length>0:
        SCA_config_bitfile(ser,command=SCA_share.JTAG_W_TDO0,data=data[-1])

def read_TDI(ser,transID,sca_enable,which_csm,length):
    readback = ''
    if length>128:
        print("Too long for a single transaction, please split!")
        return
    if length>96:
        try:
            [SCA_channel,error,data]=SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TDI3,data=0,wait = 0.002)
        except Exception as e:
            print("JTAG_R_TDI3 failed for CSM%d" %(which_csm))
            print(e)
        else:
            readback += format(data,'b').zfill(32)[-length+96:]
    if length>64:
        try:
            [SCA_channel,error,data]=SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TDI2,data=0,wait = 0.002)
        except Exception as e:
            print("JTAG_R_TDI2 failed for CSM%d" %(which_csm))
            print(e)
        else:
            readback += format(data,'b').zfill(32)[-length+64:]
    if length>32:
        try:
            [SCA_channel,error,data]=SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TDI1,data=0,wait = 0.002)
        except Exception as e:
            print("JTAG_R_TDI1 failed for CSM%d" %(which_csm))
            print(e)
        else:
            readback += format(data,'b').zfill(32)[-length+32:]
    if length>0:
        try:
            [SCA_channel,error,data]=SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TDI0,data=0,wait = 0.002)
        except Exception as e:
            print("JTAG_R_TDI0 failed for CSM%d" %(which_csm))
            print(e)
        else:
            readback += format(data,'b').zfill(32)[-length:]
    return readback

def SCA_JTAG_go(ser,transID,sca_enable,which_csm,ir_value,dr_value,isASD=False):
    jtag_gen = JTAGTAPController()
    full_tdo, full_tms, tdi_mask = jtag_gen.full_sequence(ir_value, dr_value,isASD)
    readback = ''
    while len(full_tms):
        length = len(full_tms) if len(full_tms)<128 else 128
        write_TMS(ser=ser,transID=transID,sca_enable=sca_enable,which_csm=which_csm,data=full_tms[0:length])
        write_TDO(ser=ser,transID=transID,sca_enable=sca_enable,which_csm=which_csm,data=full_tdo[0:length])
        SCA_JTAGset(ser=ser,transID=transID,sca_enable=sca_enable,which_csm=which_csm,data_len=length)
        SCA_config(ser,sca_enable=sca_enable,which_csm=which_csm,sca_address=0x00,transID=transID,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_GO,data=0,wait=0.01)

        readback += read_TDI(ser=ser,transID=transID,sca_enable=sca_enable,which_csm=which_csm,length=length)
        full_tms=full_tms[length:]
        full_tdo=full_tdo[length:]
    # print('JTAG seq: '+self.readback)
    # print('DR seq: '+self.readback[::-1])
    
    valid_tdi = ''
    for i in range(len(tdi_mask)):
        if tdi_mask[i]=='1':
            if isASD:
                valid_tdi += readback[i]  
            else:
                valid_tdi += readback[i]  
    print('Valid TDI in DR seq: '+format(int(valid_tdi[::-1],2),'x'))
    
    if isASD:
        print('Valid TDI ASD2 DR seq: '+format(int(valid_tdi[55*2:55*3][::-1],2),'x'))
        print('Valid TDI ASD2 DR seq: '+format(int(valid_tdi[55*2:55*3][::-1],2),'055b'))

        print('Valid TDI ASD1 DR seq: '+format(int(valid_tdi[55*1:55*2][::-1],2),'x'))
        print('Valid TDI ASD1 DR seq: '+format(int(valid_tdi[55*1:55*2][::-1],2),'055b'))

        print('Valid TDI ASD0 DR seq: '+format(int(valid_tdi[0:55][::-1],2),'x'))
        print('Valid TDI ASD0 DR seq: '+format(int(valid_tdi[0:55][::-1],2),'055b'))

    return valid_tdi
    
def reset_TDO(self,which_csm):
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO3,data=0)
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO2,data=0)
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO1,data=0)
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=4,command=SCA_share.JTAG_W_TDO0,data=0)


def read_TMS(self,which_csm):
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TMS0,data=0)
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TMS1,data=0)
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TMS2,data=0)
    SCA_config(self.ser,sca_enable=0b0010,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_TMS3,data=0)

    
    
