from PyQt5.QtCore import *
import subprocess
import time
import sys
import pandas as pd
from TDC_config_low_level_function import *
import csv


class Worker_lpGBT(QObject):
    finished = pyqtSignal()
    message_out = pyqtSignal(str)

    def __init__(self,ser):
        super(Worker_lpGBT, self).__init__()
        self.ser = ser


    def load_lpgbt_config(self,which_csm, is_master):
        if is_master:
            filename = "lpgbt_master_reg.csv"
            lpgbt_addr = 117
        else:
            filename = "lpgbt_slave_reg.csv"
            lpgbt_addr = 115

        try:
            self.df = pd.read_csv(filename,delimiter=' ',header=None)
        except Exception as e:
            print("Error loading "+filename)
            print(e)
        else:
            print(filename+" loaded successfully.")
            reg_value_list = [int(data,16) for data in self.df[1].values.tolist()]
            addr=0
            while len(reg_value_list):
                a=[]
                for i in range(15):
                    if len(reg_value_list):
                        a.append(reg_value_list.pop(0))
                try:
                    lpgbt_config_write(self.ser, reg_addr=addr, 
                        value_list=a,which_csm=which_csm,lpgbt_addr=lpgbt_addr)
                except Exception as e:
                    print(e)
                addr += 15

        self.finished.emit()
