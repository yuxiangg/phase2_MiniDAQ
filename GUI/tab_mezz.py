# pop_mezz.py

from PyQt5 import QtCore, QtGui, QtWidgets

from TDC_config_low_level_function import *

from SCA_share import *
from TDCreg import *
from CSMreg import *
from pop_single_mezz import pop_single_mezz
from pop_mezz_setup import pop_mezz_setup


class tab_mezz(object):


    def __init__(self, MainWindow,ser):
        self.ser = ser
        self.transID = 1
        self.CSM_per_MiniDAQ = 2
        self.mezz_per_CSM = 20
        self.gpio_tdc_mapping = [10, 14, 1, 13, 17, 6, 26, 20, 12, 19, 23, 15, 27, 24, 21, 18, 28, 16, 25, 22]
        self.gpio_output_bit = self.gpio_tdc_mapping+[29]
        self.jtag_gen = JTAGTAPController()
        self.CSM_list = []
        for i in range(self.CSM_per_MiniDAQ):
            self.CSM_list.append(CSMreg())
           
        
        

        self.setup_UI(MainWindow) 
        

        
    def setup_UI(self, MainWindow):
        sectionLayout = QtWidgets.QVBoxLayout(MainWindow)
        buttonWindow = QtWidgets.QWidget()
        buttonWindow.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        self.gridLayout = QtWidgets.QGridLayout(buttonWindow)

         # Create checkboxes linked to self.TDC_enable
        self.csm_checkbox_list = []  # List to hold all TDC checkboxes. e.g.self.csm_checkbox_list[CSM][Mezz]
        self.csm_indicator_list = []  # List to hold all color indicators. 
        # e.g. self.csm_indicator_list[CSM][Mezz][0] is TDC config status, [CSM][Mezz][1] is ASD status, 
        # [CSM][Mezz][2] is dline lock status
        



        row = 0
        column = 0

        for j in range(self.CSM_per_MiniDAQ):
            column = 0
            single_csm_checkboxes = []
            single_csm_indicators = []

            # Add 'Check All' checkbox
            self.check_all_checkbox = QtWidgets.QCheckBox("All CSM%d"%j)
            self.check_all_checkbox.stateChanged.connect(lambda state, which_csm=j:self.check_all_checkboxes(which_csm,state))
            self.gridLayout.addWidget(self.check_all_checkbox, row, column, 1, 3)  
            row += 1
            column = 0

            for i in range(self.mezz_per_CSM):
                checkbox = QtWidgets.QCheckBox(f"Mezz {i+j*20}")
                checkbox.setChecked(self.CSM_list[j].TDC_enable[0][i] == '1')
                checkbox.stateChanged.connect(lambda state, which_csm=j, idx=i: self.update_TDC_enable(which_csm, idx, state))
                single_csm_checkboxes.append(checkbox)
                if i == 10:
                    row += 2
                    column = 0
                self.gridLayout.addWidget(checkbox, row, column,1,3)
                row += 1
                color_indicators = []
                for k in range(3):
                    color_indicator = QtWidgets.QLabel()
                    color_indicator.setFixedSize(20, 20)
                    color_indicator.setStyleSheet("background-color: lightgray; border: 1px solid black;")
                    self.gridLayout.addWidget(color_indicator, row, column,1,1)
                    color_indicators.append(color_indicator)
                    column += 1
                single_csm_indicators.append(color_indicators)
                row -= 1
                column += 1
                column += 1
            self.csm_checkbox_list.append(single_csm_checkboxes)
            self.csm_indicator_list.append(single_csm_indicators)

            row += 2
        
        row = 0
        self.pushButton_scanID = self.create_button("Mezz Scan", row, column, 1,self.mezz_scan)


        row += 1
        self.pushButton_write_TDC_all = self.create_button("write_TDC_all", row, column, 1,self.write_TDC_all)

        row += 1
        self.pushButton_write_ASD_all = self.create_button("write_ASD_all", row, column, 1,self.write_ASD_all)

        row += 1
        self.pushButton_TDC_input_disable = self.create_button("TDC_input_disable", row, column, 1,lambda:self.TDC_input_config(False))

        row += 1
        self.pushButton_dline_status = self.create_button("dline update", row, column, 1,self.get_dline_status)

        row += 1
        self.pushButton_global_reset = self.create_button("global reset", row, column, 1,self.fpga_global_reset)

        row += 1
        self.pushButton_TDC_input_enable = self.create_button("TDC_input_enable", row, column, 1,lambda:self.TDC_input_config(True))

        row += 1
        self.pushButton_TDC_setup = self.create_button("TDC_setup", row, column, 1,self.TDC_setup_dialog)

        row += 1
        self.pushButton_reset_status = self.create_button("reset status", row, column, 1,self.reset_status)



        sectionLayout.addWidget(buttonWindow)



    def TDC_input_config(self,enable):
        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        for j in range(self.CSM_per_MiniDAQ):
            for i in range(self.mezz_per_CSM):
                if enable:
                    self.CSM_list[j].TDC_list[i].channel_enable_r[0]=24*'1'
                    self.CSM_list[j].TDC_list[i].channel_enable_f[0]=24*'1'
                    self.CSM_list[j].TDC_list[i].enable_leading[0] = '0'
                    self.CSM_list[j].TDC_list[i].enable_pair[0] = '1'
                else:
                    self.CSM_list[j].TDC_list[i].channel_enable_r[0]=24*'0'
                    self.CSM_list[j].TDC_list[i].channel_enable_f[0]=24*'0'
                    self.CSM_list[j].TDC_list[i].enable_leading[0] = '1'
                    self.CSM_list[j].TDC_list[i].enable_pair[0] = '0'
                if self.CSM_list[j].TDC_enable[0][i]=='1':
                    self.Mezz_enable(i,which_csm=j)
                    self.config_single(SCA_share.TDC_SETUP0,  self.CSM_list[j].TDC_list[i],which_csm=j)
                    crc_valid = self.read_status0(self.CSM_list[j].TDC_list[i],which_csm=j)
                    if crc_valid:
                        self.csm_indicator_list[j][i][0].setStyleSheet("background-color: green; border: 1px solid black;")
                    else:
                        self.csm_indicator_list[j][i][0].setStyleSheet("background-color: red; border: 1px solid black;")
        if enable:
            print("All TDC input enabled.")
        else:
            print("All TDC input disabled.")




    def check_all_checkboxes(self, which_csm, state):
        """Set all TDC checkboxes to checked or unchecked."""
        for checkbox in self.csm_checkbox_list[which_csm]:
            checkbox.setChecked(state == QtCore.Qt.Checked)

    def update_TDC_enable(self, which_csm, index, state):
        """Update the corresponding bit in self.TDC_enable based on checkbox state."""
        tdc_enable_list = list(self.CSM_list[which_csm].TDC_enable[0])
        tdc_enable_list[index] = '1' if state == QtCore.Qt.Checked else '0'
        self.CSM_list[which_csm].TDC_enable[0] = ''.join(tdc_enable_list)

    # Helper function to create buttons
    def create_button(self, label, row, column, width, slot_function):
        button = QtWidgets.QPushButton(label)
        button.setFixedSize(150, 30)
        self.gridLayout.addWidget(button, row, column, 1, width)
        button.clicked.connect(slot_function)
        return button

    def mezz_scan(self):
        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        for j in range(self.CSM_per_MiniDAQ):
            SCA_clk_div(ser=self.ser,transID=self.get_transID(),sca_enable=0b0010,which_csm=j,data=10)
            self.SCA_GPIO_init(which_csm=j)

        for j in range(self.CSM_per_MiniDAQ):
            for i in range(self.mezz_per_CSM):
                if self.CSM_list[j].TDC_enable[0][i]=='1':
                    self.Mezz_enable(i,which_csm=j)
                    try:
                        valid_tdi = self.config_single(SCA_share.TDC_IDCODE,self.CSM_list[j].TDC_list[i],which_csm=j)
                        if format(int(valid_tdi[::-1],2),'08x')=='fadec001':
                            self.csm_checkbox_list[j][i].setStyleSheet("background-color: cyan;")
                        else:
                            self.csm_checkbox_list[j][i].setStyleSheet("background-color: lightgray;")
                            self.csm_checkbox_list[j][i].setChecked(False)
                            
                    except Exception as e:
                        print("Mezz scan failed for CSM%d" %(j))
                        print(e)
                        self.csm_checkbox_list[j][i].setStyleSheet("background-color: lightgray;")
                        self.csm_checkbox_list[j][i].setChecked(False)
                    else:
                        pass

                        

    def write_TDC_all(self):
        fpga_config(self.ser, 1, [4])  #select uart mapping SCA

        for j in range(self.CSM_per_MiniDAQ):
            SCA_clk_div(ser=self.ser,transID=self.get_transID(),sca_enable=0b0010,which_csm=j,data=10)
            self.SCA_GPIO_init(which_csm=j)
            for i in range(self.mezz_per_CSM):
                if self.CSM_list[j].TDC_enable[0][i]=='1':
                    print("Start JTAG config for CSM%d, TDC%d"%(j,i))
                    self.Mezz_enable(i,which_csm=j)
                    self.config_single(SCA_share.TDC_SETUP0,  self.CSM_list[j].TDC_list[i],which_csm=j)
                    self.config_single(SCA_share.TDC_SETUP1,  self.CSM_list[j].TDC_list[i],which_csm=j)
                    self.config_single(SCA_share.TDC_SETUP2,  self.CSM_list[j].TDC_list[i],which_csm=j)
                    self.config_single(SCA_share.TDC_CONTROL0,self.CSM_list[j].TDC_list[i],which_csm=j)
                    self.config_single(SCA_share.TDC_CONTROL1,self.CSM_list[j].TDC_list[i],which_csm=j)
                    crc_valid = self.read_status0(self.CSM_list[j].TDC_list[i],which_csm=j)
                    if crc_valid:
                        self.csm_indicator_list[j][i][0].setStyleSheet("background-color: green; border: 1px solid black;")
                    else:
                        self.csm_indicator_list[j][i][0].setStyleSheet("background-color: red; border: 1px solid black;")
                    print("JTAG config for CSM%d, TDC%d completed."%(j,i))


    def write_ASD_all(self):
        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        for j in range(self.CSM_per_MiniDAQ):
            SCA_clk_div(ser=self.ser,transID=self.get_transID(),sca_enable=0b0010,which_csm=j,data=10)
            self.SCA_GPIO_init(which_csm=j)
            for i in range(self.mezz_per_CSM):
                if self.CSM_list[j].TDC_enable[0][i]=='1':
                    print("Start JTAG config for CSM%d, Mezz%d ASDs"%(j,i))
                    self.Mezz_enable(i,which_csm=j)

                    self.config_single(SCA_share.ASD_WRITE,self.CSM_list[j].TDC_list[i],which_csm=j)
                    valid_tdi=self.config_single(SCA_share.ASD_READ,self.CSM_list[j].TDC_list[i],which_csm=j)
                    dr_value = ''
                    for ASD in [self.CSM_list[j].TDC_list[i].ASD2.setup,self.CSM_list[j].TDC_list[i].ASD1.setup,self.CSM_list[j].TDC_list[i].ASD0.setup]:
                        for s in ASD:
                            dr_value += ''.join(s)

                    if dr_value == valid_tdi[::-1]:
                        print("ASD readback value same as write value")
                        self.csm_indicator_list[j][i][1].setStyleSheet("background-color: green; border: 1px solid black;")
                    else:
                        print("Error! ASD readback value different from write value")
                        self.csm_indicator_list[j][i][1].setStyleSheet("background-color: red; border: 1px solid black;")


    def read_status0(self,TDC_inst=0,which_csm=0):
        valid_tdi=self.config_single(SCA_share.TDC_STATUS0,TDC_inst=TDC_inst,which_csm=which_csm)
        CRC = format(int(valid_tdi[::-1][1:33],2),'08X')
        print("Readback CRC = "+CRC)
        CRC_cal = crc_cal(TDC_inst)
        print("Calculated CRC = "+CRC_cal)
        if CRC == CRC_cal:
            print("TDC readback CRC same as calculated.")
            return True
        else:
            print("Error! TDC readback CRC different from calculated!")
            return False



    def get_transID(self):
        if self.transID<0xff:
            self.transID += 1
        else:
            self.transID = 1
        return self.transID


    def SCA_GPIO_init(self,which_csm=0):
        SCA_config(self.ser,sca_enable=0b0100,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.CR_CHANNEL,datalen=4,command=SCA_share.CTRL_W_CRB,data=SCA_share.ENGPIO_DATA)
       
        data = 0
        for i in range(32):
            if i in self.gpio_output_bit:
                data += 1<<i

        SCA_config(self.ser,sca_enable=0b0100,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.GPIO_CHANNEL,datalen=4,command=SCA_share.GPIO_W_DIRECTION,data=data)
        try:
            [SCA_channel,error,data]=SCA_config(self.ser,sca_enable=0b0100,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.GPIO_CHANNEL,datalen=2,command=SCA_share.GPIO_R_DIRECTION,data=0xFFFFFFFF)
        except Exception as e:
            print("GPIO_R_DIRECTION failed for CSM%d" %(which_csm))
            print(e)
        else:
            print("CSM%d GPIO_R_DIRECTION = %08x" %(which_csm, data))
            print("SCA GPIO init succeeds for CSM%d"%which_csm)

    def Mezz_enable(self,i,which_csm=0):        
        data = (1<<SCA_share.GPIO_JTAG_RESET)+(1<<self.gpio_tdc_mapping[i])
        SCA_config(self.ser,sca_enable=0b0100,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.GPIO_CHANNEL,datalen=4,command=SCA_share.GPIO_W_DATAOUT,data=data)        
        try:
            [SCA_channel,error,data]=SCA_config(self.ser,sca_enable=0b0100,which_csm=which_csm,sca_address=0x00,transID=self.get_transID(),channel=SCA_share.GPIO_CHANNEL,datalen=2,command=SCA_share.GPIO_R_DATAOUT,data=data)
        except Exception as e:
            print("GPIO_R_DATAOUT failed for CSM%d" %(which_csm))
            print(e)
        else:
            print("CSM%d GPIO_R_DATAOUT = %08x" %(which_csm,data))



    def config_single(self, ir, TDC_inst,which_csm):
         # Mapping of IR values to corresponding TDC register data
        ir_to_dr_mapping = {
            SCA_share.TDC_SETUP0: TDC_inst.setup_0,
            SCA_share.TDC_SETUP1: TDC_inst.setup_1,
            SCA_share.TDC_SETUP2: TDC_inst.setup_2,
            SCA_share.TDC_CONTROL0: TDC_inst.control_0,
            SCA_share.TDC_CONTROL1: TDC_inst.control_1,
            SCA_share.TDC_IDCODE: TDC_inst.IDCODE,
            SCA_share.TDC_STATUS0: TDC_inst.status_0,
            SCA_share.TDC_STATUS1: TDC_inst.status_1, 
            SCA_share.ASD_WRITE: (TDC_inst.ASD2.setup,TDC_inst.ASD1.setup,TDC_inst.ASD0.setup),
            SCA_share.ASD_READ: (TDC_inst.ASD2.setup,TDC_inst.ASD1.setup,TDC_inst.ASD0.setup)
        }

        ir_value = format(ir, 'b').zfill(SCA_share.TDC_IR_LEN)
        dr_value = ''
        isASD = ir in (SCA_share.ASD_WRITE, SCA_share.ASD_READ)

        if ir in ir_to_dr_mapping:
            if isASD:
                # Handle ASD_WRITE and ASD_READ differently to add extra bit
                for ASD in ir_to_dr_mapping[ir]:
                    for s in ASD:
                        dr_value += ''.join(s)
                # print(self.dr_value)
                # print(len(self.dr_value))
                # print("ASD2 in DR seq: "+format(int(self.dr_value[0:55],2),'x'))
                # print("ASD1 in DR seq: "+format(int(self.dr_value[55:55*2],2),'x'))
                # print("ASD0 in DR seq: "+format(int(self.dr_value[55*2:55*3],2),'x'))

            else:
                for s in ir_to_dr_mapping[ir]:
                    dr_value += ''.join(s)
        else:
            print(f"Unsupported IR value: {ir}")
            return

        # print(self.dr_value)
        # print("TDO prepared to write: %d bits" %len(self.dr_value))
        # print("TDO DR seq: %x" %int(self.dr_value, 2))

        try:
            tdi=SCA_JTAG_go(self.ser,transID=self.get_transID(),sca_enable=0b0010,which_csm=which_csm,ir_value=ir_value,dr_value=dr_value,isASD=isASD)
        except Exception as e:
            print("mezz config failed for CSM%d" %(which_csm))
            print(e)
        else:
            return tdi


    # def open_Mezz_config(self):
    #     # Create and show the TDC configuration popup window
    #     self.mezz_popup = pop_mezz(self.TDC_inst)
    #     self.mezz_popup.exec_()  # exec_() for modal behavior, show() for non-modal

    def TDC_setup_dialog(self):
        # Create and show the TDC configuration popup window
        self.TDC_setup_popup = pop_mezz_setup(self.CSM_list)
        self.TDC_setup_popup.exec_()  # exec_() for modal behavior, show() for non-modal

    def get_dline_status(self):
        fpga_config(self.ser, 1, [5])  #select uart mapping FPGA readback
        bin_str=fpga_readback(self.ser, 0)

        locked_dline0 = bin_str[-94:-84]+bin_str[-74:-64]+bin_str[-54:-44]+bin_str[-34:-24]
        locked_dline1 = bin_str[-104:-94]+bin_str[-84:-74]+bin_str[-64:-54]+bin_str[-44:-34]
        locked_dline = ''.join(str(int(a) & int(b)) for a, b in zip(locked_dline0, locked_dline1))
        locked_dline_reversed = locked_dline[::-1]
        locked_dline_diff = ''.join(str(int(a) ^ int(b)) for a, b in zip(locked_dline0, locked_dline1))
        print("locked_dline0="+locked_dline0)
        print("locked_dline1="+locked_dline1)
        print("locked_dline ="+locked_dline)
        for j in range(self.CSM_per_MiniDAQ):
            for i in range(self.mezz_per_CSM):
                if self.csm_checkbox_list[j][i].isChecked():
                    if locked_dline_reversed[i+20*j]=='1':
                        self.csm_indicator_list[j][i][2].setStyleSheet("background-color: green; border: 1px solid black;")
                    else:
                        self.csm_indicator_list[j][i][2].setStyleSheet("background-color: red; border: 1px solid black;")
    
    def reset_status(self):
        for j in range(self.CSM_per_MiniDAQ):
            for i in range(self.mezz_per_CSM):
                for k in range(3):
                    self.csm_indicator_list[j][i][k].setStyleSheet("background-color: lightgray; border: 1px solid black;")

    def fpga_global_reset(self):
        fpga_config(self.ser, 5, [1])
        fpga_config(self.ser, 5, [0])