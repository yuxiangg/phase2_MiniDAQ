from PyQt5.QtCore import *
import subprocess
import time
import sys
from TDC_config_low_level_function import *
import numpy as np



class Worker_load_CSM_bit(QObject):
    finished = pyqtSignal()
    message_out = pyqtSignal(str)
    progress_signal = pyqtSignal(int)  # Signal to update progress bar

    def __init__(self,tab_lpgbt,which_csm,file_name):
        super(Worker_load_CSM_bit, self).__init__()
        self.ser = tab_lpgbt.ser
        self.tab_lpgbt = tab_lpgbt
        self.which_csm = which_csm
        self.file_name = file_name


    def run(self):

        start_time = time.time()  # Record start time
        time_TMS = 0
        time_TDO = 0
        time_JTAG_go = 0
        time_JTAG_busy = 0
        

        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        SCA_clk_div(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=self.which_csm,data=0)
        file_path = self.file_name
        # # file_path = r'../lpGBT_CSM_prod/firmware/lpgbtcsm_fpga_top.bit'
        # file_path = r'/home/muonuser/Downloads/lpgbtcsm_fpga_top.bit'
        # file_path = r'/home/muonuser/gitfolder/firm_lpgbtCsmProd/lpgbtcsm_prod.runs/impl_1/lpgbtcsm_fpga_top.bit'
        tdo_ints, tms_ints = self.tab_lpgbt.FPGA_jtag.write_bit_file_full_sequence(file_path)
        self.message_out.emit("Bit file loaded. Now start to config the FPGA.")
        index = 0
        previous_TMS = []
        current_TMS = []
        SCA_JTAGset(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=self.which_csm,data_len=128)
        while index < len(tms_ints):
        # while index < 2:
            remaining = len(tms_ints) - index
            length = remaining if remaining < 4 else 4
            current_TMS = tms_ints[index:index+length]
            # if remaining%1000<4:
            #     self.message_out.emit(str(f"{remaining} integers left"))
            self.progress_signal.emit(int(index*100)//len(tms_ints))
            if np.array_equal(current_TMS, previous_TMS):
                # self.message_out.emit("TMS equal found")
                pass
            else:
                # self.message_out.emit("New TMS found")
                time_tmp = time.time()
                write_TMS_integ(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=self.which_csm,data=tms_ints[index:index+length])
                time_TMS += time.time() - time_tmp
            # TDO will be overwritten by the received TDI, so TDO must be updated for each cycle
            time_tmp = time.time()
            write_TDO_integ(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=self.which_csm,data=tdo_ints[index:index+length])
            time_TDO += time.time() - time_tmp
            if length!=4:               
                SCA_JTAGset(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=self.which_csm,data_len=length*32)
            # SCA_config(self.ser,sca_enable=0b0001,which_csm=self.which_csm,sca_address=0x00,transID=10,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_GO,data=0)
            # _,_,readback_data = SCA_config(self.ser,sca_enable=0b0001,which_csm=self.which_csm,sca_address=0x00,transID=10,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_CTRL,data=0,read_required=1)
            time_tmp = time.time()
            SCA_config_JTAG_GO_REQ(self.ser,command=1,read_required=0)
            time_JTAG_go += time.time() - time_tmp
            time_tmp = time.time()
            _,_,readback_data = SCA_config_JTAG_GO_REQ(self.ser,command=0,read_required=1)
            time_JTAG_busy += time.time() - time_tmp

            while readback_data<0 or format(readback_data,'b').zfill(32)[-9] == '1':
                if readback_data<0:
                    self.message_out.emit("Readback empty for SCA JTAG_R_CTRL")
                else:
                    self.message_out.emit(f"SCA JTAG busy, readback_data= {readback_data}")
                _,_,readback_data = SCA_config_JTAG_GO_REQ(self.ser,command=0,read_required=1) 
                # _,_,readback_data = SCA_config(self.ser,sca_enable=0b0001,which_csm=self.which_csm,sca_address=0x00,transID=10,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_R_CTRL,data=0)


            index += 4
            previous_TMS = current_TMS
        self.message_out.emit("Bit file programmed.")

        end_time = time.time()  # Record end time
        runtime = end_time - start_time  # Compute runtime
        self.message_out.emit(f"Runtime: {runtime:.2f}s, TMS: {time_TMS:.2f}s, TDO: {time_TDO:.2f}s, GO: {time_JTAG_go:.2f}s, Busy: {time_JTAG_busy:.2f}s")

        self.progress_signal.emit(100)
        self.finished.emit()
