import re
import os
from SCA_share import *
import numpy as np

class JTAGTAPController:
    def __init__(self):
        """
        Initialize the JTAG TAP controller with the starting state.
        """
        # Define the initial TAP state
        self.state = "Run-Test/Idle"
        self.state_transitions = self._build_state_transitions()

    def _build_state_transitions(self):
        """
        Define the JTAG state machine transitions based on TMS values.

        Returns:
            dict: A dictionary representing state transitions for each state
                  based on the TMS bit (0 or 1).
        """
        return {
            "Test-Logic-Reset": {0: "Run-Test/Idle", 1: "Test-Logic-Reset"},
            "Run-Test/Idle": {0: "Run-Test/Idle", 1: "Select-DR-Scan"},
            "Select-DR-Scan": {0: "Capture-DR", 1: "Select-IR-Scan"},
            "Capture-DR": {0: "Shift-DR", 1: "Exit1-DR"},
            "Shift-DR": {0: "Shift-DR", 1: "Exit1-DR"},
            "Exit1-DR": {0: "Pause-DR", 1: "Update-DR"},
            "Pause-DR": {0: "Pause-DR", 1: "Exit2-DR"},
            "Exit2-DR": {0: "Shift-DR", 1: "Update-DR"},
            "Update-DR": {0: "Run-Test/Idle", 1: "Select-DR-Scan"},
            "Select-IR-Scan": {0: "Capture-IR", 1: "Test-Logic-Reset"},
            "Capture-IR": {0: "Shift-IR", 1: "Exit1-IR"},
            "Shift-IR": {0: "Shift-IR", 1: "Exit1-IR"},
            "Exit1-IR": {0: "Pause-IR", 1: "Update-IR"},
            "Pause-IR": {0: "Pause-IR", 1: "Exit2-IR"},
            "Exit2-IR": {0: "Shift-IR", 1: "Update-IR"},
            "Update-IR": {0: "Run-Test/Idle", 1: "Select-DR-Scan"}
        }

    def apply_tms(self, tms, tdi_sequence, tms_sequence, tdi_mask, tdi='0',tdi_valid=''):
        """
        Move to the next state based on the current TMS bit, pad TDI with '0' for non-shifting states,
        and update the TDI_mask accordingly.

        Args:
            tms (int): The TMS bit (0 or 1).
            tdi_sequence (list): The TDI sequence to be padded.
            tms_sequence (list): The TMS sequence to be appended.
            tdi_mask (list): The TDI mask sequence to be updated.
        """
        tms_sequence[0]+=tms
        tdi_sequence[0]+=tdi

        # Pad TDI with '1' (don't care) bit and set mask to '0' for each TMS transition in non-shifting states
        if len(tdi_valid):
            tdi_mask[0]+=tdi_valid
        else:
            if self.state not in ["Shift-DR"]:
                tdi_mask[0]+='0'  # Mark as 'don't care' in the mask
            else:
                tdi_mask[0]+='1'  # Mark as 'valid' in the mask
        self.state = self.state_transitions[self.state][int(tms)]

    def shift_ir(self, instruction):  # starts from RTI and ends at RTI
        """
        Shift an instruction into the IR register and produce TDI, TMS, and TDI_mask sequences.

        Args:
            instruction (int): Instruction bits to shift in.
            ir_length (int): Length of the IR register in bits.

        Returns:
            list, list, list: TDI, TMS, and TDI_mask sequences for IR shift.
        """
        tdi_sequence = ['']
        tms_sequence = ['']
        tdi_mask = ['']

        # Move from Run-Test/Idle to Select-DR-Scan, then to Select-IR-Scan
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask)  # enter Select-DR-Scan
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask)  # enter Select-IR-Scan
        self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask)  # enter Capture-IR
        self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask)  # enter Shift-IR

        # Shift the instruction into IR
        for i in range(len(instruction)-1):
            self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask, instruction[-i-1])  # Shift-IR

        
        # Exit Shift-IR state
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask, instruction[0])  # Exit1-IR
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask)  # Update-IR
        self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask)  # Run-Test/Idle

        return tdi_sequence, tms_sequence, tdi_mask

    def shift_dr(self, data, isASD=False):  #data is MSB first, so need a reverse when generating JTAG sequence
        """
        Shift data into the DR register and produce TDI, TMS, and TDI_mask sequences.

        Args:
            data (int): Data bits to shift in.
            dr_length (int): Length of the DR register in bits.

        Returns:
            list, list, list: TDI, TMS, and TDI_mask sequences for DR shift.
        """
        tdi_sequence = ['']
        tms_sequence = ['']
        tdi_mask = ['']

        # Enter DR shift state
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask)  # entering Select-DR-Scan
        self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask)  # entering Capture-DR
        self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask)  # entering Shift-DR
        if isASD: 
            #add one additional bits for ASD shifting at the beginning. This is due to a SCLK block at the beginning.
            self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask,tdi='0',tdi_valid='0')  # entering Shift-DR
            # Add a known extra bit for ASD TDO reg
            self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask,tdi='0',tdi_valid='0')  # entering Shift-DR

        # Shift the data into DR
        for i in range(len(data)-1):
            self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask, data[-i-1])  # Shift-DR

        # Exit Shift-DR state
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask, data[0])  # Exit1-DR
        self.apply_tms('1', tdi_sequence, tms_sequence, tdi_mask)  # Update-DR
        self.apply_tms('0', tdi_sequence, tms_sequence, tdi_mask)  # Run-Test/Idle

        return tdi_sequence, tms_sequence, tdi_mask

    def full_sequence(self, ir_value, dr_value, isASD=False):
        """
        Generate the full TDI, TMS, and TDI_mask sequences for shifting IR, DR, and returning to Run-Test/Idle.

        Args:
            ir_value (int): The instruction value for IR shift.
            dr_value (int): The data value for DR shift.
            ir_length (int): The length of the instruction register in bits.
            dr_length (int): The length of the data register in bits.

        Returns:
            list, list, list: The combined TDI, TMS, and TDI_mask sequences for the entire operation.
        """
        # Shift IR and get the TDI, TMS, and TDI_mask sequences
        ir_tdi, ir_tms, ir_mask = self.shift_ir(ir_value)

        # Shift DR and get the TDI, TMS, and TDI_mask sequences
        dr_tdi, dr_tms, dr_mask = self.shift_dr(dr_value,isASD)

        # Combine TDI, TMS, and TDI_mask sequences
        tdi_RTI  = '000000'
        tms_RTI  = '111110'
        mask_RTI = '000000'

        combined_tdi = tdi_RTI + ir_tdi[0] + dr_tdi[0]
        combined_tms = tms_RTI + ir_tms[0] + dr_tms[0]
        combined_mask = mask_RTI + ir_mask[0] + dr_mask[0]
        return combined_tdi, combined_tms, combined_mask

    def chunk_to_32bit_ints(self,bit_str):
        bit_str = bit_str.ljust((len(bit_str) + 31) // 32 * 32, '0')  # Pad to a multiple of 32 bits
        return np.array([int(bit_str[i:i+32], 2) for i in range(0, len(bit_str), 32)], dtype=np.uint32)

    def write_bit_file_full_sequence(self, file_path):
        # UG470 table 10-4 Single Device Configuration Sequence

        tdi_out  = ['000000']  # return to RTI
        tms_out  = ['111110']

        # Shift IR for FPGA_JPROGRAM
        tdi, tms, _ = self.shift_ir(format(SCA_share.FPGA_JPROGRAM, 'b').zfill(SCA_share.FPGA_IR_LEN))
        tdi_out[0]  += tdi[0]
        tms_out[0]  += tms[0]

        # Convert outputs into 32-bit chunks and store as numpy.uint32
        tdi_out_ints = self.chunk_to_32bit_ints(tdi_out[0])
        tms_out_ints = self.chunk_to_32bit_ints(tms_out[0])
        
        # Append zeros to maintain RTI state for a minimum 10 ms
        tdi_out_ints = np.concatenate((tdi_out_ints, np.zeros((1000,), dtype=np.uint32)))
        tms_out_ints = np.concatenate((tms_out_ints, np.zeros((1000,), dtype=np.uint32)))
        
        # Shift IR for FPGA_CFG_IN
        tdi, tms, _ = self.shift_ir(format(SCA_share.FPGA_CFG_IN, 'b').zfill(SCA_share.FPGA_IR_LEN))
        
        # Enter DR shift state
        self.apply_tms('1', tdi, tms, [''])  # Select-DR-Scan
        self.apply_tms('0', tdi, tms, [''])  # Capture-DR
        self.apply_tms('0', tdi, tms, [''])  # Shift-DR
        # print(tdi)
        # print(tms)
        
        # Append new TDI and TMS values. Additional 0s are introduced to the front, continuing the RTI state
        tdi_out_ints = np.concatenate((tdi_out_ints, np.array([int(tdi[0], 2)], dtype=np.uint32)))
        tms_out_ints = np.concatenate((tms_out_ints, np.array([int(tms[0], 2)], dtype=np.uint32)))
        
        # # Debugging output
        # print(tdi_out_ints)
        # print(tms_out_ints)

        # Load bit file and append content
        try:
            file_size = os.path.getsize(file_path)
            print(f"{file_size} total bytes in file "+file_path)
            tdi = ['']
            tms = ['']
            with open(file_path, "rb") as f:
                loaded_bit_data = np.fromfile(f, dtype=np.dtype('>u4'))
                # print("loaded file first 10 integers")
                # print(format(loaded_bit_data[0],'08x'))
                tdi_out_ints = np.concatenate((tdi_out_ints, loaded_bit_data))
                tms_out_ints = np.concatenate((tms_out_ints, np.zeros(len(loaded_bit_data), dtype=np.uint32)))

                # Check for leftover bytes
                remaining_bytes = file_size % 4
                if remaining_bytes:
                    extra_data = f.read(remaining_bytes)
                    
                    # print(f"{remaining_bytes} extra bytes detected in file.")
                    # special handling for last byte
                    tdi[0] = ''.join(format(byte, '08b') for byte in extra_data)
                    tms[0] = format(1, 'b').zfill(8*remaining_bytes)
                else:
                    # print(f"bit file ends with a full 4 bytes. ")
                    tms_out_ints[-1] = 1  #append '1' at the end of tms for the last Shift-DR 
     
        except FileNotFoundError:
            print("Error: The file was not found.")
        except Exception as e:
            print(f"An error occurred: {e}")
        else:
            self.apply_tms('1', tdi, tms, [''])  # Update-DR
            self.apply_tms('1', tdi, tms, [''])  # Run-Test/Idle

        tdi_tmp, tms_tmp, _ = self.shift_ir(format(SCA_share.FPGA_JSTART,'b').zfill(SCA_share.FPGA_IR_LEN))

        tdi[0]  += tdi_tmp[0]
        tms[0]  += tms_tmp[0]


        tdi_out_ints = np.concatenate((tdi_out_ints, self.chunk_to_32bit_ints(tdi[0])))
        tms_out_ints = np.concatenate((tms_out_ints, self.chunk_to_32bit_ints(tms[0])))

        # Append zeros to maintain RTI state for a minimum 2 ms
        tdi_out_ints = np.concatenate((tdi_out_ints, np.zeros((100,), dtype=np.uint32)))
        tms_out_ints = np.concatenate((tms_out_ints, np.zeros((100,), dtype=np.uint32)))



        # self.apply_tms('1', tdi_out, tms_out, mask_out)  # entering select-DR
        # self.apply_tms('1', tdi_out, tms_out, mask_out)  # entering select-IR
        # self.apply_tms('1', tdi_out, tms_out, mask_out)  # entering Test-logic-reset
        tdi_out_ints = np.concatenate((tdi_out_ints, self.chunk_to_32bit_ints('000'))) #don't care bits
        tms_out_ints = np.concatenate((tms_out_ints, self.chunk_to_32bit_ints('111'))) #RTI to TLR

        return tdi_out_ints, tms_out_ints


# Example Usage
if __name__ == "__main__":
    jtag_tap = JTAGTAPController()

    # Example Instruction Register (IR) and Data Register (DR) values
    ir_value = '10001'  # Example binary value for IR
    dr_value = 32*'0'  # Example binary value for DR
    # ir_length = 5  # Example IR length in bits
    # dr_length = 9  # Example DR length in bits

    # Generate the full TDI, TMS, and TDI_mask sequences
    full_tdi, full_tms, full_mask = jtag_tap.full_sequence(ir_value, dr_value)

    print("Full TDI Sequence:", full_tdi)
    print("Full TMS Sequence:", full_tms)
    print("TDI Mask Sequence:", full_mask)
