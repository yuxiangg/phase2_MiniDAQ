from PyQt5 import QtGui, QtCore, QtWidgets
import sys

from tab_sca import tab_sca
from tab_lpgbt import tab_lpgbt
from tab_csm import tab_csm
from tab_mezz import tab_mezz
from datetime import datetime

class Ui_MainWindow():
    
    def __init__(self,ser):
        self.ser = ser


    def setupUi(self, MainWindow):
        
        MainWindow.setWindowTitle("MiniDAQ")
        
        # Setup main layout
        self.centralWidget = QtWidgets.QWidget()
        MainWindow.setCentralWidget(self.centralWidget)

        mainLayout = QtWidgets.QVBoxLayout(self.centralWidget)

        self.tabWidget = QtWidgets.QTabWidget()
        self.tabWidget.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)
        mainLayout.addWidget(self.tabWidget)


        self.pushButton_clearinfo = QtWidgets.QPushButton()
        self.pushButton_clearinfo.setText("clear info")
        self.pushButton_clearinfo.clicked.connect(self.clear_info)
        mainLayout.addWidget(self.pushButton_clearinfo)


        label_TextBrowser = QtWidgets.QLabel()
        label_TextBrowser.setText("Output Log")
        label_TextBrowser.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        mainLayout.addWidget(label_TextBrowser)
        
        self.textBrowser = QtWidgets.QTextBrowser()
        self.textBrowser.setMinimumHeight(100)
        self.textBrowser.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)


        mainLayout.addWidget(self.textBrowser)

        # Setup tabs

        lpgbt_tab = QtWidgets.QWidget()
        self.tabWidget.addTab(lpgbt_tab, "LpGBT Config")
        self.tab_lpgbt_inst = tab_lpgbt(lpgbt_tab,self.ser)


        sca_tab = QtWidgets.QWidget()
        self.tabWidget.addTab(sca_tab, "Mezz Monitor")
        self.tab_sca_inst = tab_sca(sca_tab,self.ser)

        csm_tab = QtWidgets.QWidget()
        self.tabWidget.addTab(csm_tab, "CSM Monitor")
        self.tab_csm_inst = tab_csm(csm_tab,self.ser)

        mezz_tab = QtWidgets.QWidget()
        self.tabWidget.addTab(mezz_tab, "Mezz Config")
        self.mezz_tab_inst = tab_mezz(mezz_tab,self.ser)

        # tdc_tab = QtWidgets.QWidget()
        # self.tabWidget.addTab(tdc_tab, "TDC Config")
        # self.tdc_tab_inst = tab_TDC(tdc_tab)


    def clear_info(self):
        self.textBrowser.clear()
      
