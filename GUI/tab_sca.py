from PyQt5 import QtCore, QtGui, QtWidgets

from TDC_config_low_level_function import *

import time
import datetime
import pandas as pd

from Worker_SCA import *

class mezz_cell(object):
    def __init__(self):
        self.name_label = QtWidgets.QLabel('0')
        self.checkBox = QtWidgets.QCheckBox()
        self.AVDD_raw = 0
        self.AVDD_return = 0
        self.DVDD_raw = 0
        self.DVDD_return = 0
        self.temp_raw = 0
        self.temp_return = 0
        self.AVDD_cal = QtWidgets.QLabel('0')
        self.DVDD_cal = QtWidgets.QLabel('0')
        self.temp_cal = QtWidgets.QLabel('0')
        self.value_list = [self.AVDD_raw,self.AVDD_return,self.DVDD_raw,self.DVDD_return,self.temp_raw,self.temp_return]
        self.label_list = [self.AVDD_cal,self.DVDD_cal,self.temp_cal]

    def add_to_layout(self,layout,row,column):
        layout.addWidget(self.name_label,   row,    column, 1, 1)
        layout.addWidget(self.checkBox,     row+1,  column, 1, 1)
        layout.addWidget(self.AVDD_cal,     row+2,  column, 1, 1)
        layout.addWidget(self.DVDD_cal,     row+3,  column, 1, 1)
        layout.addWidget(self.temp_cal,     row+4,  column, 1, 1)


class tab_sca(object):


    def __init__(self, MainWindow,ser):
        self.transID = 0
        self.CSM_count = 2
        self.mezz_per_CSM = 20
        self.mezz_enable = '0'*self.CSM_count*self.mezz_per_CSM
        self.cell_list = [[],[]]
        self.ser = ser
        self.ADC_channel = 21
        self.setup_UI(MainWindow) 
        
    def setup_UI(self, MainWindow):

        sectionLayout = QtWidgets.QVBoxLayout(MainWindow)
        buttonWindow = QtWidgets.QWidget()
        buttonWindow.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        self.gridLayout = QtWidgets.QGridLayout(buttonWindow)

        column = 0
        row = 0
        self.pushButton_SCA_start = QtWidgets.QPushButton()
        self.pushButton_SCA_start.setText("SCA start")
        self.gridLayout.addWidget(self.pushButton_SCA_start,row,column,1,3)
        self.pushButton_SCA_start.clicked.connect(self.SCA_start)

        column = 0
        row += 1
        self.checkBox_checkCSM0 = QtWidgets.QCheckBox()
        self.checkBox_checkCSM0.setText("check all")
        self.gridLayout.addWidget(self.checkBox_checkCSM0,   row+1,    0, 1, 1)
        self.checkBox_checkCSM0.stateChanged.connect(self.mezz_check_all_CSM0)
        self.checkBox_checkCSM1 = QtWidgets.QCheckBox()
        self.checkBox_checkCSM1.setText("check all")
        self.gridLayout.addWidget(self.checkBox_checkCSM1,   row+6,    0, 1, 1)
        self.checkBox_checkCSM1.stateChanged.connect(self.mezz_check_all_CSM1)

        for j in range(self.CSM_count):
            for i in range(self.mezz_per_CSM):
                cell = mezz_cell()
                if i == 0: # add first column label
                    label = QtWidgets.QLabel('AVDD(V)')
                    self.gridLayout.addWidget(label,  row+2,  column, 1, 1)
                    label = QtWidgets.QLabel('DVDD(V)')
                    self.gridLayout.addWidget(label,  row+3,  column, 1, 1)
                    label = QtWidgets.QLabel('Temp(C)')
                    self.gridLayout.addWidget(label,  row+4,  column, 1, 1)

                if i == self.mezz_per_CSM:
                    row += 5
                    column = 0

                cell.name_label.setText("Mezz%d"%(i+self.mezz_per_CSM*j))
                if self.mezz_enable[i+j*self.mezz_per_CSM] == '1':
                    cell.checkBox.setChecked(True)
                cell.checkBox.stateChanged.connect(self.mezz_enable_update)
                cell.add_to_layout(self.gridLayout,row,column+1)
                self.cell_list[j].append(cell)
                column += 1
            row += 5
            column = 0

        sectionLayout.addWidget(buttonWindow)


    def get_transID(self):
        if self.transID<0xff:
            self.transID += 1
        else:
            self.transID = 1
        return self.transID


    def mezz_enable_update(self):
        enable = ''
        for j in range(self.CSM_count):
            for cell in self.cell_list[j]:
                if cell.checkBox.isChecked():
                    enable +='1'
                else:
                    enable +='0'
            self.mezz_enable = enable
        # print(self.mezz_enable)

    def mezz_check_all_CSM0(self):
        if self.checkBox_checkCSM0.isChecked():
            for cell in self.cell_list[0]:
                cell.checkBox.setChecked(True)
        else:
            for cell in self.cell_list[0]:
                cell.checkBox.setChecked(False)
        print(self.mezz_enable)

    def mezz_check_all_CSM1(self):
        if self.checkBox_checkCSM1.isChecked():
            for cell in self.cell_list[1]:
                cell.checkBox.setChecked(True)
        else:
            for cell in self.cell_list[1]:
                cell.checkBox.setChecked(False)
        print(self.mezz_enable)
                

    # def sca_reset(self):
    #     SCA_start_reset(self.ser,self.sca_enable,verbose=1)

    # def sca_tx_rx_reset(self):
    #     SCA_tx_rx_reset(self.ser,self.sca_enable)


    def print_message(self,str):
        print(str)

    def SCA_start(self):
        self.pushButton_SCA_start.setEnabled(False)
        # Step 2: Create a QThread object
        self.thread_sca = QThread()
        # Step 3: Create a worker object
        self.worker_sca = Worker_SCA(self)
        # Step 4: Move worker to the thread
        self.worker_sca.moveToThread(self.thread_sca)
        # Step 5: Connect signals and slots
        # self.thread_sca.started.connect(self.disable_sca_button)
        self.thread_sca.started.connect(self.worker_sca.run)

        self.thread_sca.finished.connect(self.thread_sca.deleteLater)
        self.thread_sca.finished.connect(self.enable_sca_button)

        # self.thread_sca.finished.connect(self.print_res_result)

        self.worker_sca.message_out.connect(self.print_message)

        self.worker_sca.finished.connect(self.thread_sca.quit)
        self.worker_sca.finished.connect(self.worker_sca.deleteLater)

        # Step 6: Start the thread_sca
        self.thread_sca.start()

    def disable_sca_button(self):
        self.pushButton_SCA_start.setEnabled(False)

    def enable_sca_button(self):
        self.pushButton_SCA_start.setEnabled(True)


