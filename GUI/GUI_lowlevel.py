from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from functools import partial

def create_checkbox(label, row, column, width, slot_function, setchecked=False, layout=None):
    if layout is None:
        raise ValueError("Layout must be provided for checkbox placement.")
    checkbox = QtWidgets.QCheckBox()
    checkbox.setText(label)
    checkbox.setChecked(setchecked)
    layout.addWidget(checkbox, row, column, 1, width)
    checkbox.stateChanged.connect(slot_function)
    return checkbox

def create_button(label, row, column, width, slot_function, layout=None):
    if layout is None:
        raise ValueError("Layout must be provided for button placement.")
    button = QtWidgets.QPushButton()
    button.setText(label)
    layout.addWidget(button, row, column, 1, width)
    button.clicked.connect(slot_function)
    return button