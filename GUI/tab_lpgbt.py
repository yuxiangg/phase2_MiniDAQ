from PyQt5 import QtCore, QtGui, QtWidgets

from TDC_config_low_level_function import *
from lpgbt_register_map_v1 import LpgbtRegisterMapV1
from JTAGTAPController import *
from pop_miniDAQ_setup import pop_miniDAQ_setup
from GUI_lowlevel import *
from MiniDAQ_reg import *

import time
import datetime
import pandas as pd

from Worker_lpGBT import *
from Worker_load_CSM_bit import *


class tab_lpgbt(object):
    """docstring for rad_tab"""

    def __init__(self, MainWindow, ser):
        self.ser = ser
        self.worker_lpGBT = Worker_lpGBT(ser)
        self.lpgbt_master_addr = 117
        self.lpgbt_slave_addr = 115
        self.FPGA_jtag = JTAGTAPController()
        self.miniDAQ_reg = MiniDAQ_reg()
        self.default_file = '/home/muonuser/gitfolder/phase2_MiniDAQ/lpGBT_CSM_prod/firmware/lpgbtcsm_fpga_top.bit'
        self.setup_UI(MainWindow)

    def setup_UI(self, MainWindow):
        sectionLayout = QtWidgets.QVBoxLayout(MainWindow)
        buttonWindow = QtWidgets.QWidget()
        buttonWindow.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred)
        self.gridLayout = QtWidgets.QGridLayout(buttonWindow)
        self.uplink_status_component=[]

        row = 0
        column = 0
        label = QtWidgets.QLabel("CSM0 Master")
        self.gridLayout.addWidget(label, row, column, 1, 1)
        self.uplink_status_component.append(label)

        column += 1
        checkbox=create_checkbox("CSM0 Slave", row, column, 1, partial(self.slave_control, 0), setchecked=False, layout=self.gridLayout)
        self.uplink_status_component.append(checkbox)

        column += 1
        label = QtWidgets.QLabel("CSM1 Master")
        self.gridLayout.addWidget(label, row, column, 1, 1)
        self.uplink_status_component.append(label)

        column += 1
        checkbox=create_checkbox("CSM1 Slave", row, column, 1, partial(self.slave_control, 1), setchecked=False, layout=self.gridLayout)
        self.uplink_status_component.append(checkbox)


        row += 1
        column = 0
        create_button("Load", row, column, 1, partial(self.load_lpgbt,0,1), self.gridLayout)

        column += 1
        create_button("Load", row, column, 1, partial(self.load_lpgbt,0,0), self.gridLayout)

        column += 1
        create_button("Load", row, column, 1, partial(self.load_lpgbt,1,1), self.gridLayout)

        column += 1
        create_button("Load", row, column, 1, partial(self.load_lpgbt,1,0), self.gridLayout)

        column += 1
        create_button("update status", row, column, 1, self.update_status, layout=self.gridLayout)

        # column += 1
        # self.checkBox_enable_vtrx_slave0 = QtWidgets.QCheckBox()
        # self.checkBox_enable_vtrx_slave0.setText("VTRX Slave")
        # self.checkBox_enable_vtrx_slave0.setChecked(False)
        # self.checkBox_enable_vtrx_slave0.stateChanged.connect(self.slave_control0)
        # self.gridLayout.addWidget(self.checkBox_enable_vtrx_slave0, row, column, 1, 1)

        row += 1
        column = 0
        create_checkbox("prbs31", row, column, 1, partial(self.set_uplink_prbs, 0, self.lpgbt_master_addr), setchecked=bool(False), layout=self.gridLayout)

        column += 1
        create_checkbox("prbs31", row, column, 1, partial(self.set_uplink_prbs, 0, self.lpgbt_slave_addr), setchecked=bool(False), layout=self.gridLayout)

        column += 1
        create_checkbox("prbs31", row, column, 1, partial(self.set_uplink_prbs, 1, self.lpgbt_master_addr), setchecked=bool(False), layout=self.gridLayout)

        column += 1
        create_checkbox("prbs31", row, column, 1, partial(self.set_uplink_prbs, 1, self.lpgbt_slave_addr), setchecked=bool(False), layout=self.gridLayout)

        # self.checkBox_enable_master_prbs0 = QtWidgets.QCheckBox()
        # self.checkBox_enable_master_prbs0.setText("prbs31")
        # self.checkBox_enable_master_prbs0.setChecked(False)
        # self.checkBox_enable_master_prbs0.stateChanged.connect(self.set_prbs)
        # self.gridLayout.addWidget(self.checkBox_enable_master_prbs0, row, column, 1, 1)

        row += 1
        column = 0
        create_button("Read CSM0 IDCODE", row, column, 1, partial(self.read_fpga_id, 0), self.gridLayout)

        column += 1
        self.pushButton_load_CSM0_bit=create_button("Load CSM0 bit", row, column, 1, partial(self.load_fpga_bit, 0), self.gridLayout)
        self.pushButton_load_CSM0_bit.setEnabled(False)

        column += 1
        create_button("Read CSM1 IDCODE", row, column, 1, partial(self.read_fpga_id, 1), self.gridLayout)

        column += 1
        self.pushButton_load_CSM1_bit=create_button("Load CSM1 bit", row, column, 1, partial(self.load_fpga_bit, 1), self.gridLayout)
        self.pushButton_load_CSM1_bit.setEnabled(False)

        row += 1
        column = 0

        create_button("Browse", row, column, 1, self.open_file_dialog, self.gridLayout)

        column += 1
        # Text box to show selected file
        self.file_path_box = QtWidgets.QLineEdit()
        self.file_path_box.setText(self.default_file)  # Set initial file
        self.gridLayout.addWidget(self.file_path_box,row,column,1,2)

        column += 2
        self.progress_bar = QtWidgets.QProgressBar()
        self.progress_bar.setValue(0)  # Initialize at 0%
        self.gridLayout.addWidget(self.progress_bar,row,column,1,2)

        row += 1
        column = 0
        create_button("Uplink Reset", row, column, 1, self.fpga_uplink_reset, self.gridLayout)

        column += 1
        create_button("Downlink Reset", row, column, 1, self.fpga_downlink_reset, self.gridLayout)

        column += 1
        create_button("SCA Reset", row, column, 1, self.fpga_sca_reset, self.gridLayout)

        column += 1
        create_button("Global Reset", row, column, 1, self.fpga_global_reset, self.gridLayout)

        row += 1
        column = 0
        create_button("MiniDAQ_setup", row, column, 1, self.miniDAQ_setup_dialog, self.gridLayout)



        sectionLayout.addWidget(buttonWindow)

    def update_status(self):
        fpga_config(self.ser, 1, [5])  #select uart mapping FPGA readback
        bin_str=fpga_readback(self.ser, 3)
        print(bin_str)

        print('Uplink ready = '+bin_str[-24:-16])
        print('Downlink ready = '+bin_str[-16:-8])
        print('Dline status= '+bin_str[-34:-24])
        print('Dline status= '+bin_str[-44:-34])
        print('Dline status= '+bin_str[-54:-44])
        print('Dline status= '+bin_str[-64:-54])
        print('Dline status= '+bin_str[-74:-64])
        print('Dline status= '+bin_str[-84:-74])
        print('Dline status= '+bin_str[-94:-84])
        print('Dline status= '+bin_str[-104:-94])
        # fpga_reg_reset(self.ser)
        # print("MiniDAQ reg reset")
        # bin_str_reversed = bin_str[::-1]
        # for i in range(16):
        #     print(bin_str_reversed[i*10:i*10+10])
        # uplink_ready = bin_str_reversed[80:84]
        # print("uplink_ready="+uplink_ready)
        # downlink_ready = bin_str_reversed[90:92]
        # print("downlink_ready="+downlink_ready)



    def load_lpgbt(self,which_csm,is_master):
        fpga_config(self.ser, 1, [which_csm*2+1-is_master])
        self.worker_lpGBT.load_lpgbt_config(which_csm=which_csm, is_master=is_master)

    # def slave_control0(self):
    #     self.slave_control(which_csm=self.comboBox_ICselect.currentIndex() // 2, set_enable=self.checkBox_enable_vtrx_slave0.isChecked())

    def set_prbs(self):
        lpgbt_addr = self.lpgbt_master_addr if (self.comboBox_ICselect.currentIndex()%2)==0 else self.lpgbt_slave_addr
        self.set_uplink_prbs(which_csm=self.comboBox_ICselect.currentIndex() // 2, lpgbt_addr=lpgbt_addr, set_enable=self.checkBox_enable_master_prbs0.isChecked())

    def set_uplink_prbs(self, which_csm, lpgbt_addr, state):
        set_enable = 1 if state == Qt.Checked else 0
        is_master = 1 if lpgbt_addr==self.lpgbt_master_addr else 0
        fpga_config(self.ser, 1, [which_csm*2+1-is_master])
        if set_enable:
            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ULDATASOURCE0.address, value_list=[0x04], which_csm=which_csm, lpgbt_addr=lpgbt_addr)
        else:
            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.ULDATASOURCE0.address, value_list=[0x00], which_csm=which_csm, lpgbt_addr=lpgbt_addr)

    def slave_control(self, which_csm, state):
        set_enable = 1 if state == Qt.Checked else 0
        fpga_config(self.ser, 1, [which_csm*2])  #select uart mapping CSM master
        lpgbt_read(self.ser, lpgbt_addr=self.lpgbt_master_addr, reg_addr=LpgbtRegisterMapV1.I2CM1TRANCNT.address, nb_to_be_read=1)
        lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1ADDRESS.address, value_list=[0x50], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # slave address for VTRX+

        lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1DATA0.address, value_list=[0b00001010], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # data0 for I2C M1 master control reg, set I2C M1 master frequency (0=100kHz, 1=200kHz, 2=400 KHz,3=1MHz)

        lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1CMD.address, value_list=[0x0], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # Write data0 to I2C M1 control reg

        lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1DATA0.address, value_list=[0x00], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # data0 for I2C M1 data reg

        if set_enable:
            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1DATA1.address, value_list=[0x03], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # data1 for I2C M1 data reg
        else:
            lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1DATA1.address, value_list=[0x01], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # data1 for I2C M1 data reg

        lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1CMD.address, value_list=[0x8], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # write data0 to multibyte write first byte
        lpgbt_config_write(self.ser, reg_addr=LpgbtRegisterMapV1.I2CM1CMD.address, value_list=[0xC], which_csm=which_csm, lpgbt_addr=self.lpgbt_master_addr)  # start multibyte write
        lpgbt_read(self.ser, lpgbt_addr=self.lpgbt_master_addr, reg_addr=LpgbtRegisterMapV1.I2CM1TRANCNT.address, nb_to_be_read=1)

    def set_ic_enable(self):
        value_list = []
        value_list.append(self.comboBox_ICselect.currentIndex())
        fpga_config(self.ser, 1, value_list)

    def fpga_uplink_reset(self):
        fpga_config(self.ser, 2, [1])
        fpga_config(self.ser, 2, [0])

    def fpga_downlink_reset(self):
        fpga_config(self.ser, 3, [1])
        fpga_config(self.ser, 3, [0])

    def fpga_sca_reset(self):
        fpga_config(self.ser, 4, [1])
        fpga_config(self.ser, 4, [0])

    def fpga_global_reset(self):
        fpga_config(self.ser, 5, [1])
        fpga_config(self.ser, 5, [0])

    def read_fpga_id(self,which_csm=0):

        fpga_config(self.ser, 1, [4])  #select uart mapping SCA
        SCA_connect(self.ser,sca_enable=0b1111,which_csm=which_csm,sca_address=0x00,verbose=1)
        SCA_start_reset(self.ser,sca_enable=0b1111,which_csm=which_csm,sca_address=0x00,verbose=1)
        SCA_tx_rx_reset(self.ser,sca_enable=0b1111,which_csm=which_csm,sca_address=0x00,verbose=1)
        SCA_JTAGset(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=which_csm,data_len=128)

        self.pushButton_load_CSM0_bit.setEnabled(False)
        self.pushButton_load_CSM1_bit.setEnabled(False)

        ir = SCA_share.FPGA_IDCODE
        ir_value = format(ir, 'b').zfill(SCA_share.FPGA_IR_LEN)
        dr_value = 32*'0'

        SCA_clk_div(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=which_csm,data=0)
        try:
            readback = SCA_JTAG_go(
                self.ser,transID=10,sca_enable=0b0001,which_csm=which_csm,ir_value=ir_value,dr_value=dr_value,isASD=False)
        except Exception as e:
            print(e)
        else:
            if format(int(readback[::-1],2),'x') == '362d093':
                if which_csm == 0:
                    self.pushButton_load_CSM0_bit.setEnabled(True)
                else:
                    self.pushButton_load_CSM1_bit.setEnabled(True)

    def print_message(self,str):
        print(str)

    def enable_load_CSM_bit_button(self):
        self.pushButton_load_CSM0_bit.setEnabled(True)
        self.pushButton_load_CSM1_bit.setEnabled(True)

    def update_progress(self, value):
        self.progress_bar.setValue(value)

    def load_fpga_bit(self,which_csm=0):
        self.pushButton_load_CSM0_bit.setEnabled(False)
        self.pushButton_load_CSM1_bit.setEnabled(False)
        # Step 2: Create a QThread object
        self.thread_load_CSM_bit = QThread()
        # Step 3: Create a worker object
        self.worker_load_CSM_bit = Worker_load_CSM_bit(self,which_csm,self.default_file)
        # Step 4: Move worker to the thread
        self.worker_load_CSM_bit.moveToThread(self.thread_load_CSM_bit)
        # Step 5: Connect signals and slots
        # self.thread_load_CSM_bit.started.connect(self.disable_sca_button)
        self.thread_load_CSM_bit.started.connect(self.worker_load_CSM_bit.run)

        self.thread_load_CSM_bit.finished.connect(self.thread_load_CSM_bit.deleteLater)
        self.thread_load_CSM_bit.finished.connect(self.enable_load_CSM_bit_button)

        # self.thread_load_CSM_bit.finished.connect(self.print_res_result)

        self.worker_load_CSM_bit.message_out.connect(self.print_message)
        self.worker_load_CSM_bit.progress_signal.connect(self.update_progress)

        self.worker_load_CSM_bit.finished.connect(self.thread_load_CSM_bit.quit)
        self.worker_load_CSM_bit.finished.connect(self.worker_load_CSM_bit.deleteLater)

        # Step 6: Start the thread_load_CSM_bit
        self.thread_load_CSM_bit.start()

    def load_fpga_bit_back(self):

        fpga_config(self.ser, 1, [4])  #select uart mapping SCA

        SCA_clk_div(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=0,data=0)
        file_path = r'../lpGBT_CSM_prod/firmware/lpgbtcsm_fpga_top.bit'
        tdo_ints, tms_ints = self.FPGA_jtag.write_bit_file_full_sequence(file_path)
        print("Bit file loaded. Now start to config the FPGA.")

        while len(tms_ints):
            print(str(len(tms_ints))+' integers left')
            length = len(tms_ints) if len(tms_ints)<4 else 4
            write_TMS_integ(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=0,data=tms_ints[0:length])
            write_TDO_integ(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=0,data=tdo_ints[0:length])
            SCA_JTAGset(ser=self.ser,transID=10,sca_enable=0b0001,which_csm=0,data_len=length*32)
            SCA_config(self.ser,sca_enable=0b0001,which_csm=0,sca_address=0x00,transID=10,channel=SCA_share.JTAG_CHANNEL,datalen=2,command=SCA_share.JTAG_GO,data=0)

            full_tms=full_tms[length:]
            full_tdo=full_tdo[length:]
        print("Bit file programmed.")

    def miniDAQ_setup_dialog(self):
        # Create and show the TDC configuration popup window
        self.miniDAQ_setup_popup = pop_miniDAQ_setup(self.ser,self.miniDAQ_reg)
        self.miniDAQ_setup_popup.exec_()  # exec_() for modal behavior, show() for non-modal

    def handle_checkbox(self, index, name, state):
        value = 1 if state == Qt.Checked else 0
        print(f"{name} changed to {value}")
        fpga_config(self.ser, index, [value])

    def open_file_dialog(self):
        options = QtWidgets.QFileDialog.Options()
        file_path, _ = QtWidgets.QFileDialog.getOpenFileName(
            None,
            "Select a File",
            self.default_file,  # Start with default file
            "Bitstream Files (*.bit);;All Files (*)",  # Filter to show only .bit files
            options=options
        )

        if file_path:  # If a file was selected
            self.file_path_box.setText(file_path)

