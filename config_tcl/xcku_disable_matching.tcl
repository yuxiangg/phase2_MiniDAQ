set_property OUTPUT_VALUE 1 [get_hw_probes tdc_decoder_top_inst/enable_internal_trigger -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/enable_internal_trigger} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]]


set_property OUTPUT_VALUE 20 [get_hw_probes tdc_decoder_top_inst/trigger_latency -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_trigger_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/trigger_latency} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_trigger_inst"}]]

set_property OUTPUT_VALUE 1 [get_hw_probes tdc_decoder_top_inst/hit_trigger_ratio -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/hit_trigger_ratio} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]]

source $origin_dir/xcku_global_reset.tcl

set_property OUTPUT_VALUE 0 [get_hw_probes tdc_decoder_top_inst/enable_matching -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_trigger_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/enable_matching} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_trigger_inst"}]]

