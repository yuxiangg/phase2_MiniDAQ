set_property OUTPUT_VALUE_RADIX UNSIGNED [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/main_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]]
set_property OUTPUT_VALUE 114 [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/main_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]]
commit_hw_vio [get_hw_probes {inst_Mezz_config/inst_mezz_config_vio/main_thr} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]]
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/channel_mode -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/chip_mode -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/deadtime -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/hyst_dac_reversed -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/int_gate -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/main_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/not_used -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/rundown_curr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/start_action_asd_read -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/start_action_asd_write -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
set_property NAME.SELECT short [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/wilk_thr -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_asd_setup_inst"}]] 
startgroup
set_property OUTPUT_VALUE 1 [get_hw_probes ASD_config -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
commit_hw_vio [get_hw_probes {ASD_config} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
endgroup
after 1000
startgroup
set_property OUTPUT_VALUE 0 [get_hw_probes ASD_config -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
commit_hw_vio [get_hw_probes {ASD_config} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
endgroup