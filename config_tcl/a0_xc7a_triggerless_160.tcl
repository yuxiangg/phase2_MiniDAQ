set origin_dir [file dirname [info script]]
source $origin_dir/xc7a_commit_hw_vio.tcl
source $origin_dir/xc7a_set_name_short.tcl
source $origin_dir/xc7a_common_setup.tcl
source $origin_dir/xc7a_disable_all_tdc_input.tcl
source $origin_dir/xc7a_single_edge_mode.tcl
source $origin_dir/xc7a_160Mbps.tcl
source $origin_dir/xc7a_triggerless.tcl
source $origin_dir/xc7a_setup_JTAG_vio.tcl 
source $origin_dir/xc7a_reset_JTAG_link.tcl
source $origin_dir/xc7a_config_TDC.tcl
source $origin_dir/0_config_asd_20mezz.tcl