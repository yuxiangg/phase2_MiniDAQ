commit_hw_vio [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"downlink[0].vio_downlink_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"downlink[1].vio_downlink_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"lpgbtFpga_top_inst/mgt_inst/uplink[0].drp_interface_inst/vio_drp_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"lpgbtFpga_top_inst/mgt_inst/uplink[1].drp_interface_inst/vio_drp_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"lpgbtFpga_top_inst/mgt_inst/uplink[2].drp_interface_inst/vio_drp_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"lpgbtFpga_top_inst/mgt_inst/uplink[3].drp_interface_inst/vio_drp_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/uplink[0].vio_BER_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/uplink[1].vio_BER_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/uplink[2].vio_BER_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/uplink[3].vio_BER_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_40M_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_trigger_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"uplink[0].vio_uplink_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"uplink[1].vio_uplink_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"uplink[2].vio_uplink_inst"}]\
  [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"uplink[3].vio_uplink_inst"}]