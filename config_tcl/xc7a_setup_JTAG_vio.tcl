set_property INPUT_VALUE_RADIX BINARY [get_hw_probes active_chains -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
set_property INPUT_VALUE_RADIX BINARY [get_hw_probes chain_success -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
set_property OUTPUT_VALUE_RADIX BINARY [get_hw_probes mezz_enable_vio -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
set_property INPUT_VALUE_RADIX UNSIGNED [get_hw_probes num_active_ports -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]

set_property OUTPUT_VALUE 010 [get_hw_probes clk_div -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
commit_hw_vio [get_hw_probes {clk_div} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"vio_top_inst"}]]
