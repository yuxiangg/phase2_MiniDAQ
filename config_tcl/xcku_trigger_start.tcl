set_property OUTPUT_VALUE 0 [get_hw_probes tdc_decoder_top_inst/enable_K28_1 -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_40M_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/enable_K28_1} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_40M_inst"}]]

source $origin_dir/xcku_global_reset.tcl


set_property OUTPUT_VALUE 0 [get_hw_probes tdc_decoder_top_inst/enable_internal_trigger -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/enable_internal_trigger} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_hit_inst"}]]
