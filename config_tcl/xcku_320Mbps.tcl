set_property OUTPUT_VALUE 1 [get_hw_probes tdc_decoder_top_inst/enable_320M -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_40M_inst"}]]
commit_hw_vio [get_hw_probes {tdc_decoder_top_inst/enable_320M} -of_objects [get_hw_vios -of_objects [get_hw_devices xcku035_0] -filter {CELL_NAME=~"tdc_decoder_top_inst/vio_40M_inst"}]]
