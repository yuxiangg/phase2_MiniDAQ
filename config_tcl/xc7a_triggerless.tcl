set_property OUTPUT_VALUE 0 [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/enable_trigger -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {inst_Mezz_config/inst_mezz_config_vio/enable_trigger} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_setup0_inst"}]]

set_property OUTPUT_VALUE 0 [get_hw_probes inst_Mezz_config/inst_mezz_config_vio/enbale_fake_hit -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_setup0_inst"}]]
commit_hw_vio [get_hw_probes {inst_Mezz_config/inst_mezz_config_vio/enbale_fake_hit} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a35t_0] -filter {CELL_NAME=~"inst_Mezz_config/inst_mezz_config_vio/vio_setup0_inst"}]]
